# -*-coding: utf-8 -*-
from __future__ import unicode_literals
from mailer import Mailer
from mailer import Message
import logging
import time
import csv


logdir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/logs/"
csv_file = logdir + "New_Course_Track_19-01-2016.csv"
mailer = Mailer('mail.epfl.ch')


def create_logger(logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        file_handler = logging.FileHandler(log_file, mode='w')
        l.addHandler(file_handler)
        return l


def write_msg(name, en_course_name, fr_course_name, code):

    msg = """
Dear {0},</br>

<p>
I am the Moodle administrator and I have been preparing the new term beginning soon on February 22nd 2016.</p>

<p>We have created a brand new Moodle course page for your course, {1} ({3}), and have enrolled you as its professor.This new course remains invisible to everyone except its teaching staff.</p>


<p>First of all, please log into Moodle with your gaspar credentials. You will see your courses in the dropdown menu 'My Courses', click on the course to access the page. The first steps to start the semester will be to <a href="http://moodle.epfl.ch/pluginfile.php/1654464/mod_resource/content/3/EnrollAssistant.pdf">enrol your assistants</a> and <a href="http://moodle.epfl.ch/course/view.php?id=14888&section=3">edit the course</a>. Once you are ready, <a href="http://moodle.epfl.ch/pluginfile.php/1654445/mod_resource/content/4/MakeCourseVisible.pdf">make your course visible for students</a>. To find out other tips about how to manage and design your course, access the <a href="http://moodle.epfl.ch/course/view.php?id=14888">'Moodle Model Course'</a>.</p>

<p>
We are also offering two workshops to demonstrate how to use Moodle for new users on Monday February 8th and Friday February 12th from 2 pm to 3 pm in the Rolex Center with Patrick Jermann. You can sign up for either of these via the
<a href="https://epfl.doodle.com/poll/nhgt8d3934nhwct6">Doodle</a>.</p>


best wishes,</br>

Ian Flitman</br>
Moodle Administrator</br>

</br>
===================== en français ========================</br>
</br>

Chère / Cher {0},</br>

<p>Je suis l’administrateur Moodle et en vue du nouveau semestre commençant le 22 février 2016, J’ai fait la mise à jour du site Moodle. </p>

<p>Nous avons créé une nouvelle page Moodle pour votre cours {1} ({3}), et vous y avons inscrit en tant que professeur. Cette nouvelle page est actuellement cachée pour tout le monde, exception faite des enseignants.</p>

<p>Pour commencer, connectez-vous dans Moodle avec votre identifiant Gaspar.  Dans le menu déroulant « My Courses » vous verrez apparaitre vos cours, pour y accéder, cliquer sur le cours. La première étape est <a href="http://moodle.epfl.ch/pluginfile.php/1654464/mod_resource/content/3/EnrollAssistant.pdf">d’inscrire vos assistants</a>. Une fois que votre page est prête, il faut <a href="http://moodle.epfl.ch/pluginfile.php/1654445/mod_resource/content/4/MakeCourseVisible.pdf">la rendre visible pour les étudiants </a>. Vous pourrez trouver plus d’information sur la manière de gérer votre page Moodle sur la page <a href="http://moodle.epfl.ch/course/view.php?id=14888">« Moodle Model Course » </a>dans la section « Miscellaneous ».</p>

<p>Nous organisons également deux ateliers avec Patrick Jermann pour montrer aux nouveaux utilisateurs comment utiliser Moodle les lundi 8 février et vendredi 12 février de 14h à 15h au Rolex Learning Center. Vous pouvez vous inscrire pour l’un des deux ateliers grâce au <a href="https://epfl.doodle.com/poll/nhgt8d3934nhwct6">Doodle</a>.</p>


Meilleures salutations</br>

Ian Flitman

    """.encode('utf-8').format(name, en_course_name, fr_course_name, code)

    return msg

# change the 'To=address' to 'To=my_address' when testing
def send_email(address, msg, code):
    my_address = "ian.flitman@epfl.ch"
    message = Message(From="1234@epfl.ch", To=address, charset="utf-8")
    message.Subject = "Your new Moodle course {0}".format(code)
    message.Html = msg
    message.Body = msg
    try:
        mailer.send(message)
        notifications.info(' Notification email sent to {0} about new course {1}'.format(address, code))
    except:
        print 'recipient failed for {0} with course {1}'.format(address, code)
        pass

notifications = create_logger('course_log', logdir + 'New_Course_Notifications_' + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')

with open(csv_file, 'r') as csvfile:
    csv_reader = csv.reader(csvfile, delimiter=str(u','))
    for row in csv_reader:
        #print ', '.join(row)
        #msg = write_msg(row[3].encode('utf-8'), row[1].encode('utf-8'), row[2].encode('utf-8'), row[0].encode('utf-8'))
        msg = write_msg(row[3], row[1], row[2], row[0])
        #print msg
        send_email(row[4], msg, row[0])
        pass
__author__ = 'ian'

from mysql.connector import connect
from mysql.connector.connection import MySQLConnection, MySQLCursor
from sets import Set
import urllib2
import requests
from urllib2 import HTTPError

import requests
from requests.exceptions import HTTPError
import logging
import calendar
import paramiko

from xml.etree import ElementTree

# ssh tunnel in thus first before running this script
# ssh -L 3307:craftmoodle50.epfl.ch:3306 -i /home/ian/Documents/ssh_keys/openssh_ianflitman_privatekey kis@craftmoodle30.epfl.ch


class ProcessInactives(object):

    def __init__(self):
         # self.ssh = paramiko.SSHClient()
         # self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
         # self.ssh.connect('craftmoodle30.epfl.ch', username='kis', password=None, key_filename='/home/ian/Documents/ssh_keys/ianflitman_privatekey.ppk', look_for_keys=False)
         #
          self.mdl_cnx = MySQLConnection()
          self.connect_mdl()
         # self.go_through_courses()

    def setup_logging(self):
        self.logger = logging.getLogger('processInactives')
        self.loghandler = logging. FileHandler('/logs/processInactives.log')
        self.loghandler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s %(messages)s'))
        self.logger.addHandler(self.loghandler)
        self.logger.setLevel(logging.WARNING)

    def connect_mdl(self):
        #self.mdl_cnx = connect(user='moodle', password='tOYY6naw:r', host='craftmoodle50.epfl.ch', database='moodle')
        self.mdl_cnx = connect(user='moodleroot', password='sTe1la6!', host='localhost', port='3307', database='moodle')
        pass
        #self.mdl_cnx = connect(user='moodle', password='tOYY6naw:r', host='127.0.0.1', database='moodle')
        #self.mdl_cnx = connect(user='root', password='ian', host='127.0.0.1', database='moodle


    def students_mdl_enrolled(self, shortname):
        get_students_query = ("SELECT u.idnumber "
                 "FROM mdl_user u "
                 "INNER JOIN mdl_user_enrolments ue ON ue.userid = u.id "
                 "INNER JOIN mdl_enrol e ON e.id = ue.enrolid "
                 "INNER JOIN mdl_course c ON  c.id = e.courseid "
                 "WHERE c.shortname = '{0}'".format(shortname))

        function_mdl_cnx = MySQLConnection()
        function_mdl_cnx = connect(user='root', password='ian', host='127.0.0.1', database='moodle')
        function_mdl_cnx.cmd_query(get_students_query)
        student_ids = []
        for row in function_mdl_cnx.get_rows()[0]:
            student_ids.append(row[0])

        return student_ids


    def go_through_courses(self):
        get_courses_query = ("SELECT shortname from mdl_course WHERE shortname LIKE '%-%'")
        cmd = self.mdl_cnx.cmd_query(get_courses_query)
        headers = {'Content-Type': 'application/xml'}
        total_changes = 0

        for row in self.mdl_cnx.get_rows()[0]:
            inactive_students = []
            if row[0].startswith('--') or row[0].startswith('!'):
                print row
                self.logger.warn(row)
                continue

            courses = row[0].lstrip().split('+')
            for course in courses:
                url = 'http://isa.epfl.ch/services/inscriptions/2014-2015/course/'+course.lstrip()
                try:
                    response = requests.get(url, headers=headers)
                    print str(response.status_code) + ": " + url
                    if response.status_code is 200:
                        xml = ElementTree.fromstring(response.content)
                        inscriptions = xml.findall('./course/curricula/cursus/inscriptions/inscription')
                        for inscription in inscriptions:
                            if(inscription.find('./active').text == 'false'):
                                inactive_students.append(inscription.find('./sciper').text)

                        inactive_set = Set(inactive_students)
                        enrolled_set = Set(self.students_mdl_enrolled(row[0]))
                        intersection = enrolled_set & inactive_set
                        if len(intersection) > 0:
                            print(row[0].lstrip())
                            print(intersection)
                            total_changes += len(intersection)


                except HTTPError:
                    pass

        print "total changes: " + str(total_changes)


    def test_course(self, shortname):

        url = 'http://isa.epfl.ch/services/inscriptions/2014-2015/course/' + shortname
        headers = {'Content-Type': 'application/xml'}
        inactive_students = []
        try:
            response = requests.get(url, headers=headers)
            print str(response.status_code) + ": " + url
            if response.status_code is 200:
                xml = ElementTree.fromstring(response.content)
                inscriptions = xml.findall('./course/curricula/cursus/inscriptions/inscription')
                for inscription in inscriptions:
                    if inscription.find('./active').text == 'false':
                        inactive_students.append(inscription.find('./sciper').text)
        except HTTPError:
                pass

        inactive_set = Set(inactive_students)
        enrolled_set = Set(self.students_mdl_enrolled(shortname))
        remove_set = enrolled_set & inactive_set
        print remove_set
        self.test_delete_inactives(shortname, list(remove_set))
        pass

    def test_delete_inactives(self, course_shortname, students):

        function_mdl_cnx = MySQLConnection()
        function_mdl_cnx = connect(user='root', password='ian', host='127.0.0.1', database='moodle')


        for student in students:
            enrol_query = ("SELECT id from mdl_enrol WHERE enrol='database' AND courseid = "
                           "(SELECT id from mdl_course WHERE shortname = '{0}')".format(course_shortname))

            user_query = ("SELECT id from mdl_user WHERE idnumber = {0}".format(student))

            function_mdl_cnx.cmd_query(enrol_query)
            enrol_result = function_mdl_cnx.get_rows()[0]
            function_mdl_cnx.cmd_query(user_query)
            user_result = function_mdl_cnx.get_rows()[0]

            simple_delete_query = ("DELETE from mdl_user_enrolments WHERE enrolid = {0} AND userid = {1}".format(enrol_result[0], user_result[0]))

            delete_query =("DELETE FROM mdl_user_enrolments "
                           "WHERE enrolid = "
                           "(SELECT id from mdl_enrol WHERE enrol='database' AND courseid = "
                           "(SELECT id from mdl_course WHERE shortname = '{0}'))"
                           "AND userid = "
                           "(SELECT id from mdl_user WHERE idnumber = {1})".format(course_shortname, student))
            #

            function_mdl_cnx.cmd_query(delete_query)
            #delete_result = function_mdl_cnx.get_rows()[0]

            pass

process = ProcessInactives()
process.test_course('PHYS-101(b)')



__author__ = 'ian'
from connectors.IsAcademiaConnector import IsAcademiaConnector
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.MdlServiceConnector import MDLServiceConnector
from mysql.connector import Error as MySQLError
from requests.exceptions import HTTPError
from requests.exceptions import ConnectionError
import re
from mapping import AcademicCalandar
from mapping.courseCategoryMapping import CourseCodeToCategory
from mapping.mdlServers import servers
from mapping.mdlServers import dev_srv_server
from mapping.constants import logs
import time
import logging
from subprocess import Popen, PIPE
import xml.etree.ElementTree as ET

# will need to ssh tunnel-in if in testing on dev or running outside of server
# ssh -L 3308:localhost:3306 -i /home/ian/.ssh/id_rsa root@mdl-dev.epfl.ch
# ssh -L 3307:localhost:3306 flitman@craftmoodle20.epfl.ch
# ssh -L 3307:localhost:3306 flitman@craftmoodle70.epfl.ch


class MdlEnrol:

    def __init__(self, year, term, db):
        self.course_log = self.create_logger('course_log', logs['course'] + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')
        self.inscription_log = self.create_logger('inscription_log', logs['inscription'] + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')
        self.sync_log = self.create_logger('sync_log', logs['sync'] + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')
        self.year = year
        self.term = term
        self.db = servers[db]
        self.IsA = IsAcademiaConnector(year, term)

        self.isDouble = False
        self.other_students = set([])

        # for dev only otherwise connect to real service
        # self.all_course_data = self.IsA.getAllCoursesFromFile('courses_2016')

        try:
            #self.all_course_data = self.IsA.getAllCourses()
            self.all_course_data = self.IsA.getAllCoursesFromFile('courseOct26')
        except Exception as err:
            self.inscription_log.warn("Is Academia Service failed to return. {0}".format(err.message))

        self.Mdl = MdlSqlConnector(db)
        self.MdlSrv = MDLServiceConnector()

        self.professors_enrolled = set([])
        self.professors = set([])
        self.assistants_enrolled = set([])
        self.assistants = set([])
        self.students_enrolled = set([])
        self.student_inactives = set([])
        self.student_actives = set([])

        self.test_parse()

        #self.parse_courses()
        self.call_php_sync()


    def test_parse(self):
        for course_data in self.all_course_data:
            course = course_data['course']
            shortcode = course['courseCode']
            if self.course_isValid(shortcode):
                print "course:{0} exists?: {1} ".format(shortcode, self.Mdl.courseExistsByIdNumber(shortcode))


    def parse_courses(self):
        for course_data in self.all_course_data:
            course = course_data['course']
            shortcode = course['courseCode']

            exists_namesame_name = self.course_valid_and_new(shortcode)

            if shortcode == u'CH-160(a)' or shortcode == u'CH-160(b)':
                pass

            if not exists_namesame_name[0] and exists_namesame_name[1] is not None:
                pass
                #try:
                #    self.create_course(course)
                #except Exception as err:
                #    self.course_log.warn('Failed to create course {0} with exception: {1}'.format(shortcode, err.message))
            elif exists_namesame_name[0] and not exists_namesame_name[1]:
                self.course_log.info("Course shortcode {0} applies to {1}".format(shortcode, exists_namesame_name[2]))
                other_shortcode = exists_namesame_name[2].replace(shortcode, '').replace('$', '')
                others = [course_info for course_info in self.all_course_data if course_info['course']['courseCode'] == other_shortcode]
                if len(others) > 0:
                    self.isDouble = True
                    for other in others[0]['planInscriptions']:
                        self.other_students.add(other['enrolment']['etudiant']['sciper'])

                shortcode = exists_namesame_name[2]
            elif not exists_namesame_name[0] and exists_namesame_name[1] is None:
                self.course_log.info("Not creating or enrolling for {0}".format(shortcode.encode('utf-8')))
                continue

            self.student_inactives = set([])
            self.student_actives = set([])

            if 'planInscriptions' in course_data and len(course_data['planInscriptions']) > 0:
                inscriptions = course_data['planInscriptions']
                for student in inscriptions:
                    student_sciper = student['enrolment']['etudiant']['sciper']
                    if student['active']:
                        self.student_actives.add(student_sciper)
                    else:
                        self.student_inactives.add(student_sciper)

                self.students_enrolled = set(self.Mdl.getAutoEnrolledByShortnameAndRole(shortcode, 'student'))

                if self.isDouble:
                    self.students_enrolled = self.students_enrolled.difference(self.other_students)
                    self.other_students = set([])
                    self.isDouble = False


                # new set common to both ie. those currently enrolled but now inactive (and without an 'active' status
                # as students can go from active to inactive back to active and then have two enrolments in the payload
                # one active and one inactive!)


                true_inactives = self.student_inactives.difference(self.student_actives)
                #old_students = self.students_enrolled.difference(self.student_actives).difference(true_inactives)
                course_nonactives = self.students_enrolled.difference(self.student_actives)
                new_inactives = self.students_enrolled.intersection(true_inactives)
                old_students = new_inactives.union(course_nonactives)
                # new set with actives not enrolled i.e. new students to enrol
                new_students = self.student_actives.difference(self.students_enrolled)
                pass

                if shortcode == 'CH-160(a)$CH-160(b)':

                    if len(old_students) > 0:
                        for sciper in old_students:
                            #self.removeUserTypeByShortnameAndSciper('student', shortcode, sciper)
                            old_user = {'sciper': sciper, 'shortname': shortcode, 'role': 'student'}
                            try:
                                #self.Mdl.unAutoEnrollUserBySciperShortnameAndRole(old_user)
                                #self.inscription_log.info("unAutoEnrolled student: {0}  from {1}".format(sciper, shortcode))
                                print ("unAutoEnrolled student: {0}  from {1}".format(sciper, shortcode))
                            except MySQLError as err:
                                self.inscription_log.warn("unAutoEnroll failed with student:{0} for {1} failed.\n".format(sciper, shortcode) + err.message)


                    if len(new_students) > 0:
                        for sciper in new_students:
                            new_user = {'sciper': sciper, 'shortname': shortcode, 'role': 'student'}
                            try:
                                self.Mdl.autoEnrollUserBySciperShortnameAndRole(new_user)
                                #self.inscription_log.info("autoEnrolled student: {0} to {1}".format(sciper, shortcode))
                                print ("autoEnrolled student: {0} to {1}".format(sciper, shortcode))
                            except MySQLError as err:
                                self.inscription_log.warn("autoEnroll for student:{0} to {1} failed.\n".format(sciper, shortcode) + err.message)

                else:
                    self.inscription_log.info("No student enrollment data in course {0}".format(shortcode))

            pass

            """
            self.professors = set([])

            if 'professors' in course_data['course'] and len(course_data['course']['professors']) > 0:
                professors = course_data['course']['professors']
                for professor in professors:
                    self.professors.add(professor['sciper'])

                self.professors_enrolled = set(self.Mdl.getAutoEnrolledByShortnameAndRole(shortcode, 'professor'))
                # new set with those auto-enrolled but not in Is Academia i.e. old profs
                old_profs = self.professors_enrolled.difference(self.professors)

                if len(old_profs) > 0:
                    for sciper in old_profs:
                        old_user = {'sciper': sciper, 'shortname': shortcode, 'role': 'professor'}
                        try:
                            self.Mdl.unAutoEnrollUserBySciperShortnameAndRole(old_user)
                            self.inscription_log.info("unAutoEnrolled professor: {0} from {1}".format(sciper, shortcode))
                        except MySQLError as err:
                            self.inscription_log.warn("unAutoEnroll failed with professor:{0} from {1} failed.\n".format(sciper, shortcode) + err.message)

                new_profs = self.professors.difference(self.professors_enrolled)

                if len(new_profs) > 0:
                    for sciper in new_profs:
                        new_user = {'sciper': sciper, 'shortname': shortcode, 'role': 'professor'}
                        try:
                            self.Mdl.autoEnrollUserBySciperShortnameAndRole(new_user)
                            self.inscription_log.info("autoEnrolled professor: {0} to {1}".format(sciper, shortcode))
                        except MySQLError as err:
                            self.inscription_log.warn("autoEnroll for professor:{0} to {1} failed.\n".format(sciper, shortcode) + err.message)
            else:
                self.inscription_log.warn("No professors found for course {0}".format(shortcode))

            """

            """
            self.assistants = set([])

            if 'assistants' in course_data['course'] and len(course_data['course']['assistants']) > 0:
                assistants = course_data['course']['assistants']
                for assistant in assistants:
                    self.assistants.add(assistant['sciper'])

                self.assistants_enrolled = set(self.Mdl.getAutoEnrolledByShortnameAndRole(shortcode, 'editingteacher'))
                old_assistants = self.assistants_enrolled.difference(self.assistants)

                if len(old_assistants) > 0:
                    for sciper in old_assistants:
                        old_user = {'sciper': sciper, 'shortname': shortcode, 'role': 'editingteacher'}
                        try:
                            self.Mdl.unAutoEnrollUserBySciperShortnameAndRole(old_user)
                            self.inscription_log.info("Old assistant {0} unAutoEnrolled from {1}".format(sciper, shortcode))
                        except MySQLError as err:
                            self.inscription_log.warn("unAutoEnroll for assistant:{0} for {1} failed.\n".format(sciper, shortcode) + err.message)

                new_assistants = self.assistants.difference(self.assistants_enrolled)

                if len(new_assistants) > 0:
                    for sciper in new_assistants:
                        new_user = {'sciper': sciper, 'shortname': shortcode, 'role': 'editingteacher'}
                        try:
                            self.Mdl.autoEnrollUserBySciperShortnameAndRole(new_user)
                            self.inscription_log.info("New assistant: {0} autoEnrolled for {1}".format(sciper, shortcode))
                        except MySQLError as err:
                            self.inscription_log.warn("autoEnroll for assistant:{0} for {1} failed.\n".format(sciper, shortcode) + err.message)
            else:
                self.inscription_log.info("No assistants found for course {0}".format(shortcode))

            """
    def checkDoubles(self, shortcode, shortcode_str):
        self.other_shortcode = shortcode_str.replace(shortcode, '').replace('$', '')


    def removeUserTypeByShortnameAndSciper(self, user_type, shortname, sciper):
        try:
            self.Mdl.removeFromCourseByRoleAndSciper(shortname, user_type, sciper)
            self.inscription_log.info('Unenrolled {0} with sciper: {1} from {2}'.format(user_type, sciper, shortname))
        except Exception:
            self.inscription_log.warn('Failed to remove {0} with sciper: {1} from {2}'.format(user_type, sciper, shortname))
        pass


    def course_valid_and_new(self, shortcode):
        if self.course_isValid(shortcode):
            return self.Mdl.courseExistsByIdNumber(shortcode)
        else:
            return False, None, None


    def course_isValid(self, shortname):
        if re.search('-\d{3}', shortname):
            sectionCode = shortname[0:shortname.index('-')]
            if sectionCode not in ['EPFL', 'ETH']:  # known exceptions
                return True
            else:
                return False
        else:
            self.course_log.warn("Not Valid Course: {0}".format(shortname.encode('utf-8')))
            #print '\t\t\t\Not Valid {0}'.format(shortname.encode('utf-8'))
            return False


    def create_course(self, course):

        try:
            course_title = course['subject']['name']['fr'].encode('utf-8')
            shortname = course['courseCode'].encode('utf-8')
        except ValueError as err:
            print self.course_log.warn(err.message)

        try:
            category = self.getCourseCategory(course['courseCode'])
        except ValueError as err:
            self.course_log.warn(err.message)

        params = {'fullname': course_title,
                  'shortname': shortname,
                  'categoryid': category,
                  'idnumber': shortname,
                  'startdate': AcademicCalandar.Timetable[self.year][self.term]['start'],  # for new academic year!
                  'numsections': AcademicCalandar.Timetable[self.year][self.term]['weeks'],
                  'visible': 0}

        # category. shortname, idnumber, format, startdate, visible
        # args = (category, shortname, shortname, 'weeks', AcademicCalandar.Timetable['2015-2016'][self.term]['start'], 0)
        # section_num = AcademicCalandar.Timetable['2015-2016'][self.term]['weeks']

        course_params = {'courses': params}
        try:
            #result = self.Mdl.createCourse(args, section_num)
            result = self.MdlSrv.rest_protocol(dev_srv_server, course_params, 'core_course_create_courses')
            res_tree = ET.fromstring(result)
            if res_tree.tag == 'EXCEPTION':
                self.course_log.warn("Failed to create course {0}. Errorcode: {1}. Message {2}".format(shortname, res_tree[0].text, res_tree[1].text))
            elif res_tree.tag == 'RESPONSE':
                self.course_log.info("Course Created - {0}:{1} with id:{2}".format(shortname, course_title, res_tree.find(".//KEY[@name='id']/VALUE").text))

        except Exception as err:
            #print 'failed to create course {0} with exception: {1}'.format(shortname, err.message)
            raise Exception('Failed to create course {0} due to: '.format(shortname, err.message))

        # Moodle API fails to hide these courses so we have to do it manually. Great, huh?
        try:
            self.Mdl.hideCourseByShortname(shortname)
        except Exception:
            raise Exception('course {0} not made invisible'.format(shortname))


    def getCourseCategory(self, shortname):

        hypenIndex = shortname.index('-')
        sectionCode = shortname[0:hypenIndex]

        try:
            year_num = int(shortname[hypenIndex+1:hypenIndex+2])
        except:
            raise ValueError('shortname {0} does not have valid numeral after hyphen'.format(shortname))

        if year_num == 0:
            if sectionCode == 'UNIL':
                return CourseCodeToCategory['UNIL']
            else:
                return CourseCodeToCategory['PREPA']
        elif year_num <= 3:
            try:
                return CourseCodeToCategory[sectionCode]['Bachelor']
            except:
                raise ValueError('no Bachelor course category found for {0}'.format(shortname))
        elif year_num <= 5:
            try:
                return CourseCodeToCategory[sectionCode]['Master']
            except:
                raise ValueError('no Master course category found for {0}'.format(shortname))
        elif year_num >= 6:  # never runs as no inscriptions against PhD students as yet so raising error
            raise ValueError('Doctoral course category found for {0}'.format(shortname))



    def call_php_sync(self):
        proc = Popen(['php', '/var/www/vhosts/moodle.epfl.ch/htdocs/enrol/database/cli/sync.php', '-v'], stdout=PIPE)
        stdout_value = proc.communicate()[0]
        self.sync_log.info(stdout_value)


    def create_logger(self, logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        fileHandler = logging.FileHandler(log_file, mode='w')
        l.addHandler(fileHandler)
        return l



#mdlenrol = MdlEnrol('2015-2016', 'HIVER', 'db_dev')
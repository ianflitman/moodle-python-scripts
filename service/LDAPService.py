__author__ = 'ian'
import ldap
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.MdlServiceConnector import MDLServiceConnector
from mapping.mdlServers import servers
from mapping.constants import logs
import time
import logging

# to re-run this once the db has already been populated run:
# DELETE from mdl_user WHERE id > 2;
# ALTER TABLE 'mdl_user' AUTO_INCREMENT = 3;

class LDAPService:

    def __init__(self, db='dev_srv_server', debug=True):
        self.log = logs['ldap'] + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt'
        logging.basicConfig(filename=self.log, level=logging.DEBUG)

        self.service_server = servers[db]
        self.mdlSQLConnector = MdlSqlConnector(db)
        self.mdlServiceConnector = MDLServiceConnector()
        self.ldap = ldap.open("ldap.epfl.ch")
        self.basedn = "c=ch"
        self.everyone_filter = "(uid=*)"
        self.debug = debug
        self.everyone = []

        # debug variables only
        self.duplicates = 0
        self.peopleTotal = 0
        self.no_mail = 0
        self.no_given_name = 0
        self.no_surname = 0
        self.accounts_made = 0
        self.execution_start = int(round(time.time()))

        self.get_everyone()
        self.log_summary()


    def get_everyone(self):
        self.everyone = self.ldap.search_s(self.basedn, ldap.SCOPE_SUBTREE, self.everyone_filter)
        self.process_everyone()


    def process_everyone(self):
        last_sciper = "0"
        for person in self.everyone:
            sciper = person[1]['uniqueIdentifier'][0]
            #if sciper == '261275':
            #    pass
            if(sciper == last_sciper):
                self.duplicates += 1
            else:
                if not self.mdlSQLConnector.userExistsWithSciper(sciper):
                    isValid = self.check_ldap_entry(person[1])
                    if not isValid:
                        continue

                    self.create_user(person[1])

                self.peopleTotal += 1

            last_sciper = sciper


        if self.debug:
            logging.info("{0} duplicates".format(self.duplicates))
            logging.info("{0} individuals".format(self.peopleTotal))
            logging.info("{0} users with no emails".format(self.no_mail))
            logging.info("{0} users with no givenName".format(self.no_given_name))


    def check_ldap_entry(self, person):

        if 'givenName' not in person:
            self.no_given_name += 1
            return False

        if 'sn' not in person:
            self.no_surname += 1
            return False

        if 'mail' not in person:
            self.no_mail += 1
            logging.warn("{0} user has no email".format(person['uniqueIdentifier'][0]))
            return False

        if 'uniqueIdentifier' not in person:
            return False

        if 'uid' not in person:
            return False

        return True

    def create_user(self, person):
        user_params = {}
        user_params['firstname'] = person['givenName'][0]
        user_params['lastname'] = person['sn'][0]
        user_params['email'] = person['mail'][0]
        user_params['idnumber'] = person['uniqueIdentifier'][0]
        user_params['username'] = self.parse_username(person['uid'][0], person['mail'][0])
        user_params['auth'] = 'tequila'
        user_params['confirmed'] = 1
        user_params['mnethostid'] = 12


        # call to mdlService api will fail as we have no passwords to pass in and it needs them to work :(
        # self.mdlServiceConnector.rest_protocol(self.service_server, users, 'core_user_create_users')
        try:
            self.mdlSQLConnector.createUser(user_params)
        except Exception:
            logging.warn('Failed to create user with firstname: {0}, lastname: {1}, email:{2}, '
                         'sciper: {3}, username: {4} and auth: {5}'.format(user_params['firstname'], user_params['lastname'],
                                                               user_params['email'], user_params['idnumber'],
                                                               user_params['username'], user_params['auth']))
        self.accounts_made += 1
        logging.info('New Account: {0} {1} \t{2}\t{3}\t{4}\t{5}'.format(user_params['firstname'], user_params['lastname'], user_params['username'], user_params['idnumber'], user_params['email'], user_params['auth']))


    # EPFL members with duplicate identities in ldap can have their uid or username as 'name@acronymn'
    # rather than just 'name' and therefore we use this function to check whether the uids have the
    # @ symbol and whether they have an EPFL email address. If so, we snip off the '@acronym' part of
    # the returned username/uid. If they do not have an epfl email address, their username should be
    # identical to their email address so we keep it intact.
    def parse_username(self, uid, mail):
        if '@' in uid and '@epfl.ch' in mail:
            return uid[0:uid.index('@')]
        else:
            return uid


    def log_summary(self):
        logging.info('\n\nLDAP Service Summary for {0}'.format(time.strftime("%d/%m/%Y")))
        logging.info('Accounts created: {0} in {1} secs'.format(self.accounts_made, (int(round(time.time())) - self.execution_start)))

        pass


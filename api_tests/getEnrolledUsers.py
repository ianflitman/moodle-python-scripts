__author__ = 'ian'

from connectors.MdlServiceConnector import MDLServiceConnector
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.IsAcademiaConnector import IsAcademiaConnector
from mapping.mdlServers import prod_srv_server, dev_srv_server
from xml.etree import ElementTree as etree
#from sets import Set


myMdlServiceConnector = MDLServiceConnector()
myMdlSqlConnector = MdlSqlConnector('db_prod')
myIsAcademiaConnector = IsAcademiaConnector('2014-2015', 'ETE')


moodle_user_ids = myMdlSqlConnector.getUsersEnrolledByCourseShortname('AR-101')

#returns dict with {'active': [], 'inactive':[]}
is_academia_user_ids = myIsAcademiaConnector.getCourseInscriptions('MGT-635')

#make them sets
mdlers = set(moodle_user_ids)
actives = set(is_academia_user_ids['active'])
inactives = set(is_academia_user_ids['inactive'])

#any inactives in current mdl users?
mdlers_inactive = mdlers & inactives
if(len(mdlers_inactive) > 0):
    print 'In MATH-126 these Moodle users are inactive in Is Academia:\n{0}'.format(mdlers_inactive)

#any users not enrolled when they should be?

shortnames = myIsAcademiaConnector.getAllCourseShortnames()



pass


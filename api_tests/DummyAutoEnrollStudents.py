__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector
from subprocess import Popen, PIPE


mdlSql = MdlSqlConnector('db_dev')

new_user_1 = {'sciper': 'G22098', 'shortname': 'EE-456', 'role': 'student'} # Marc 123
new_user_2 = {'sciper': 'G22092', 'shortname': 'EE-456', 'role': 'student'} # Booba 92i
new_user_3 = {'sciper': '260087', 'shortname': 'EE-456', 'role': 'student'} # Simen Aamodt
new_user_4 = {'sciper': 'G21624', 'shortname': 'EE-456', 'role': 'student'} # Saad aarich
new_user_5 = {'sciper': '238266', 'shortname': 'EE-456', 'role': 'student'} # Theo Michel Abadie

mdlSql.autoEnrollUserBySciperShortnameAndRole(new_user_1)
mdlSql.autoEnrollUserBySciperShortnameAndRole(new_user_2)
mdlSql.autoEnrollUserBySciperShortnameAndRole(new_user_3)
mdlSql.autoEnrollUserBySciperShortnameAndRole(new_user_4)
mdlSql.autoEnrollUserBySciperShortnameAndRole(new_user_5)

# proc = Popen(['php', '/var/www/vhosts/moodle.epfl.ch/htdocs/enrol/database/cli/sync.php', '-v'], stdout=PIPE)
# stdout_value = proc.communicate()[0]
# print stdout_value


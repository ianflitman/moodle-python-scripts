__author__ = 'ian'

from datetime import date
from time import mktime

LDAP_LOG_FILE = '/home/ian/workspace/pyMoodleScripts/logs/LDAP_'
MDL_LDAP_FILE = '/home/ian/workspace/pyMoodleScripts/logs/MDL_LDAP_'
IS_ACADEMIA_LOG_FILE = '/home/ian/workspace/pyMoodleScripts/logs/IS_Academia_'
COURSE_SERVICE_FILE = '/home/ian/workspace/pyMoodleScripts/logs/CourseService_'
INSCRIPTION_SERVICE_FILE = '/home/ian/workspace/pyMoodleScripts/logs/InscriptionService_'
OLD_INSCRIPTION_SERVICE_FILE = '/home/ian/workspace/pyMoodleScripts/logs/OldCourseInscriptionService_'
SYNC_LOG_FILE = '/home/ian/workspace/pyMoodleScripts/logs/SyncPHPLog_'
RESYNC_LOG_FILE = '/home/ian/workspace/pyMoodleScripts/logs/ReSyncPHPLog_'
RESET_FILE = 'home/ian/workspace/pyMoodleScripts/logs/resetFile_'

logs = {
    'mdl_check': MDL_LDAP_FILE,
    'ldap': LDAP_LOG_FILE,
    'IsAcademia': IS_ACADEMIA_LOG_FILE,
    'course': COURSE_SERVICE_FILE,
    'inscription': INSCRIPTION_SERVICE_FILE,
    'old_inscription': OLD_INSCRIPTION_SERVICE_FILE,
    'sync': SYNC_LOG_FILE,
    'resync': RESYNC_LOG_FILE
}

Year_2014 = {
    'HIVER': {'start': int(mktime(date(2014, 9, 15).timetuple())), 'end': int(mktime(date(2014, 12, 19).timetuple())), 'weeks': 14},
    'ETE': {'start': int(mktime(date(2015, 2, 16).timetuple())), 'end': int(mktime(date(2015, 5, 29).timetuple())), 'weeks': 15}
}

Year_2015 = {
    'HIVER': {'start': int(mktime(date(2015, 9, 14).timetuple())), 'end': int(mktime(date(2015, 12, 18).timetuple())), 'weeks': 14},
    'ETE': {'start': int(mktime(date(2015, 2, 22).timetuple())), 'end': int(mktime(date(2015, 6, 3).timetuple())), 'weeks': 15}
}

Year_2016 = {
    'HIVER': {'start': int(mktime(date(2016, 9, 20).timetuple())), 'end': int(mktime(date(2015, 12, 23).timetuple())), 'weeks': 14},
    'ETE': {'start': int(mktime(date(2015, 2, 20).timetuple())), 'end': int(mktime(date(2015, 6, 2).timetuple())), 'weeks': 15}
}

Academic_Calandar = {
    '2014-2015': Year_2014,
    '2015-2016': Year_2015,
    '2016-2017': Year_2016,
}

DOCTORAL_CAT = 19
PREP_CAT = 17
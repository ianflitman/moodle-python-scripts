__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector

myMdlSqlConnector = MdlSqlConnector('db_dev')
myMdlSqlConnector.connect_mdl()

enrolled_users = myMdlSqlConnector.getUsersEnrolledByCourseShortname('MICRO-450')

for user in enrolled_users:
    myMdlSqlConnector.removeStudentFromCourseBySciper(user, 'MICRO-450')
    pass
__author__ = 'ian'
from connectors.IsAcademiaConnector import IsAcademiaConnector
from connectors.MdlServiceConnector import MDLServiceConnector
from mapping.mdlServers import dev_srv_server
from mapping.mdlServers import prod_srv_server
from mapping.courseCategoryMapping import CodeToCategoryDict

myIsAcademiaConnector = IsAcademiaConnector('2014-2015', 'ETE')
myMdlConnector = MDLServiceConnector()

def ba_or_ma(course):
    hyphen_index = course['shortname'].find('-')
    courseFirstNumberCode = int(course['shortname'][hyphen_index+1:hyphen_index+2])
    return 'BA' if courseFirstNumberCode < 4 else 'MA'

def find_category(shortcode):
    myMdlConnector.rest_protocol(prod_srv_server,)
    pass


sectionCourses = myIsAcademiaConnector.getSectionCourseBooks('AR')

for course in sectionCourses:
    courseInfo = {'courses': course}

    cat = ba_or_ma(course)
    course['categoryid'] = CodeToCategoryDict['AR'][2][cat]

    result = myMdlConnector.rest_protocol(dev_srv_server, courseInfo, 'core_course_create_courses')
    print result

pass


    #course['categoryid'] = CodeToCategory['AR'][2][cat]


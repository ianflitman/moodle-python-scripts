__author__ = 'ian'
from connectors.MdlServiceConnector import MDLServiceConnector
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.IsAcademiaConnector import IsAcademiaConnector

myMdlServiceConnector = MDLServiceConnector()
myMdlSqlConnector = MdlSqlConnector('db_prod')
myIsAcademiaConnector = IsAcademiaConnector('2014-2015', 'HIVER')


shortnames = myIsAcademiaConnector.getAllCourseShortnames()

totalInactives = 0
nonMoodle = 0
moodle = 0

for shortname in shortnames:
    returnList = myMdlSqlConnector.getIdCategoryFromShortname(shortname)
    if len(returnList) > 0:
        moodle += 1
        moodle_user_ids = myMdlSqlConnector.getUsersEnrolledByCourseShortname(shortname)
        is_academia_user_ids = myIsAcademiaConnector.getCourseInscriptions(shortname)
        mdlers = set(moodle_user_ids)
        actives = set(is_academia_user_ids['active'])
        inactives = set(is_academia_user_ids['inactive'])
        mdlers_inactive = mdlers & inactives
        if(len(mdlers_inactive) > 0):
            inactives_file = open('logs/inactives_autumn2014.txt', 'a')
            inactives_file.write('{0} has inactive users in Is Academia that are still enrolled in Moodle:\n{1}\n\n'.format(shortname.encode('utf8'), mdlers_inactive))
            inactives_file.close()
            totalInactives += len(mdlers_inactive)
    else:
        no_course_file = open('logs/no_course_autumn2014.txt', 'a')
        no_course_file.write(shortname.encode('utf8') + '\n')
        no_course_file.close()
        nonMoodle += 1

inactives_file = open('logs/inactives_autumn2014.txt', 'a')
inactives_file.write('total inactives: {0}'.format(totalInactives))
no_course_file = open('logs/no_course.txt_autumn2014', 'a')
no_course_file.write('\n\n\nTotal courses not in Moodle: ' + str(nonMoodle) + '\n')
no_course_file.write('\n\n\nTotal courses in Moodle: ' + str(moodle) + '\n')
no_course_file.close()
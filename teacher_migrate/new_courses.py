__author__ = 'ian'

import json
import re
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.LDAPConnector import LDAPConnector
from mailer import Mailer
from mailer import Message
import logging
import time
import csv


# ssh -L 3307:localhost:3306  flitman@craftmoodle70.epfl.ch
# then supply passwd prior to running script

# ssh -L 3308:localhost:3306  root@cedegesrv1.epfl.ch
# then supply passwd prior to running script

mailer = Mailer('mail.epfl.ch')
ldap = LDAPConnector()
Mdl = MdlSqlConnector('db_70')
Mdl_Archive = MdlSqlConnector('db_archive')

datadir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/datadir/"
current = "ete_2016_Jan18.json"
logdir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/logs/"
csv_log = logdir + 'New_Course_Track_' + time.strftime("%d/%m/%Y").replace('/', '-') + '.csv'

current_stream = open(datadir + current, "r")
current_data = json.loads(current_stream.readline())


def create_logger(logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        file_handler = logging.FileHandler(log_file, mode='w')
        l.addHandler(file_handler)
        return l


def get_email(sciper, code):
    #for count, sciper in enumerate(scipers, start=0):
    email = ldap.email_from_sciper(sciper)

    if email is None:

        print 'Trying Moodle instead.'
        try:
            email = Mdl.getEmailFromSciper(sciper)[0]
            print 'Found email in Moodle: ' + email
        except IndexError:
            print 'No email either in Moodle'

            try:
                email = Mdl_Archive.getEmailFromSciper(sciper)[0]
                print 'Found email in Archive: ' + email
            except IndexError:
                print 'No email either in Archive. Finally ignoring user.'
                notifications.info('\t Could not find email for course {0} prof with sciper: {1}'.format(code, sciper))
                pass
    else:
        print 'found email in ldap'

    if email is None:
        return None
    else:
        return email

def writeMsg(name, en_course_name, fr_course_name, code):

    msg = """
Dear {0},

I am the Moodle administrator and I am preparing the new term beginning on February 22nd 2016. We have created a brand new Moodle course page for your course, {1} ({0}), and have enrolled you as its professor.

The course remains invisible to everyone except its teaching staff. To access your new course page, log into Moodle with your gaspar credentials.   To find out, how to access the course

    """

    return msg


def send_email(address, msg, code):
    my_address = "ian.flitman@epfl.ch"
    message = Message(From="ian.flitman@epfl.ch", To=address, charset="utf-8")
    message.Subject = "Removing content from your old Moodle course {0}".format(code)
    message.Body = msg
    try:
        mailer.send(message)
        notifications.info(' Notification email sent to {0} about {1}'.format(address, code))
    except:
        print 'recipient failed for {1} with course {}'.format(address, code)
        pass


def course_isValid(shortname):
    if re.search('-\d{3}', shortname):
        sectionCode = shortname[0:shortname.index('-')]
        if sectionCode not in ['EPFL', 'ETH']:  # known exceptions
            return True
        else:
            return False
    else:
        #print '\t\t\t\Not Valid {0}'.format(shortname.encode('utf-8'))
        return False

notifications = create_logger('course_log', logdir + 'New_Course_Info_' + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')

current = {}
for course_data in current_data:
    course = course_data['course']
    if course_isValid(course["courseCode"]):
        current[course['courseCode']] = course

new_course_total = 0
for code in current.keys():
    if course_isValid(course["courseCode"]):  # and not Mdl.courseExistsByIdNumber(code)[0]:
        exist_tuple = Mdl.courseExistsByIdNumber(code)
        if exist_tuple[0] == 0 and exist_tuple[1] is False:
            new_course_total += 1
            notifications.info("new course: {0}".format(code))
            profs = current[code]["professors"]
            for prof in profs:
                email = get_email(prof["sciper"], code)
                if email is not None:
                    subject = current[code]["subject"]["name"]
                    if "en" in subject:
                        en_name = subject["en"].encode('utf-8')
                    else:
                        en_name = subject["fr"].encode('utf-8')

                    if "fr" in subject:
                        fr_name = subject["fr"].encode('utf-8')
                    else:
                        fr_name = subject["en"].encode('utf-8')

                    notifications.info("taught by: {0}, sciper: {1}, email: {2}".format(prof["fullName"].encode('utf-8'), prof["sciper"], email))
                    with open(csv_log, 'a') as csvfile:
                        csv_writer = csv.writer(csvfile, delimiter=',')
                        csv_writer.writerow([code, en_name, fr_name, prof["fullName"].encode('utf-8'), email])

                else:
                    print 'No email found'

print 'New course total: {0}'.format(new_course_total)

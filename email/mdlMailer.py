#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = 'ian'
from mailer import Mailer
from mailer import Message

import csv


class MoodleMailer:

    def __init__(self, csv_file, message_body, subject):
        self.mailer = Mailer('mail.epfl.ch')
        self.csv = csv_file
        self.msg_body = message_body.encode(encoding="utf-8", errors="ignore")
        self.subject = subject
        self.send()


    def send(self):
        with open(self.csv, 'rb') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',')
            for row in csv_reader:
                message = Message(From="ian.flitman@epfl.ch", To="{0}".format(row[2]), charset="utf-8")
                message.Body = self.msg_body
                message.Subject = self.subject
                print row[2]
                try:
                    self.mailer.send(message)
                except:
                    print 'recipient failed'
                    pass




messageBody = """
Dear Teacher,

I am writing to you as the EPFL Moodle administrator about the forthcoming changes to Moodle which you use for your courses. Here are the three maintenance dates during the summer:

Week beginning July 13th => Archive courses
July 24th => Reset courses
August 13th => Upgrade and new skin

Archive. As the academic year has just finished we will shortly need to archive Moodle. This will produce a reliable copy of the site from which you can, if required, restore your course data from. This will happen during the week beginning July 13th. 

Reset. Moodle also needs to be reset in preparation for the new academic year. This means:  
all registrations for all courses for the academic year 2014-2015 will be deleted. 
all courses that began in February this year will be hidden
all quiz attempt data and grades will be cleared.
all forum posts will be cleared
This will happen on July 24th. After that date, if you need the data removed you can get it from the archive for your course.

Upgrade. We are also upgrading Moodle to a newer version with a new skin, which should, subject to testing, allow it to be used across a wider variety of devices including tablets and phones. This will happen on August 13th.

Integration with IS-Academia. We are also working to have better integration between IS-Academia, the academic database of EPFL, and Moodle. 

After August 13th, all courses in the Bachelor and Master study plans (http://edu.epfl.ch/studyplan) should have a Moodle page that is created by default. The course remains hidden for students as long as you wish. To make it visible simply user your course settings. All currently existing courses will remain with all their content.

Students who register for the course in IS-Academia should have automatically access to the Moodle course. When students officially opt out of the course, they should be removed from the students list in Moodle.  Naturally, those who still need manual enrolment or enrolment keys (a course-wide password that gives access to the course) can, however, continue to use this method if they want to prevent students who are not officially registered in IS Academia to have access to the course.

Wishing you all the best of summers,
Ian Flitman

Computer Scientist,
Center For Digital Education

===============================================================================================

Chères et Chers enseignant-e-s,
En tant qu’administrateur de la plateforme Moodle de l’EPFL, je vous écris afin de vous informer des changements qui vont intervenir sous peu. Voici les trois dates de maintenance de Moodle pour cet été :
Semaine du 13 juillet : archivage des cours
24 juillet : réinitialisation des cours
13 août : mise à jour et nouveau design
Archivage : Comme l’année académique vient de toucher à sa fin, nous allons bientôt faire l’archivage Moodle. Cela génèrera une copie conforme de vos pages Moodle telles qu’elles seront à la date de l’archivage. L’archivage aura lieu dans le courant de la semaine du 13 juillet.
Réinitialisation des cours : Nous devons réinitialiser les pages Moodle en préparation de la plateforme 2015-2016. Cela signifie que :
* Toutes les inscriptions pour les cours de l’année académique 2014-2015 seront effacées.
* Tous les cours du semestre d’été de cette année seront cachés.
* Toutes les tentatives des quizz et les notes seront effacées.
* Tous les messages dans les forums seront effacés.
Cela se fera le 24 juillet. Si passé cette date vous avez besoin de données qui ont été effacées, vous pourrez les retrouver dans l’archive.
Mise à jour: Nous allons également passer à une nouvelle version de Moodle avec un nouveau design qui devrait, selon les tests, permettre son utilisation depuis une plus large variété d’appareils, y compris les tablettes et les smartphones. L’upgrade sera fait le 13 août.
Intégration de IS Academia : Nous travaillons également dans le but d’avoir une meilleure intégration entre Moodle et IS Academia, la base de données académique de l’EPFL.
Après le 13 août, tous les cours Bachelor et Master figurant dans le plan d’étude (http://edu.epfl.ch/studyplan/fr) devraient avoir une nouvelle page Moodle créée par défaut. Le cours sera invisible pour les étudiants, vous pourrez le rendre visible à votre convenance dans les paramètres du cours (section administration).
Bien entendu, les cours existants et leur contenu seront aussi disponibles sur la plateforme Moodle 2015-2016.
Les étudiants qui s’inscriront pour le cours dans IS Academia devraient avoir automatiquement accès à la page du cours dans Moodle. Les étudiants qui se désinscriront officiellement du cours devraient être automatiquement retirés de la liste des étudiants du cours dans Moodle. Naturellement, ceux qui auront besoin d’utiliser l’inscription manuelle ou de mettre une clé d’inscription (le mot de passe permettant l’accès à la page Moodle) pourront toujours le faire afin de limiter l’accès au cours aux étudiants n’étant pas inscrits dans IS Academia.
En vous souhaitant un excellent été,

Ian Flitman

Computer Scientist,
Center For Digital Education
"""
#f = file('/home/ian/moodleDatas/emails/changes_coming/combined.doc').read()
#print f


print 'send line code commented out to prevent mass resends'
#print 'mass emailing'



#mdl_mailer = MoodleMailer('/home/ian/Downloads/moodle-teachers-2013-2014.csv', messageBody.decode(encoding='utf-8'), "Changes to Moodle / Les modifications apportées à Moodle")


#message = Message(From="ian.flitman@epfl.ch",
                  #To="{0}".format(),
                  #charset="utf-8")

#message.Subject = "An HTML Email"
#message.Html = """This email uses <strong>HTML</strong>!"""
#message.Body = """This is alternate text."""

#sender = Mailer('smtp.example.com')
#sender.send(message)


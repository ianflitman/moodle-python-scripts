__author__ = 'ian'
import ldap

class LDAPConnector():

    def __init__(self):
        self.epfl_ldap = ldap.open("ldap.epfl.ch")
        self.epfl_ldap.simple_bind_s()
        self.basedn = "c=ch"


    def email_from_sciper(self, sciper):
        unique_identifier = "(uniqueidentifier={0})".format(sciper)
        full_result = self.epfl_ldap.search_s(self.basedn, ldap.SCOPE_SUBTREE, unique_identifier)
        if len(full_result) == 0:
            print 'no EPFL LDAP record for sciper: {0}'.format(sciper)
            return None
        try:
            result = self.epfl_ldap.search_s(self.basedn, ldap.SCOPE_SUBTREE, unique_identifier)[0][1]
        except IndexError:
            pass
        if 'mail' in result:
            return result['mail'][0]
        else:
            print ''.join([result['cn'][0].decode('utf-8'), ' has no mail in ldap'])
            return None
            pass

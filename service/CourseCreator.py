__author__ = 'ian'

from connectors.MdlServiceConnector import MDLServiceConnector
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.IsAcademiaConnector import IsAcademiaConnector
from mapping.constants import logs, DOCTORAL_CAT, PREP_CAT
from mapping.courseCategoryMapping import CourseCodeToCategory, CourseCodesToIgnore
from mapping.mdlServers import servers
from mapping import AcademicCalandar
import time
import logging

# When re-running script execute the following mySQL on the db
# DELETE FROM `mdl_course` WHERE id > 1;
# ALTER TABLE `mdl_course` AUTO_INCREMENT = 2;

# for db_prod
# ssh tunnel in thus first before running this script if running outside the server itself
# ssh -L 3307:craftmoodle50.epfl.ch:3306 -i /home/ian/Documents/ssh_keys/openssh_ianflitman_privatekey kis@craftmoodle30.epfl.ch

# for db_dev
# ssh tunnel in thus first before running this script if running outside the server itself
# ssh -L 3308:localhost:3306 -i /home/ian/.ssh/id_rsa ian@mdl-dev.epfl.ch


class CourseCreator:

    def __init__(self, db, year, term):
        self.log = logs['course'] + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt'
        logging.basicConfig(filename=self.log, level=logging.INFO)

        self.year = year
        self.term = term
        self.db = servers[db]

        self.srv_db = servers[db]
        # if db == 'db_dev':
        #     self.srv_db = servers['dev_srv_server']
        # elif db == 'db_prod':
        #     self.srv_db = servers['prod_srv_server']

        self.mdlServiceConnector = MDLServiceConnector()
        self.mdlSqlConnector = MdlSqlConnector(db)
        #self.mdlDevConnector = MdlSqlConnector('db_mdl_dev')
        #self.isAcademiaConnector = IsAcademiaConnector(year, term)

        self.courseStart = AcademicCalandar.Timetable[year][term]['start']
        self.courseWeeks = AcademicCalandar.Timetable[year][term]['weeks']
        self.courses = []

        # debug var
        self.coursesNotCreated = []
        self.courseNum = 0
        self.courseHiddenNum = 0
        self.getShortnames()
        logging.info('Course Creation for {0} {1} made on {2}\n\nAlready Existing and Visible Courses\n\n'.format(year, term, time.strftime("%d/%m/%Y")))
        self.createCourses()
        # creates courses that have inscriptions but not currently in Moodle
        logging.info('\n\nNew and Hidden Courses\n\n'.format(year, term))
        self.createNewCourses()
        logging.info('\n{0} Courses not created {1}'.format(len(self.coursesNotCreated), self.coursesNotCreated))
        logging.info('\n{0} Total Courses made\n{1} Already existing and visible courses\n{2} New and hidden courses'.format((self.courseNum + self.courseHiddenNum), self.courseNum, self.courseHiddenNum))


    # def getShortnames(self):
    #     # self.isAcademiaConnector.getAllCourseInfo()
    #     self.courses = self.isAcademiaConnector.getAllCourseInfo()
    #     for course in self.courses:
    #         validation = self.validateCourse(course['shortname'])
    #         if validation[0] is False:
    #             print '\tinvalid {0}'.format(course['shortname'])
    #             self.courses.remove(course)
    #             self.coursesNotCreated.append(course['shortname'])


    # returns tuple (isValid, MA/BA)
    def validateCourse(self, shortname):
        # print shortname
        if '-' not in shortname:
            return (False, None)
        else:
            hypenIndex = shortname.index('-')
            try:
                year_num = int(shortname[hypenIndex+1:hypenIndex+2])
            except:
                return (False, None)

            if year_num <= 3:
                return(True,'Bachelor')
            elif year_num <= 5:
                return(True, 'Master')
            elif year_num >= 6:  # never runs as no inscriptions against PhD students
                return (True, 'Doctoral')


    def createCourses(self):
        for course in self.courses:
            existingTuple = self.getExistingDetailFromShortname(course['shortname'])
            if existingTuple[0] is True:
                course['existing'] = True
                course['category'] = existingTuple[1]
                course['fullname'] = existingTuple[2]
                course['format'] = existingTuple[3]
                self.makeExistingCourse(course)
            else:
                course['existing'] = False

        self.makeExistingDoctoralCourses()


    def makeExistingCourse(self, course, visible=1):
        params = {}
        if course['format'] == 'weeks':
            params = {
                'fullname': course['fullname'],
                'shortname': course['shortname'],
                'categoryid': course['category'],
                'startdate': self.courseStart,
                'numsections': self.courseWeeks
            }
        else:
            params = {
                'fullname': course['fullname'],
                'shortname': course['shortname'],
                'categoryid': course['category'],
                'startdate': self.courseStart,
                'format': course['format']
            }

        courses = {'courses': params}
        result = self.mdlServiceConnector.rest_protocol(self.srv_db, courses, 'core_course_create_courses')
        logging.info('{0} {1} format: {2}'.format(course['shortname'], course['fullname'], course['format']))
        self.courseNum +=1


    def makeNewHiddenCourse(self, course):
        params = {
                'fullname': course['fullname'],
                'shortname': course['shortname'],
                'categoryid': course['category'],
                'startdate': self.courseStart,
                'numsections': self.courseWeeks,
                'visible': 0
            }

        courses = {'courses': params}
        result = self.mdlServiceConnector.rest_protocol(self.srv_db, courses, 'core_course_create_courses')
        logging.info('{0} {1} format: {2}'.format(course['shortname'], course['fullname'], 'weeks'))
        self.coursesNotCreated.remove(course['shortname'])
        self.courseHiddenNum +=1


    def makeExistingDoctoralCourses(self):
        self.mdlDevConnector.connect_mdl()
        doctoral_query = ("SELECT shortname, fullname, format from mdl_shortname_category where category = {0}".format(DOCTORAL_CAT))
        self.mdlDevConnector.cursor.execute(doctoral_query)

        course_params = {}

        for(shortname, fullname, format) in self.mdlDevConnector.cursor:
            course_params['shortname'] = shortname
            course_params['fullname'] = fullname.encode('utf-8')
            course_params['category'] = DOCTORAL_CAT
            course_params['format'] = format

            if course_params['format'] == 'weeks':
                params = {
                    'fullname': course_params['fullname'],
                    'shortname': course_params['shortname'],
                    'categoryid': course_params['category'],
                    'startdate': self.courseStart,
                    'numsections': self.courseWeeks
                }
            else:
                params = {
                    'fullname': course_params['fullname'],
                    'shortname': course_params['shortname'],
                    'categoryid': course_params['category'],
                    'startdate': self.courseStart,
                    'format': course_params['format']
                }

            courses = {'courses': params}
            result = self.mdlServiceConnector.rest_protocol(self.srv_db, courses, 'core_course_create_courses')
            logging.info('{0} {1} format: {2}'.format(course_params['shortname'], course_params['fullname'], course_params['format']))
            self.courseNum +=1

        self.mdlDevConnector.cursor.close()
        self.mdlDevConnector.mdl_cnx.close()


    def createNewCourses(self):
        for course in self.courses:
            if course['existing'] == False and '-' in course['shortname']:
                hypenIndex = course['shortname'].index('-')
                courseSectionCode = course['shortname'][0:hypenIndex]
                if courseSectionCode not in CourseCodesToIgnore:
                    try:
                        year_num = int(course['shortname'][hypenIndex+1:hypenIndex+2])
                    except:
                        continue

                    if year_num == 0:
                        course['category'] = PREP_CAT
                        self.makeNewHiddenCourse(course)
                    elif year_num <= 3:
                        course['category'] = CourseCodeToCategory[courseSectionCode]['Bachelor']
                        self.makeNewHiddenCourse(course)
                    elif year_num <= 5:
                        course['category'] = CourseCodeToCategory[courseSectionCode]['Master']
                        self.makeNewHiddenCourse(course)
                else:
                    continue


    def getExistingDetailFromShortname(self, shortname):

        self.mdlDevConnector.connect_mdl()
        cat_query = ("SELECT category, fullname, format from mdl_shortname_category where shortname LIKE '%{0}%'".format(shortname))
        try:
            self.mdlDevConnector.mdl_cnx.cmd_query(cat_query)
        except:
            self.coursesNotCreated.append(shortname)
            return (False, None)
            # raise Exception('could not select {0} course id from mdl_course_categories'.format(shortname))

        result = self.mdlDevConnector.mdl_cnx.get_rows()[0]

        if len(result) == 0:
            self.coursesNotCreated.append(shortname)
            return (False, None)

        self.mdlDevConnector.mdl_cnx.close()
        return (True, int(result[0][0]),result[0][1], result[0][2])


    def processCourseBooks(self):
        for course in self.shortnames:
            coursebook = self.isAcademiaConnector.getCourseBook(course)
            pass


creator = CourseCreator('db_dev', '2014-2015', 'ETE')
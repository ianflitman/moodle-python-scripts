__author__ = 'ian'
from connectors.IsAcademiaConnector import IsAcademiaConnector
from connectors.MdlServiceConnector import MDLServiceConnector
from mapping.mdlServers import dev_srv_server
from mapping import courseCategoryMapping

mdlConnector = MDLServiceConnector()

categoryList = []

for cat in courseCategoryMapping.CodeToCategory:
    category = {}
    category['id'] = courseCategoryMapping.CodeToCategory[cat][0].encode('utf-8')
    pass
# -*-coding: utf-8 -*-
from __future__ import unicode_literals
import json
import re
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.LDAPConnector import LDAPConnector
from mailer import Mailer
from mailer import Message
import logging
import time
import csv


mailer = Mailer('mail.epfl.ch')
ldap = LDAPConnector()
course_staff_changes = 0


def create_logger(logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        fileHandler = logging.FileHandler(log_file, mode='w')
        l.addHandler(fileHandler)
        return l


def sendEmail(address, msg, code):
    my_address = "ian.flitman@epfl.ch"
    message = Message(From="1234@epfl.ch", To=address, charset="utf-8")
    message.Subject = "Removing content from your old Moodle course {0}".format(code)
    message.Body = msg
    try:
        mailer.send(message)
        notifications.info(' Notification email sent to {0} about {1}'.format(address, code))
    except:
        print 'recipient failed for {1} with course {}'.format(address, code)
        pass


def writeMsg(name, en_course_name, fr_course_name, code):

    msg = """
Dear {0},

While preparing Moodle for the new term beginning on February 22nd 2016, I noticed that your course, '{1}' ({3}) appears to be no longer taught by you.

This might be because your name is simply missing at present from the Is Academia record. If so, you should update the record for the course in Is Academia through your section.

However, if you taught this course and it was populated with your content in Moodle, you may not want it to be used by the new professor or teaching team taking over from you.

In this case, if you wish we can delete the content of the course for you. If you are concerned about losing the content, you will be pleased to know that we already have an archived copy of the course available at http://moodlearchive.epfl.ch in case you need to recover the material at a later date, or reuse it in another course within Moodle.

If I do not hear from you, I will do nothing and leave the Moodle course as it currently is.

best wishes,

Ian Flitman
Moodle Administrator


===================== en français ========================

{0},

En faisant la préparation du site Moodle pour le début du semestre de printemps qui débutera le 22 février 2016, j’ai remarqué que le cours '{2}' ({3}) semblerait ne plus être enseigné par vous.

Il se peut que cela vienne du fait que votre nom ne soit pas inscrit dans IS Academia pour l’enseignement de ce cours. Si c’est le cas, merci de faire le nécessaire auprès de votre section.

Par contre, si vous avez enseigné ce cours, que la page Moodle contient vos documents de cours et que vous ne souhaitez pas que le nouvel enseignant utilise ce matériel, merci de me contacter afin que je puisse effacer le contenu de la page. Si vous êtes soucieux de conserver le matériel de ce cours, nous vous informons qu’une copie de cette page et de son contenu est disponible dans les archives Moodle (http://moodlearchive.epfl.ch/).

Sans nouvelle de votre part, aucune mesure ne sera prise et la page sera laissée telle quelle.

Avec mes meilleures salutations

Ian Flitman
Administrateur Moodle

""".encode('utf-8').format(name, en_course_name, fr_course_name, code)

    return msg

# for current data curl the json service endpoint thus:
# curl --header "Accept:application/json" "https://isa.epfl.ch/services/courses/2015-2016/ETE" > /home/ian/moodleDatas/ete_2016_Jan12.json

logdir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/logs/"
csvdir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/csv/"
notifications = create_logger('course_log', logdir + 'Course_Staff_Changes' + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')
datadir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/datadir/"
current = "hiver_2016_10June.json"

# connection for this years Moodle db
# some courses exist but in different terms from its present instantiation
# i.e. an autumn course was formerly held in spring and vice versa
# tunnel in first before running this script:
#  ssh -L 3307:localhost:3306 flitman@craftmoodle70.epfl.ch
Mdl = MdlSqlConnector('db_70')
# some courses happen every two years so we need to check the relevant archive
# tunnel in first before running this script:
# ssh -L 3308:localhost:3306 flitman@cedegesrv7.epfl.ch
Archive = MdlSqlConnector('db_archive')

current_stream = open(datadir + current, "r")
current_data = json.loads(current_stream.readline())
print "%s current courses" % len(current_data)


def check_previous(shortcode):
    profs = set([])
    res_tup = Mdl.courseExistsByIdNumber(shortcode)

    # course exists with same name i.e. not in a double
    if res_tup[0] and res_tup[1]:
        profs |= set(Mdl.getEnrolledByShortnameAndRole(shortcode, 'professor'))
        print '\tpast profs {0}'.format(", ".join(str(s) for s in profs))
    # course exists but in a double
    elif res_tup[0] and not res_tup[1]:
        profs |= set(Mdl.getEnrolledByIdnumberAndRole(res_tup[2], 'professor'))
        print '\tpast profs {0}'.format(", ".join(str(s) for s in profs))
        shortcode = res_tup[2]
    # course does not exist in current moodle so check the archive for last year
    # since some courses only run once every two years
    elif not res_tup[0]:
        print('\tcourse not in this year\'s moodle so checking archive...')
        check_archive(shortcode)

    return profs, shortcode


def check_archive(shortcode):
    profs = set([])
    res_tup = Archive.courseExistsByIdNumber(shortcode)

    if res_tup[0] and res_tup[1]:
        profs |= set(Archive.getEnrolledByShortnameAndRole(shortcode, 'professor'))
        print '\tpast profs {0}'.format(", ".join(str(s) for s in profs))
    # course exists but in a double
    elif res_tup[0] and not res_tup[1]:
        profs |= set(Archive.getEnrolledByIdnumberAndRole(res_tup[2], 'professor'))
        print '\tpast profs {0}'.format(", ".join(str(s) for s in profs))
    elif not res_tup[0]:
        print '\tNot in archive. Treat as new course.'

    return profs


def course_is_valid(shortname):
    if re.search('-\d{3}', shortname):
        sectionCode = shortname[0:shortname.index('-')]
        if sectionCode not in ['EPFL', 'ETH']:  # known exceptions
            return True
        else:
            return False
    else:
        return False


def add_different_profs_csv(info):
    with open(csvdir + 'course_new_staff_' + time.strftime("%d/%m/%Y").replace('/', '-'), 'a') as changes_file:
        wr = csv.writer(changes_file, quoting=csv.QUOTE_ALL)
        wr.writerow(info)
    pass


def get_course_names(course):
    subject = course["subject"]["name"]
    if "en" in subject:
        en_name = subject["en"].encode('utf-8')
    else:
        en_name = subject["fr"].encode('utf-8')

    if "fr" in subject:
        fr_name = subject["fr"].encode('utf-8')
    else:
        fr_name = subject["en"].encode('utf-8')

    return [en_name, fr_name]


def get_emails_from_scipers(scipers):
    emails = []
    for count, sciper in enumerate(scipers, start=0):
        email = ldap.email_from_sciper(sciper)
        if email is not None:
            emails.append(email)
        else:
            email_list = Mdl.getEmailFromSciper(sciper)
            if len(email_list) > 0:
                emails.append(email_list[0])
            else:
                email_list = Archive.getEmailFromSciper(sciper)
                if len(email_list) > 0:
                    emails.append(email_list[0])
    return emails


def get_names_from_sciper(scipers):
    pass


current = {}
for course_data in current_data:
    course = course_data['course']
    if course_is_valid(course["courseCode"]):
        current[course['courseCode']] = course

for code in current.keys():
    profs = current[code]["professors"]
    current_scipers = set([])
    current_profs = []
    for prof in profs:
        current_scipers.add(prof['sciper'])
        current_profs.append(prof['fullName'])
    print code
    print '\tcurrent profs {0}'.format(", ".join(s for s in current_scipers))
    check_tup = check_previous(code)
    past_scipers = check_tup[0]
    if code != check_tup[1]:
        code = check_tup[1]

    if len(past_scipers) == 0:
        print '\tExists this year but has no profs. Checking archive...'
        past_scipers = check_archive(code)
        # if len(past_scipers) == 0:
        #     print '\tno profs in archive either.'
        #     pass

    if len(current_scipers) > 0:
        intersection = current_scipers & past_scipers
        if len(intersection) > 0:
            print('\twith {0} profs in common'.format(", ".join(s for s in intersection)))
            # do nothing
        if len(intersection) == 0 and len(past_scipers) > 0:
            print '\t{0} has different profs this year.'.format(code)
            # write email to old profs
            # get_course_professor_names(current[code])
            isEmpty = Mdl.isCourseEmptyByShortname(code)
            print isEmpty, code
            if not isEmpty:
                add_different_profs_csv([code, get_course_names(current[code]), get_emails_from_scipers(past_scipers), current_profs])
                course_staff_changes += 1
            pass

    if len(current_scipers) == 0 and len(past_scipers) > 0:
        print '\thas no current profs but had {0} previously'.format(", ".join(str(scip) for scip in past_scipers))
        # write email to old profs
        if not Mdl.isCourseEmptyByShortname(code):
            add_different_profs_csv([code, get_course_names(current[code]), get_emails_from_scipers(past_scipers), None])
            course_staff_changes += 1
        pass


print '\ntotal courses with new staff: {0}'.format(course_staff_changes)


# check for completely different set of professor between the years
# no_profs = 0
# diff_profs = 0


# notifications.info('{0} has different profs. Old: {1}. New {2})'.format(code, past_scipers, current_scipers))
"""
        for count, sciper in enumerate(past_scipers, start=0):
            email = ldap.email_from_sciper(sciper)

            if email is None:

                print 'Trying Moodle instead.'
                try:
                    email = Mdl.getEmailFromSciper(sciper)[0]
                    print past_profs[count]['fullName'].encode('utf-8')
                    print 'Found email in Moodle: ' + email
                except IndexError:
                    print past_profs[count]['fullName'].encode('utf-8')
                    print 'No email either in Moodle'

                    try:
                        email = Mdl_Archive.getEmailFromSciper(sciper)[0]
                        print 'Found email in Archive: ' + email
                    except IndexError:
                        print past_profs[count]['fullName'].encode('utf-8')
                        print 'No email either in Archive. Finally ignoring user.'
                        name = past_profs[count]['fullName'].encode('utf-8')
                        notifications.info('\t Could not find email for {3}'.format(code, past_scipers, current_scipers, name.decode('utf-8')))
                        continue
                        pass

            name = past_profs[count]['fullName'].encode('utf-8')

            if "en" in subject:
                en_name = subject["en"].encode('utf-8')
            else:
                en_name = subject["fr"].encode('utf-8')

            if "fr" in subject:
                fr_name = subject["fr"].encode('utf-8')
            else:
                fr_name = subject["en"].encode('utf-8')

            msg = writeMsg(name, en_name, fr_name, code)
            #print msg
            sendEmail(email, msg, code)
            pass
"""

































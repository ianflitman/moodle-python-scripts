__author__ = 'ian'

from connectors.MdlServiceConnector import MDLServiceConnector
from mapping.mdlServers import staging_srv_server
from mapping import AcademicCalandar
from xml.etree.ElementTree import Element as etree
import xml.etree.ElementTree as ET

myConnector = MDLServiceConnector()

params = {
 'fullname': 'Ians test course',
 'shortname': 'IAN-114',
 'categoryid': 1,
 'startdate': AcademicCalandar.Timetable['2014-2015']['ETE']['start'],
 'numsections': AcademicCalandar.Timetable['2014-2015']['ETE']['weeks']
}

users = {'courses': params}
result = myConnector.rest_protocol(staging_srv_server, users, 'core_course_create_courses')

res_tree = ET.fromstring(result)
print res_tree.tag
if res_tree.tag == 'EXCEPTION':
    print 'ErrorCode: ' + res_tree[0].text
    print 'Message: ' + res_tree[1].text
elif res_tree.tag == 'RESPONSE':
    print res_tree.find(".//KEY[@name='shortname']/VALUE").text
    print res_tree.find(".//KEY[@name='id']/VALUE").text
    print('ok')

pass

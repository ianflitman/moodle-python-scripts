__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector
from mapping.courseCategoryMapping import oldNoChildrenCatIds, oldCatToNewCat

# you need to tunnel into both servers

mdlProdConnection = MdlSqlConnector('db_prod')
mdlConnection = MdlSqlConnector('db_dev')
mdlConnection.connect_mdl()
mdlDevConnection = MdlSqlConnector('db_mdl_dev')
mdlDevConnection.connect_mdl()

doctoralCourseNum = 0

def writeToDev():
    result = mdlProdConnection.getAllShortnameAndCategory()

    for course in result:
        print course['shortname']
        # if '+' in course['shortname']:
        #     checked = False
        #     print '===split===='
        #     subcourses = course['shortname'].split('+')
        #     for subcourse in subcourses:
        #         subcourse = subcourse.strip(' ')
        #         print course['category']
        #         #if checked is False:
        #         #separate_course = {}
        #         separate_course = course
        #         separate_course['shortname'] = subcourse
        #         if checked is False:
        #             separate_course['category'] = checkCat(course) #{'shortname': subcourse, 'category': course['category']})
        #             checked = True
        #         createShortnameCategory(separate_course)
        #     print '=== end split===='
        #     checked = False
        # else:
        print course['category']
        courseCheckTuple = checkCat(course)

        if courseCheckTuple[0] is True:
            course['category'] = courseCheckTuple[1] #checkCat(course)
            createShortnameCategory(course)
        pass

    mdlDevConnection.cursor.close()
    mdlDevConnection.mdl_cnx.close()

    pass

def checkCat(course):
    if '-' not in course['shortname']:
        return (False, 0)

    if course['category'] in oldNoChildrenCatIds:
        return (True, oldCatToNewCat[str(course['category'])])
    else:
        parent = mdlProdConnection.getCategoryParent(course['category'])
        try:
            oldcat = parent[0][0]
            hypenIndex = course['shortname'].index('-')
            year_num = int(course['shortname'][hypenIndex+1:hypenIndex+2])
            if year_num <= 3:
                courseType = 'Bachelor'
            elif year_num <= 5:
                courseType = 'Master'


            if courseType != 'Bachelor' and courseType != 'Master':
                return (False, 0)
                #raise Exception('courseType neither Bachelor or Master')

            #year_num = int(course['shortname'][course['shortname'].index('-'): course['shortname'].index('-')+1])
            result = getCategoryFromChildCat(oldCatToNewCat[oldcat], courseType)
            return (True, result)
        except:
            print 'ignoring the category {}'.format(course['category'])
            return (False, course['category'])


def createShortnameCategory(course):
    if course['category'] is not None:
        create_shortname_category_query = ("INSERT INTO mdl_shortname_category (category, shortname, fullname, format) VALUES (%(category)s, %(shortname)s, %(fullname)s, %(format)s)")
        mdlDevConnection.cursor.execute(create_shortname_category_query, course)
        mdlDevConnection.mdl_cnx.commit()


def getCategoryFromChildCat(parent, courseType):

    mdlConnection.connect_mdl()
    cat_query = ("SELECT id from mdl_course_categories where parent = {0} AND name = '{1}'".format(parent, courseType))
    try:
        mdlConnection.mdl_cnx.cmd_query(cat_query)
    except:
        raise Exception('could not select id from mdl_course_categories')

    result = mdlConnection.mdl_cnx.get_rows()[0]
    mdlConnection.mdl_cnx.close()
    return int(result[0][0])

writeToDev()
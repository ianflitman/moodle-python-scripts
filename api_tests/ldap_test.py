__author__ = 'ian'
import ldap

epfl_ldap = ldap.open("ldap.epfl.ch")
epfl_ldap.simple_bind_s()

basedn = "c=ch"

cn = "Ian Anthony"
sn = "Flitman"

uid = "flitman"
mail = "ian.flitman@epfl.ch"

me_sciper = "251706"
student_sciper = "224808"
ta_sciper = "107296"
hendrik_sciper ="153087"
pierre1_sciper = "106272"
pierre2_sciper = "215168"
pierre3_sciper = "247216"
pierre4_sciper = "217285"
double_sciper = "100435"

filterD = "(|(cn=Ian Anthony)(sn=flitman))"
filterE = "(|(cn={0})(sn={1}))".format(cn, sn)
filterF = "(uniqueidentifier={0})".format(me_sciper)

studentFilter = "(uniqueidentifier={0})".format(student_sciper)
TAFilter = "(uniqueidentifier={0})".format(ta_sciper)
TAFilter2 = "(uniqueidentifier={0})".format(hendrik_sciper)
#everyone = "(uniqueidentifier=*)" 44 022 but includes departments
everyone = "(uid=*)"
double_identity = "(uniqueidentifier={0})".format(double_sciper)
no_email_sciper = "(uniqueidentifier={0})".format(252360)
not_sciper = "(uniqueidentifier={0})".format(252360999)
sacchetto = "(uniqueidentifier={0})".format(181895)
email_empty = "(uniqueidentifier={0})".format(192838)
aghazada = "(uniqueidentifier={0})".format(232889)
test_empty_email = "(uniqueidentifier={0})".format(172717)

Pierre1 = "(uniqueidentifier={0})".format(pierre1_sciper)
Pierre2 = "(uniqueidentifier={0})".format(pierre2_sciper)
Pierre3 = "(uniqueidentifier={0})".format(pierre3_sciper)
suspect = "(uniqueidentifier={0})".format(100472)
evita = "(uniqueidentifier={0})".format(254624)
moeller1 = "(uniqueidentifier={0})".format("G22521")
guest2 = "(uniqueidentifier={0})".format("G22661")
guest3 = "(uniqueidentifier={0})".format(260753)
moeller2 =  "(uniqueidentifier={0})".format(258540)
andri = "(uniqueidentifier={0})".format("G22253")
moro = "(uniqueidentifier={0})".format("257549")
email = "(mail={0})".format('ian.flitman@epfl.ch')
displayName = "(cn={0})".format('Ian Anthony Flitman')
moro_cn = "(cn={0})".format('Mariu Abritta Moro') #, 'Abritta Moro']
someone_strange = "(uniqueidentifier={0})".format('186204')
someone_strange2 = "(uniqueidentifier={0})".format('218964')
# resultsA = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, studentFilter)
# resultsB = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, TAFilter)
# resultsC = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, TAFilter2)
# resultsD = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, filterD)
# resultsE = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, filterE)
# resultsF = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, filterF)
# p1 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, Pierre1)
# p2 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, Pierre2)
# p3 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, Pierre3)
# p4 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, Pierre4)
# no_mail = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, no_email_sciper)
# empty_result = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, not_sciper)
# email_not_there = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, email_empty)
# sacchetto_res = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, sacchetto)

doubleResult = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, suspect)
noMail = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, studentFilter)#test_empty_email)
#resultMega = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, everyone)

# for result in [resultsA, resultsB, resultsC, resultsD, p1, p2, p3, doubleResult]:
#     user = {}
#     user['firstname'] = result[0][1]['cn'][0].strip(result[0][1]['cn'][len(result[0][1]['cn'])-1])#result[0][1]['givenName']
#     user['lastname'] = result[0][1]['sn']
#     user['email'] = result[0][1]['mail']
#     user['idnumber'] = result[0][1]['uniqueIdentifier']
#     user['username'] = result[0][1]['uid'][0]
#
#     pass


# if not empty_result:
#     print 'no such user found'

result1 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, someone_strange2)
strange_res = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, someone_strange)
print result1
print strange_res
moeller_1 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, moeller1)
result3 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, guest2)
moeller_2 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, moeller2)
andr_1 = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, andri)
moro_res = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, moro)
moro_name = epfl_ldap.search_s(basedn, ldap.SCOPE_SUBTREE, moro_cn)

pass



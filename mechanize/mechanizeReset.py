__author__ = 'ian'
import mechanize
import cookielib
from connectors.MdlSqlConnector import MdlSqlConnector
import logging
from mapping.constants import logs


class MechanizeMoodle:

    def __init__(self):
        print "initialising moodle.."
        # self.server = server
        # self.year = year
        # self.term = term

        self.br = mechanize.Browser(factory=mechanize.RobustFactory())
        self.br.set_handle_equiv(False)
        self.br.set_handle_robots(False)
        self.br.set_handle_referer(True)
        self.br.set_handle_redirect(True)
        self.br.set_debug_redirects(True)
        self.br.set_debug_responses(False)
        self.br.set_debug_http(False)
        self.br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=2)
        self.br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:37.0) Gecko/20100101 Firefox/37.0')]
        self.cookiejar = cookielib.LWPCookieJar()
        self.br.set_cookiejar(self.cookiejar)
        logging.basicConfig(filename='/home/ian/workspace/pyMoodleScripts/logs/mechanizeMdl.txt', level=logging.INFO)

        # needs hand-editing by checking a reset page in the current moodle version this script will be run against
        self.roles = ['No roles', 'Super teacher', 'Audit Manager', 'Hidden non-editing teacher', 'hidden teacher'
                      'Alumni', 'Student', 'Non-editing teacher', 'teacher', 'Professor', 'Manager']

        # debugging

        self.misdirections = []
        self.reset_events = []
        self.reset_logs = []
        self.reset_notes = []
        self.reset_comments = []
        self.reset_completion = []
        self.delete_blog_associations = []
        self.reset_start_date_enabled = []
        self.reset_start_date_year = []
        self.reset_start_date_month = []
        self.reset_start_date_day = []
        self.roles_deletes = []
        self.reset_gradebook_grades = []
        self.reset_groups_members = []
        self.reset_forum_all = []
        self.reset_success = []


    def login(self):
        mdl_home = self.br.open("http://mdl-dev.epfl.ch")
        #html = mdl_home.read()
        #print html
        for link in self.br.links():
            if link.url == "http://mdl-dev.epfl.ch/login/index.php":
                break

        self.br.follow_link(link)
        print(self.br.geturl())

        self.br.select_form(name="loginform")
        self.br["username"] = 'flitman'
        self.br["password"] = 'Greatshoe499'
        res = self.br.submit()
        #html = res.read()


    def courseReset(self, courseId, shortname):
        baseUrl = 'http://mdl-dev.epfl.ch/course/reset.php?id='+ courseId
        reset = self.br.open(baseUrl)
        # sometimes this shows that the page opened is not the one I asked for
        # but I can access the page directly in the browser when manually inputting the url
        # takes me to enrol page instead!
        actualURL = self.br.geturl()
        print(actualURL)

        if "enrol" in actualURL:
            logging.info('Misdirection in courseReset: {0}'.format(actualURL))
            self.misdirections.append((shortname, courseId))

        # uncomment to activate!

        for form in self.br.forms():
            print str(form.attrs["id"])
            if str(form.attrs["id"]) == "mform1":
                self.br.form = form
                try:
                    self.br.form.find_control(name='reset_events').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_events failed'.format(shortname, courseId))
                    self.reset_events.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='reset_logs').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_logs failed'.format(shortname, courseId))
                    self.reset_logs.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='reset_notes').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_notes failed'.format(shortname, courseId))
                    self.reset_notes.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='reset_comments').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_comments failed'.format(shortname, courseId))
                    self.reset_comments.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='reset_completion').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_completion failed'.format(shortname, courseId))
                    self.reset_completion.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='delete_blog_associations').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} delete_blog_associations failed'.format(shortname, courseId))
                    self.delete_blog_associations.append((courseId, shortname))

                try:
                    self.br.form.find_control(name='reset_start_date[enabled]').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_start_date failed'.format(shortname, courseId))
                    self.reset_start_date_enabled.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='reset_start_date[year]').value = ["2015"]
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_start_date year failed'.format(shortname, courseId))
                    self.reset_start_date_day.append((shortname, courseId))

                try:
                    # months need to be numbers - jan is 1
                    self.br.form.find_control(name='reset_start_date[month]').value = ["9"]
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_start_date month failed'.format(shortname, courseId))
                    self.reset_start_date_month.append((shortname, courseId))

                try:
                    self.br.form.find_control(name='reset_start_date[day]').value = ["14"]
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_start_date day failed'.format(shortname, courseId))
                    self.reset_start_date_day.append((shortname, courseId))

                # Removing everyone this list. This needs checking against the moodle instance currently
                # in use as the roles that are removable will be different for each instance depending on
                # the custom roles it has implemented.
                # Go to a Reset page and 'Inspect Element' in firefox for each option to
                # get index of the role. Here ignoring first option 'No roles'.
                # self.br.form.find_control(name='unenrol_users[]').items[0].selected = True

                # for 1 to 10 - check first in Moodle UI!
                for a in range(1, 9):
                     try:
                         self.br.form.find_control(name='unenrol_users[]').items[a].selected = True
                     except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                          logging.warn('{0}:{1} unenroll user failed for {3}'.format(shortname, courseId, self.roles[a]))
                          self.roles_deletes.append((shortname, courseId, self.roles[a]))
                #try:
                #    self.br.form.find_control(name='unenrol_users[]').items[2].selected = True
                #    self.br.form.find_control(name='unenrol_users[]').items[3].selected = True
                #    self.br.form.find_control(name='unenrol_users[]').items[4].selected = True
                #    self.br.form.find_control(name='unenrol_users[]').items[5].selected = True
                #    self.br.form.find_control(name='unenrol_users[]').items[6].selected = True
                #    self.br.form.find_control(name='unenrol_users[]').items[7].selected = True
                #    self.br.form.find_control(name='unenrol_users[]').items[8].selected = True
                #except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                #    logging.warn('{0}:{1} unenroll user failed'.format(shortname, courseId))
                #    self.roles_deletes.append((shortname, courseId))
                #self.br.form.find_control(name='unenrol_users[]').items[9].selected = True
                #self.br.form.find_control(name='unenrol_users[]').items[10].selected = True

                try:
                    # Delete grades
                    self.br.form.find_control(name='reset_gradebook_grades').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_gradebook_grades'.format(shortname, courseId))
                    self.reset_gradebook_grades.append((shortname, courseId))

                try:
                    # Delete all group members
                    self.br.form.find_control(name='reset_groups_members').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_groups_members'.format(shortname, courseId))
                    self.reset_groups_members.append((shortname, courseId))

                try:
                    # delete all post
                    self.br.form.find_control(name='reset_forum_all').items[0].selected = True
                except (mechanize.HTTPError, mechanize.LocateError, mechanize.LinkNotFoundError, mechanize.ParseError, mechanize.FormNotFoundError):
                    logging.warn('{0}:{1} reset_forum_all'.format(shortname, courseId))
                    self.reset_forum_all.append((shortname, courseId))

                # delete all quiz attempts using id here as there are two references with name='reset_quiz_attempts'
                # which throws an error
                #self.br.form.find_control(name='reset_quiz_attempts').items[0].selected = True


                response = self.br.submit()
                break


    def goToCourseRestore(self, courseId):
        baseUrl = self.server + '/course/view.php?id='
        self.br.open(baseUrl + courseId)
        print(self.br.geturl())

        for link in self.br.links():
            print link
            if link.text == "Restore":
                break

        self.br.follow_link(link)
        print(self.br.geturl())

        for form in self.br.forms():
            if str(form.attrs["id"]) == "mform1":
                self.br.form = form
                break

        for link in self.br.links():
            print link

        backup_choose = self.br.form.find_control(name="backupfilechoose")
        #backup_choose


    def goToCoursePageBackUp(self, courseId):
        baseUrl = "http://moodle.epfl.ch/backup/backup.php?id="
        self.br.open(baseUrl + courseId)
        print(self.br.geturl())

        for form in self.br.forms():
            if str(form.attrs["id"]) == "mform1":
                self.br.form = form
                break

        include_users_checkbox = self.br.form.find_control(name="setting_root_users")
        print include_users_checkbox.items[0].selected
        include_users_checkbox.items[0].selected = False
        res = self.br.submit()  # Next
        html = res.read()
        print(self.br.geturl())
        print html

        for form in self.br.forms():
            if str(form.attrs["id"]) == "mform2":
                self.br.form = form
                break

        res = self.br.submit()  # Next

        # keep on the mform2 on this next page
        for form in self.br.forms():
            if str(form.attrs["id"]) == "mform2":
                self.br.form = form
                break


        backup_name_input = self.br.form.find_control(name="setting_root_filename")
        print backup_name_input.value
        backup_name_input.value = "Toms Big Bollocks"
        print backup_name_input.value
        page = self.br.submit()  # Perform Backup
        html = page.read()
        print html
        # do a loop to check for
        # The backup file was successfully created
        pass

    def pollForCompletion(self, page):
        pass


myMoodle = MechanizeMoodle()#'http://moodle-dev.epfl.ch', '2014-2015', 'ETE')


myMoodle.login()
mdlConnector = MdlSqlConnector('db_dev') #('db_prod')
#myMoodle.courseReset('14478')

# 141073200 is Sept 15th 2014 in unix time
# select COUNT(*) from mdl_course where startdate = 1410732000; (530)
# 1424041200 is 16th February 2015
# select COUNT(*) from mdl_course where startdate = 1424041200; (460)

lastTermCourseIds = mdlConnector.getCourseIdsFromStartDate(1410732000)
for id in lastTermCourseIds:
    print id[0]
    myMoodle.courseReset(id[0], id[1])
pass
#myMoodle.courseReset(str(518))

#518 - OK
#14084
#14026
#14084






__author__ = 'ian'
from mysql.connector import Error as MySQLError
from mysql.connector import connect
from mysql.connector.connection import MySQLConnection
from mysql.connector.errors import Error
from mapping.mdlServers import servers #db_prod, db_dev
import datetime
import time

# for db_prod
# ssh tunnel in thus first before running this script if running outside the server itself
# ssh -L 3307:craftmoodle50.epfl.ch:3306 -i /home/ian/Documents/ssh_keys/openssh_ianflitman_privatekey kis@craftmoodle30.epfl.ch
# ssh -L 3307:localhost:3306  flitman@craftmoodle70.epfl.ch

# for db_dev
# ssh tunnel in thus first before running this script if running outside the server itself
# ssh -L 3308:localhost:3306 -i /home/ian/.ssh/id_rsa root@mdl-dev.epfl.ch


class MdlSqlConnector:

    def __init__(self, db='db_dev'):
        self.mdl_cnx = MySQLConnection()
        self.server = servers[db]
        pass


    def connect_mdl(self):
        try:
            self.mdl_cnx = connect(user=self.server['user'], password=self.server['password'], host=self.server['host'],
                                   port=self.server['port'], database=self.server['database'])
        except Error:
            print 'Connection failed'
            raise Error

        self.cursor = self.mdl_cnx.cursor()

    # when created courses have shortnames and idnumbers identical
    def courseExistsByShortname(self, shortname):
        self.connect_mdl()
        exists_query = ("SELECT EXISTS(SELECT 1 from mdl_course WHERE shortname = '{0}')".format(shortname.encode('utf-8')))
        try:
            self.mdl_cnx.cmd_query(exists_query)
        except:
            return 0
        result = self.mdl_cnx.get_rows()[0][0][0]
        return int(result)


    def courseExistsByIdNumber(self, shortname):
        self.connect_mdl()
        exists_query = ("SELECT idnumber from mdl_course WHERE idnumber LIKE '%{0}%'".format(shortname.encode('utf-8')))
        try:
            self.cursor.execute(exists_query)
        except MySQLError as err:
            raise MySQLError("error for courseExistsByIdNumber with shortname: {0}. Error num: {1}. Msg: {2}. SQLState {3}".
                             format(shortname, err.errno, err.message, err.sqlstate))

        exists = False
        idnumber = shortname
        same_name = None

        for result in self.cursor:
            if result[0] != shortname:
                if result[0].endswith(''.join(['$', shortname])) or result[0].startswith(''.join([shortname, '$'])):
                    print '\t\t\t{0} with course:{1}'.format(shortname, result[0])
                    exists = True
                    same_name = False
                    idnumber = result[0]
                    break

            else:
                #print '\t\t\t{0} has own name course'.format(shortname)
                exists = True
                same_name = True
                idnumber = result[0]
                break

        return exists, same_name, idnumber


    #returns array of tuples [(id,category)]
    def getIdCategoryFromShortname(self, shortname):
        self.connect_mdl()
        cat_query = ("SELECT id, category from mdl_course WHERE shortname='{0}'".format(shortname.encode('utf-8')))
        try:
            self.cursor.execute(cat_query)
        except:
            pass
        result = []

        for(id, category) in self.cursor:
            print id, category
            result.append((id, category))

        self.cursor.close()
        self.mdl_cnx.close()

        if len(result) != 1:
            self.file = open('logs/category_log.txt', 'a')
            self.file.write(shortname + ' has more than on category: {0}'.format(len(result))) if len(result) > 1 else self.file.write(shortname.encode('utf8') + ' missing\n')

        return result


    def getAllShortnameAndCategory(self):
        self.connect_mdl()
        short_cat_query = ("SELECT shortname, category, fullname, format from mdl_course WHERE 1")
        try:
            self.cursor.execute(short_cat_query)
        except MySQLError as err:
            print err.message

        result = []

        for(shortname, category, fullname, format) in self.cursor:
            print shortname, category
            result.append({'shortname': shortname, 'category': category, 'fullname': fullname, 'format': format})

        self.cursor.close()
        self.mdl_cnx.close()

        return result


    def sciperFromNames(self, firstname, lastname):
        self.connect_mdl()
        sciper_query = ("SELECT idnumber, id from mdl_user WHERE firstname='{0}' AND lastname='{1}'".format(firstname, lastname))
        self.cursor.execute(sciper_query)
        for(idnumber,id) in self.cursor:
           sciper = (idnumber, id)
        self.cursor.close()
        self.mdl_cnx.close()
        return sciper


    def removeStudentFromCourseBySciper(self, sciper, shortname):
        self.connect_mdl()
        remove_query = ("DELETE ue "
                        "FROM mdl_user u "
                        "INNER  JOIN mdl_user_enrolments ue ON ue.userid = u.id "
                        "INNER  JOIN mdl_enrol e ON e.id = ue.enrolid "
                        "INNER  JOIN mdl_course c ON e.courseid = c.id "
                        "WHERE u.idnumber = '{0}' AND c.shortname ='{1}'".format(sciper, shortname))

        self.cursor.execute(remove_query)

        remove_role_assignments = ("DELETE ra "
                                   "FROM mdl_user u "
                                   "INNER JOIN mdl_role_assignments ra ON ra.userid = u.id "
                                   "INNER JOIN mdl_context mc ON mc.id = ra.contextid "
                                   "INNER JOIN mdl_course c ON c.id = mc.instanceid "
                                   "where c.shortname = '{0}' AND ra.roleid = 41 AND u.idnumber ='{1}'".format(shortname, sciper))

        self.cursor.execute(remove_role_assignments)

        self.mdl_cnx.commit()
        self.cursor.close()
        self.mdl_cnx.close()


    def removeFromCourseByRoleAndSciper(self, shortname, role, sciper):

        role_identifier = self.checkRole(role)

        self.connect_mdl()
        remove_query = ("DELETE ue "
                        "FROM mdl_user u "
                        "INNER  JOIN mdl_user_enrolments ue ON ue.userid = u.id "
                        "INNER  JOIN mdl_enrol e ON e.id = ue.enrolid "
                        "INNER  JOIN mdl_course c ON e.courseid = c.id "
                        "WHERE u.idnumber = '{0}' AND c.shortname ='{1}'".format(sciper, shortname))

        self.cursor.execute(remove_query)

        remove_role_assignments = ("DELETE ra "
                                   "FROM mdl_user u "
                                   "INNER JOIN mdl_role_assignments ra ON ra.userid = u.id "
                                   "INNER JOIN mdl_context mc ON mc.id = ra.contextid "
                                   "INNER JOIN mdl_course c ON c.id = mc.instanceid "
                                   "where c.shortname = '{0}' AND ra.roleid = '{1}' AND u.idnumber ='{2}'".format(shortname, role_identifier, sciper))

        self.cursor.execute(remove_role_assignments)

        self.mdl_cnx.commit()
        self.cursor.close()
        self.mdl_cnx.close()


    def enrollUserByShortnameRoleAndSciper(self, shortname, role, sciper):
        role_identifier = self.checkRole(role)
        self.connect_mdl()
        """
        SELECT * FROM `mdl_enrol` me
        where courseid = (select id from mdl_course where shortname = 'EE-456')
        and enrol = 'database'
        """
        get_user_id = ("SELECT id from mdl_user where idnumber ='{0}'".format(sciper))

        self.mdl_cnx.cmd_query(get_user_id)
        id_result = self.mdl_cnx.get_rows()[0][0]
        if len(id_result) > 1:
            raise Exception('More than one user with this sciper. Aborting enrolling them into {0}'.format(shortname))
        else:
            user_id = id_result[0]

        get_enrol_data_query = ("SELECT id, courseid from mdl_enrol where courseid = (SELECT id from mdl_course where shortname = '{0}'"
                                " and enrol = 'database')".format(shortname))

        self.mdl_cnx.cmd_query(get_enrol_data_query)
        result = self.mdl_cnx.get_rows()[0][0]

        user_enrolment = {
            'enrolid': result[0],
            'status': 0,
            'user_id': user_id,
            'timestart': time.gmtime(),
            'timeend': 0,
            'modifierid': 251706,
            'timecreated': time.gmtime(),
            'timemodified': time.gmtime()
        }

        #mdl_user_enrollment_creation = ("INSERT into mdl_user_enrolments ")


        self.mdl_cnx.close()

        pass

    # def removeRoleAssignmentBySciper(self, sciper, shortname):
    #     self.connect_mdl()
    #     remove_role_assignments = ("DELETE ra "
    #                                "FROM mdl_user u "
    #                                "INNER JOIN mdl_role_assignments ra ON ra.userid = u.id "
    #                                "INNER JOIN mdl_context mc ON mc.id = ra.contextid "
    #                                "INNER JOIN mdl_course c ON c.id = mc.instanceid "
    #                                "where c.shortname = '{0}' AND ra.roleid = 41 AND u.idnumber ='{1}'".format(shortname, sciper))
    #
    #     self.cursor.execute(remove_role_assignments)
    #     self.mdl_cnx.commit()
    #     self.cursor.close()
    #     self.mdl_cnx.close()


    def getUsersEnrolledByCourseShortname(self, shortname):
        self.connect_mdl()
        get_users_query = ("SELECT u.idnumber "
                           "FROM mdl_user u "
                           "INNER JOIN mdl_user_enrolments ue ON ue.userid = u.id "
                           "INNER JOIN mdl_enrol e ON e.id = ue.enrolid "
                           "INNER JOIN mdl_course c ON  c.id = e.courseid "
                           "WHERE c.shortname = '{0}'".format(shortname))

        self.mdl_cnx.cmd_query(get_users_query)
        user_ids = []

        for row in self.mdl_cnx.get_rows()[0]:
            user_ids.append(row[0])

        self.mdl_cnx.close()
        return user_ids



    def getAutoEnrolledByCoursShortnameAndRole(self, shortname, role):
        self.connect_mdl()
        self.checkRole(role)

        get_autoenrolled_query = ("SELECT userid from mdl_autoenroll "
                                  "WHERE courseId = '{0}' and rolename = '{1}'".format(shortname, role))

        user_ids = []

        self.mdl_cnx.cmd_query(get_autoenrolled_query)
        for row in self.mdl_cnx.get_rows()[0]:
            user_ids.append(row[0])

        self.mdl_cnx.close()
        return user_ids




    def getEnrolledByShortnameAndRole(self, shortname, role):

        role_identifier = self.checkRole(role)

        self.connect_mdl()
        
        get_students_query = ("SELECT u.idnumber "
                              "FROM mdl_user u "
                              "INNER JOIN mdl_role_assignments ra ON ra.userid =  u.id "
                              "INNER JOIN mdl_context mc ON mc.id = ra.contextid "
                              "INNER JOIN mdl_course c ON c.id = mc.instanceid "
                              "where c.shortname = '{0}' and ra.roleid = {1}".format(shortname, role_identifier))

        user_ids = []

        self.mdl_cnx.cmd_query(get_students_query)
        for row in self.mdl_cnx.get_rows()[0]:
            user_ids.append(row[0])

        self.mdl_cnx.close()
        return user_ids


    def getEnrolledByIdnumberAndRole(self, shortname, role):

        role_identifier = self.checkRole(role)

        self.connect_mdl()

        get_students_query = ("SELECT u.idnumber "
                              "FROM mdl_user u "
                              "INNER JOIN mdl_role_assignments ra ON ra.userid =  u.id "
                              "INNER JOIN mdl_context mc ON mc.id = ra.contextid "
                              "INNER JOIN mdl_course c ON c.id = mc.instanceid "
                              "where c.idnumber = '{0}' and ra.roleid = {1}".format(shortname, role_identifier))

        user_ids = []

        self.mdl_cnx.cmd_query(get_students_query)
        for row in self.mdl_cnx.get_rows()[0]:
            user_ids.append(row[0])

        self.mdl_cnx.close()
        return user_ids

    """
    "SELECT ra.userid "
                        "FROM mdl_role_assignments ra "
                        "INNER JOIN mdl_context mc ON mc.id = ra.contextid "
                        "INNER JOIN mdl_course c ON  c.id = mc.instanceid "
                        "where c.shortname = {0} and where ra.roleid = {1}".format(shortname, 41))


        SELECT ra.userid FROM mdl_role_assignments ra
        INNER JOIN mdl_context mc ON mc.id = ra.contextid
        INNER JOIN mdl_course c ON c.id = mc.instanceid
        where c.shortname = 'AR-301(c)' and ra.roleid = 41

        SELECT u.idnumber
        FROM mdl_user u
        INNER JOIN mdl_role_assignments ra ON ra.userid =  u.id
        INNER JOIN mdl_context mc ON mc.id = ra.contextid
        INNER JOIN mdl_course c ON c.id = mc.instanceid
        where c.shortname = 'AR-301(c)' and ra.roleid = 41

    """

    def getAutoEnrolledByShortnameAndRole(self, shortname, role):

        # do not use role_identifier return
        self.checkAutoRole(role)

        self.connect_mdl()
        auto_enroll_user_query = ("SELECT userid from mdl_autoenroll WHERE courseId='{0}' and rolename='{1}'".format(shortname, role))

        self.mdl_cnx.cmd_query(auto_enroll_user_query)
        user_ids = []

        for row in self.mdl_cnx.get_rows()[0]:
            # decode to utf-8 or you get bytearray members instead of strings on prod!
            user_ids.append(row[0].decode("utf-8"))

        self.mdl_cnx.close()
        return user_ids

    def getEmailFromSciper(self, sciper):
        self.connect_mdl()
        email_query = ("SELECT email from mdl_user WHERE idnumber='{0}'".format(sciper))
        self.mdl_cnx.cmd_query(email_query)
        email = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        try:
            return email[0]
        except IndexError:
            return []
            pass
            #raise IndexError


    def autoEnrollUserBySciperShortnameAndRole(self, user_params):
        self.connect_mdl()

        autoenroll_query = ("INSERT into mdl_autoenroll "
                            "(userid, courseId, rolename) "
                            "VALUES (%(sciper)s, %(shortname)s, %(role)s)")
        try:
            self.cursor.execute(autoenroll_query, user_params)
            self.mdl_cnx.commit()
            self.cursor.close()
            self.mdl_cnx.close()
        except MySQLError as err:
            raise MySQLError("autoEnrollUserBySciperShortnameAndRole failed. Error num: {0}. Msg: {1}. SQLState: {2}"
                             .format(err.errno, err.message, err.sqlstate))


    def unAutoEnrollUserBySciperShortnameAndRole(self, user_params):

        self.connect_mdl()

        unenroll_query = ("DELETE from mdl_autoenroll "
                          "where userid = '{0}' and courseId = '{1}' and rolename = '{2}'"
                          .format(user_params['sciper'], user_params['shortname'], user_params['role']))
        try:
            self.cursor.execute(unenroll_query, user_params)
            self.mdl_cnx.commit()
            self.cursor.close()
            self.mdl_cnx.close()
        except MySQLError as err:
            raise MySQLError("unAutoEnrollUserBySciperShortnameAndRole failed. Error num: {0}. Msg: {1}. SQLState: {2}"
                             .format(err.errno, err.message, err.sqlstate))


    def checkRole(self, role):
        if role == 'student':
            role_identifier = 41
        elif role == 'professor':
            role_identifier = 91
        elif role == 'assistant':
            role_identifier = 21
        else:
            raise Error("Role not student, teacher or assistant in getEnrolledByCourseShortnameAndRole: {0}".format(role))

        return role_identifier

    def checkAutoRole(self, role):
        if role not in ['professor','editingteacher', 'student']:
            raise Error("Role {0} not student, editingteacher or profesessor in autoEnroll method called".format(role))

    def removeStudentFromCourseByNames(self, firstname, lastname, shortcode):
        sciper = self.sciperFromNames(firstname, lastname)
        self.removeStudentFromCourseBySciper(sciper[0], shortcode)


    def userExistsWithSciper(self, sciper):
        self.connect_mdl()
        sciper_user_query = ("SELECT * FROM mdl_user where idnumber = '{0}'".format(sciper))
        self.mdl_cnx.cmd_query(sciper_user_query)
        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        if not result:
            return False
        else:
            return True

    def createUser(self, user_params):
        self.connect_mdl()
        create_user_query = ("INSERT into mdl_user "
                            "(username, firstname, lastname, email, idnumber, auth, confirmed, mnethostid) "
                            "VALUES (%(username)s, %(firstname)s, %(lastname)s, %(email)s, %(idnumber)s, %(auth)s, %(confirmed)s, %(mnethostid)s)")
        try:
            self.cursor.execute(create_user_query, user_params)
        except (MySQLError, Error) as err:
            print err.message

        self.mdl_cnx.commit()
        self.cursor.close()
        self.mdl_cnx.close()


    def createCourse(self, course_params, section_num):
        self.connect_mdl()
        create_course_query = ("INSERT INTO mdl_course "
                              "(category, shortname, idnumber, format, startdate, visible) "
                              "VALUES (%s, %s, %s, %s, %s, %s)")
        try:
            self.cursor.execute(create_course_query, course_params)
        except (MySQLError, Error) as err:
            print err.message

        self.mdl_cnx.commit()
        row_id = self.cursor.lastrowid

        num_weeks_query = "INSERT INT0 mdl_course_format_options(courseid, format, name, value) " \
                          " VALUES(%s, %s, %s, %s)"

        inserts = [(row_id, 'weeks', 'numsections', course_params['numsections']),
                   (row_id, 'weeks', 'hiddensections', 1),
                   (row_id, 'weeks', 'coursedisplay', 0)]

        try:
            self.cursor.executemany(num_weeks_query, inserts)
        except (MySQLError, Error, Exception) as err:
            print err.message

        self.cursor.close()
        self.mdl_cnx.commit()
        self.mdl_cnx.close()
        return True


    def hideCourseByShortname(self, shortname):
        self.connect_mdl()
        hide_query = ("UPDATE mdl_course SET visible = 0 WHERE shortname = '{0}'".format(shortname))
        self.mdl_cnx.cmd_query(hide_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()

    def accountDetailsFromSciper(self, sciper):
        self.connect_mdl()
        sciper_user_details_query = ("SELECT username, firstname, lastname, email FROM mdl_user where idnumber = '{0}'".format(sciper))
        self.mdl_cnx.cmd_query(sciper_user_details_query)
        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        return result


    def updateUserDetails(self, idnumber, field, newValue):
        self.connect_mdl()
        update_query = ("UPDATE mdl_user SET {0} = '{1}' WHERE idnumber = '{2}'".format(field, newValue.encode('utf-8'), idnumber))
        self.mdl_cnx.cmd_query(update_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()


    def resetCourseStartDates(self, oldStart, newStart):
        self.connect_mdl()
        reset_date_query = ("UPDATE mdl_course SET startdate = {1} WHERE startdate = {0}".format(oldStart, newStart))
        self.mdl_cnx.cmd_query(reset_date_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()
        pass

    def toggleVisibilityByDate(self, unixdate, show):
        self.connect_mdl()

        if show is True:
            show_query = ("UPDATE mdl_course SET visible = 1 WHERE startdate = {0}".format(unixdate))
        else:
            show_query = ("UPDATE mdl_course SET visible = 0 WHERE startdate = {0}".format(unixdate))

        self.mdl_cnx.cmd_query(show_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()


    def getCoursesByStartDate(self, startDate):
        self.connect_mdl()
        start_date_query = ("SELECT shortname from mdl_course where startdate ={0}".format(startDate))
        self.mdl_cnx.cmd_query(start_date_query)
        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        return result


    """
        Clears all enrolments,
                   forum posts,
                   gradebook entries,
                   quiz attempts
                   group members
                   for all courses

                   NB could add feedback table for surverys to this list

    """
    # mdl_assign_submission
    def truncateTablesForReset(self):
        self.connect_mdl()
        truncated_tables = ['mdl_user_enrolments', 'mdl_role_assignments', 'mdl_autoenroll', 'mdl_forum_discussions', 'mdl_forum_posts', 'mdl_forum_read',
                            'mdl_forum_digests', 'mdl_grade_grades', 'mdl_grade_grades_history', 'mdl_quiz_grades',
                            'mdl_quiz_attempts', 'mdl_quiz_overview_regrades', 'mdl_groups_members', 'mdl_feedback_completed', 'mdl_feedback_completedtmp']

        for table in truncated_tables:
            truncate_query = ("TRUNCATE TABLE {0}".format(table))
            self.mdl_cnx.cmd_query(truncate_query)
            self.mdl_cnx.commit()

        self.mdl_cnx.close()


    def deleteUser(self, idnumber):
        self.connect_mdl()
        get_id_query = ("SELECT id from mdl_user where idnumber = '{}'".format(idnumber))
        self.mdl_cnx.cmd_query(get_id_query)
        try:
            result = int(self.mdl_cnx.get_rows()[0][0][0]) # get first entry of first array tuple!
        except Exception:
            rows = self.mdl_cnx.get_rows()
            raise MySQLError('error deleting user with sciper: {0}'.format(idnumber))

        self.mdl_cnx.close()
        course_enrolments = self.getCourseEnrolmentShortNamesById(result)

        for course in course_enrolments:
            print course[0]
            self.removeStudentFromCourseBySciper(idnumber, course[0])

        self.connect_mdl()
        delete_query = ("DELETE from mdl_user where idnumber = '{0}'".format(idnumber))
        self.mdl_cnx.cmd_query(delete_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()

        return course_enrolments

    # some users have duplicate accounts once they change their username
    # the account is auto-created once they use their new log in
    # this function is for debugging purposes
    def deleteAccountWithOldUsername(self, idnumber, username):
        self.connect_mdl()
        delete_duplicate_account_query = ("DELETE from mdl_user where idnumber = '{0}' and where username = '{1}'".format(idnumber, username))
        self.mdl_cnx.cmd_query(delete_duplicate_account_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()

    def getLastLoginByUsername(self, username):
        self.connect_mdl()
        last_login_query = ("SELECT lastlogin from mdl_user where username='{0}'".format(username))
        self.mdl_cnx.cmd_query(last_login_query)
        result = self.mdl_cnx.get_rows()[0][0][0]
        self.mdl_cnx.close()
        return int(result)

    def getAccountCreationTime(self, username):
        self.connect_mdl()
        time_created_query = ("SELECT timecreated from mdl_user where username='{0}'".format(username))
        self.mdl_cnx.cmd_query(time_created_query)
        result = self.mdl_cnx.get_row()[0][0]
        datecreated = datetime.datetime.fromtimestamp(int(result)).strftime('%Y-%m-%d %H:%M:%S')
        return datecreated


    def removeUser(self, idnumber):
        self.connect_mdl()
        delete_query = ("DELETE from mdl_user where idnumber = '{0}'".format(idnumber))
        self.mdl_cnx.cmd_query(delete_query)
        self.mdl_cnx.commit()
        self.mdl_cnx.close()


    def getCourseEnrolmentShortNamesById(self, user_id):
        self.connect_mdl()
        course_query = ("SELECT DISTINCT c.shortname "
                        "from mdl_course "
                        "INNER JOIN mdl_user_enrolments ue on ue.userid ={0} "
                        "INNER JOIN mdl_enrol e on e.id = ue.enrolid "
                        "INNER JOIN mdl_course c on c.id = e.courseid".format(user_id))

        self.mdl_cnx.cmd_query(course_query)
        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        return result


    def getCategoryParent(self, courseCat):
        self.connect_mdl()
        cat_query = ("SELECT parent from mdl_course_categories where id = {0} ".format(courseCat))
        self.mdl_cnx.cmd_query(cat_query)
        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        return result


    def getCategoryFromChildCat(self, parent, courseType):
        self.connect_mdl()
        cat_query = ("SELECT id from mdl_course_categories where parent = {0} AND name = '{1}'".format(parent, courseType))
        try:
            self.mdl_cnx.cmd_query(cat_query)
        except:
            raise Error
            pass

        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        return result


    def duplicateAccountNumByName(self, firstname, lastname):
        accounts = []
        self.connect_mdl()
        dup_query = ("SELECT idnumber, username, timecreated, email from mdl_user where firstname = '{0}' AND lastname = '{1}'".format(firstname.encode('utf-8'), lastname.encode('utf-8')))
        self.cursor.execute(dup_query)
        for row in self.cursor:
            datecreated = datetime.datetime.fromtimestamp(int(row[2])).strftime('%Y-%m-%d %H:%M:%S')
            if row[0] is u'':
                account = (row[1], None, datecreated, row[3])
            else:
                account = (row[1], row[0], datecreated, row[3])

            accounts.append(account)

        self.mdl_cnx.close()
        return accounts

    #ssh -L 3307:localhost:3306 -i /home/ian/.ssh/id_rsa flitman@craftmoodle70.epfl.ch

    def getAutoCreatedDeleteDups(self):

        self.connect_mdl()
        account_query = ("SELECT idnumber, id from mdl_user where timecreated = 0 AND lastlogin = 0")
        self.cursor.execute(account_query)

        for row in self.cursor:
            dup_account_query = ("SELECT id from mdl_user where idnumber='{0}' and timecreated != 0".format(row[0]))
            mdl_cnx = connect(user=self.server['user'], password=self.server['password'], host=self.server['host'], port=self.server['port'], database=self.server['database'])
            mdl_cursor = mdl_cnx.cursor()
            mdl_cursor.execute(dup_account_query)

            for dup in mdl_cursor:
                delete_cnx = connect(user=self.server['user'], password=self.server['password'], host=self.server['host'], port=self.server['port'], database=self.server['database'])
                delete_query = ("DELETE from mdl_user where id={0}".format(row[1]))
                delete_cursor = delete_cnx.cursor()
                delete_cursor.execute(delete_query)
                print dup, row[0]
                delete_cnx.commit()
                delete_cnx.close()

            mdl_cnx.close()

        self.mdl_cnx.close()


    def getCourseIdsFromStartDate(self, unixStart):
        self.connect_mdl()
        courseIds_query = ("select id, shortname from mdl_course where startdate = {0}".format(unixStart))
        self.mdl_cnx.cmd_query(courseIds_query)
        result = self.mdl_cnx.get_rows()[0]
        self.mdl_cnx.close()
        return result


    def getEmptyCoursesByYearMonth(self, year, month):
        self.connect_mdl()
        isEmptyQuery = ("SELECT c.id, c.idnumber, c.count_modules, c.fullname, YEAR(FROM_UNIXTIME(c.startdate)) as startyear, MONTH(FROM_UNIXTIME(c.startdate)) as startmonth, r.name, u.email "
                        "FROM (SELECT mc.id, mc.fullname, mc.idnumber, mc.visible, mc.startdate, COUNT(mm.id) AS count_modules "
                        "FROM mdl_course mc INNER JOIN mdl_course_modules mcm ON (mc.id = mcm.course) "
                        "INNER JOIN mdl_modules mm ON (mcm.module = mm.id) "
                        "WHERE mc.id <> 1 GROUP BY mc.fullname, mc.idnumber ORDER BY 3 ASC) as c "
                        "JOIN mdl_context ct ON c.id = ct.instanceid "
                        "JOIN mdl_role_assignments ra ON ra.contextid = ct.id "
                        "JOIN mdl_user u ON u.id = ra.userid "
                        "JOIN mdl_role r ON r.id = ra.roleid "
                        "WHERE r.id IN (21, 91) AND c.count_modules > 1 AND c.visible = 0 "
                        "AND YEAR(FROM_UNIXTIME(c.startdate)) = {0} AND MONTH(FROM_UNIXTIME(c.startdate)) = {1}".format(year, month))

        self.cursor.execute(isEmptyQuery)
        result = []
        for row in self.cursor:
            result.append(row)

        return result


    def isCourseEmptyByShortname(self, shortname):
        self.connect_mdl()
        module_query = ("SELECT COUNT(mcm.module) FROM mdl_course mc "
                        "INNER JOIN mdl_course_modules mcm ON mc.id = mcm.course "
                        "INNER JOIN mdl_modules mm ON mcm.module = mm.id "
                        "where mc.shortname = '{0}'".format(shortname))

        self.mdl_cnx.cmd_query(module_query)
        result = int(self.mdl_cnx.get_rows()[0][0][0])
        self.mdl_cnx.close()
        if result > 1:
            return False
        else:
            return True


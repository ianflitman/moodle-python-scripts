# -*-coding: utf-8 -*-
from __future__ import unicode_literals
import json
import re
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.LDAPConnector import LDAPConnector
from mailer import Mailer
from mailer import Message
import logging
import time


mailer = Mailer('mail.epfl.ch')
ldap = LDAPConnector()


def create_logger(logger_name, log_file, level=logging.INFO):
        l = logging.getLogger(logger_name)
        l.setLevel(level)
        fileHandler = logging.FileHandler(log_file, mode='w')
        l.addHandler(fileHandler)
        return l


def sendEmail(address, msg, code):
    my_address = "ian.flitman@epfl.ch"
    message = Message(From="ian.flitman@epfl.ch", To=address, charset="utf-8")
    message.Subject = "Removing content from your old Moodle course {0}".format(code)
    message.Body = msg
    try:
        mailer.send(message)
        notifications.info(' Notification email sent to {0} about {1}'.format(address, code))
    except:
        print 'recipient failed for {1} with course {}'.format(address, code)
        pass


def writeMsg(name, en_course_name, fr_course_name, code):

    msg = """
Dear {0},

While preparing Moodle for the new term beginning on February 22nd 2016, I noticed that your course, '{1}' ({3}) appears to be no longer taught by you.

This might be because your name is simply missing at present from the Is Academia record. If so, you should update the record for the course in Is Academia through your section.

However, if you taught this course and it was populated with your content in Moodle, you may not want it to be used by the new professor or teaching team taking over from you.

In this case, if you wish we can delete the content of the course for you. If you are concerned about losing the content, you will be pleased to know that we already have an archived copy of the course available at http://moodlearchive.epfl.ch in case you need to recover the material at a later date, or reuse it in another course within Moodle.

If I do not hear from you, I will do nothing and leave the Moodle course as it currently is.

best wishes,

Ian Flitman
Moodle Administrator


===================== en français ========================

{0},

En faisant la préparation du site Moodle pour le début du semestre de printemps qui débutera le 22 février 2016, j’ai remarqué que le cours '{2}' ({3}) semblerait ne plus être enseigné par vous.

Il se peut que cela vienne du fait que votre nom ne soit pas inscrit dans IS Academia pour l’enseignement de ce cours. Si c’est le cas, merci de faire le nécessaire auprès de votre section.

Par contre, si vous avez enseigné ce cours, que la page Moodle contient vos documents de cours et que vous ne souhaitez pas que le nouvel enseignant utilise ce matériel, merci de me contacter afin que je puisse effacer le contenu de la page. Si vous êtes soucieux de conserver le matériel de ce cours, nous vous informons qu’une copie de cette page et de son contenu est disponible dans les archives Moodle (http://moodlearchive.epfl.ch/).

Sans nouvelle de votre part, aucune mesure ne sera prise et la page sera laissée telle quelle.

Avec mes meilleures salutations

Ian Flitman
Administrateur Moodle

""".encode('utf-8').format(name, en_course_name, fr_course_name, code)

    return msg

# ssh -L 3307:localhost:3306  flitman@craftmoodle70.epfl.ch
# then supply passwd prior to running script

# ssh -L 3308:localhost:3306  root@cedegesrv1.epfl.ch
# then supply passwd prior to running script

# for current data curl the json service endpoint thus:
# curl --header "Accept:application/json" "https://isa.epfl.ch/services/courses/2015-2016/ETE" > /home/ian/moodleDatas/ete_2016_Jan12.json

logdir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/logs/"
notifications = create_logger('course_log', logdir + 'Course_Content_Notifications_' + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt')
datadir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/datadir/"
#current = "hiver_2015.json"
#previous = "hiver_2014.json"

current = "hiver_2016_10June.json" #"ete_2016_Jan12.json"
previous = "hiver_2015_10June.json" # "ete_2015.json"

Mdl = MdlSqlConnector('db_70')
#Mdl_Archive = MdlSqlConnector('db_archive')

current_stream = open(datadir + current, "r")
current_data = json.loads(current_stream.readline())

print "%s current courses" % len(current_data)

previous_stream = open(datadir + previous, "r")
previous_data = json.loads(previous_stream.readline())
print "%s previous courses" % len(previous_data)


def course_isValid(shortname):
    if re.search('-\d{3}', shortname):
        sectionCode = shortname[0:shortname.index('-')]
        if sectionCode not in ['EPFL', 'ETH']:  # known exceptions
            return True
        else:
            return False
    else:
        return False


current = {}
for course_data in current_data:
    course = course_data['course']
    if course_isValid(course["courseCode"]):
        current[course['courseCode']] = course

past = {}
for course_data in previous_data:
    course = course_data['course']
    if course_isValid(course['courseCode']):
        past[course['courseCode']] = course


# check for current courses without professors

for code in current.keys():
    profs = current[code]["professors"]
    if len(profs) == 0:
        print '{0} has no profs this term'.format(code)
        pass
        if code not in past:
            pass
            print 'But it did not exist last year. Create it, then do nothing.'
            print "course:{0} exists?: {1} ".format(code, Mdl.courseExistsByIdNumber(code))
        else:
            past_profs = past[code]["professors"]
            if len(past_profs) == 0:
                print 'And no one for it last year. Create it, then do nothing.'
                print "course:{0} exists?: {1} ".format(code, Mdl.courseExistsByIdNumber(code))
                pass
            else:
                print 'But had these last year: {0}. Contact them.'.format(past_profs)
                print "course:{0} exists?: {1} ".format(code, Mdl.courseExistsByIdNumber(code))
                pass


# check for completely different set of professor between the years
no_profs = 0
diff_profs = 0

for code in current.keys():
    profs = []
    past_profs = []

    profs = current[code]["professors"]
    subject = current[code]["subject"]["name"]
    if code not in past:
        pass
        print '{0} is new, and has these profs: {1}. Create it, then do nothing.'.format(code, profs)
        continue
    else:
        past_profs = past[code]["professors"]

    current_scipers = []
    for prof in profs:
        current_scipers.append(prof["sciper"])

    past_scipers = []
    for prof in past_profs:
        past_scipers.append(prof["sciper"])

    intersection = set(current_scipers) & set(past_scipers)


    if len(intersection) == 0 and len(past_scipers) > 0:
        print '\t{0} has different profs. Old: {1}. New {2})'.format(code, past_scipers, current_scipers)
        #past_profs[0]['fullName'].encode('utf-8')
        notifications.info('{0} has different profs. Old: {1}. New {2})'.format(code, past_scipers, current_scipers))
        """
        for count, sciper in enumerate(past_scipers, start=0):
            email = ldap.email_from_sciper(sciper)

            if email is None:

                print 'Trying Moodle instead.'
                try:
                    email = Mdl.getEmailFromSciper(sciper)[0]
                    print past_profs[count]['fullName'].encode('utf-8')
                    print 'Found email in Moodle: ' + email
                except IndexError:
                    print past_profs[count]['fullName'].encode('utf-8')
                    print 'No email either in Moodle'

                    try:
                        email = Mdl_Archive.getEmailFromSciper(sciper)[0]
                        print 'Found email in Archive: ' + email
                    except IndexError:
                        print past_profs[count]['fullName'].encode('utf-8')
                        print 'No email either in Archive. Finally ignoring user.'
                        name = past_profs[count]['fullName'].encode('utf-8')
                        notifications.info('\t Could not find email for {3}'.format(code, past_scipers, current_scipers, name.decode('utf-8')))
                        continue
                        pass

            name = past_profs[count]['fullName'].encode('utf-8')

            if "en" in subject:
                en_name = subject["en"].encode('utf-8')
            else:
                en_name = subject["fr"].encode('utf-8')

            if "fr" in subject:
                fr_name = subject["fr"].encode('utf-8')
            else:
                fr_name = subject["en"].encode('utf-8')

            msg = writeMsg(name, en_name, fr_name, code)
            #print msg
            sendEmail(email, msg, code)
            pass
        """

        if len(current_scipers) == 0:
            no_profs += 1
        else:
            diff_profs += 1


print 'no_profs: {0}'.format(no_profs)
print 'diff_profs: {0}'.format(diff_profs)


































__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector

# ssh -L 3307:localhost:3306 flitman@craftmoodle70.epfl.ch
"""
x = 0
print x is True
print x == True
if not x:
    print 'false'
if x:
    print 'true'
else:
    print 'false'

print '========'
y = 1
print y is True
print y == True
if not y:
    print 'false'
else:
    print 'true'

if y:
    print 'true'
else:
    print 'false'


print '========'
p = 10
print p is True
print p == True
if not p:
    print 'false'
else:
    print 'true'

if p:
    print 'true'
else:
    print 'false'
"""

Mdl = MdlSqlConnector('db_70')

Mdl.courseExistsByIdNumber('EE-491(a)')

Mdl.courseExistsByIdNumber('CH-160')
Mdl.courseExistsByIdNumber('CH-160(a)')
from string import atoi, join
import json

datadir = "/home/ian/workspace/pyMoodleScripts/api_tests/datadir/"
previous = "course-2014-2015.json"
current = "course-2015-2016.json"

previous_stream = open(datadir + previous, "r")
previous_data = json.loads(previous_stream.readline())
print "%s previous courses" % len(previous_data)

current_stream = open(datadir + current, "r")
current_data = json.loads(current_stream.readline())
print "%s current courses" % len(current_data)

courses = {}
for currentCourse in current_data:
    profSum = 0
    current_professors = []
    for prof in currentCourse["course"]["professors"]:
        profSum = profSum + atoi(prof["sciper"])
        current_professors.append(atoi(prof["sciper"]))
        print atoi(prof["sciper"])
        #print "%s - %s" % (course["course"]["courseCode"], profSum)
    currentCode = currentCourse["course"]["courseCode"]
    courses[currentCode] = [current_professors, profSum, [], 0]

for previousCourse in previous_data:
    profSum = 0
    previous_professors = []
    for prof in previousCourse["course"]["professors"]:
        profSum = profSum + atoi(prof["sciper"])
        previous_professors.append(atoi(prof["sciper"]))

    previousCode = previousCourse["course"]["courseCode"]

    if courses.has_key(previousCode):
        courses[previousCode][2] = previous_professors
        courses[previousCode][3] = profSum
    else:
        courses[previousCode] = [[], 0, previous_professors, profSum]

coursesfile = open(datadir + "courses.txt", "w")
no_infofile = open(datadir + "no_info.txt", "w")

for code in courses.keys():
    same = None

    if (courses[code][1] > 0) and (courses[code][3] > 0):
        if (courses[code][1] == courses[code][3]):
            same = True
        else:
            same = False
            # print courses[code][0]
            print "%s = %s = NEW %s = OLD %s" % (
            code, same, join(map(str, courses[code][0]), "-"), join(map(str, courses[code][2]), "-"))
            cc = "%s = %s = NEW %s = OLD %s" % (
            code, same, join(map(str, courses[code][0]), "-"), join(map(str, courses[code][2]), "-"))
            coursesfile.write(cc.encode('utf8') + '\n')
    else:
        if(courses[code][1] > 0):
            no_infofile.write('Current course {0} has no profs. Old course had {1}\n'.format(code.encode('utf8'), courses[code][3]))
        elif(courses[code][3] > 0):
            no_infofile.write('Previous course {0} had no profs. Current course has {1}\n'.format(code.encode('utf8'), courses[code][1]))
            pass

coursesfile.close()

profs = {}
profs_name = {}

profsfile = open(datadir + "profs.txt", "w")

for currentCourse in current_data:
    currentCode = currentCourse["course"]["courseCode"]
    for prof in currentCourse["course"]["professors"]:
        if (profs.has_key(prof["sciper"])):
            profs[prof["sciper"]][0].append(currentCode)
        else:
            profs[prof["sciper"]] = [[currentCode], []]
            profs_name[prof["sciper"]] = prof["fullName"]

for previousCourse in previous_data:
    previousCode = previousCourse["course"]["courseCode"]
    for prof in previousCourse["course"]["professors"]:
        if (profs.has_key(prof["sciper"])):
            profs[prof["sciper"]][1].append(previousCode)
        else:
            profs[prof["sciper"]] = [[], [previousCode]]
            profs_name[prof["sciper"]] = prof["fullName"]

for (prof, cc) in profs.items():
    cc = [sorted(l) for l in cc]
    if (cc[0] == cc[1]):
        # print "SAME"
        pass
    else:
        print "USER %s = %s = %s = %s = %s = %s" % (
        prof, profs_name[prof], cc[1], cc[0], list(set(cc[1]) - set(cc[0])), list(set(cc[0]) - set(cc[1])))
        tt = "USER %s = %s = %s = %s = %s = %s" % (
        prof, profs_name[prof], cc[1], cc[0], list(set(cc[1]) - set(cc[0])), list(set(cc[0]) - set(cc[1])))
        profsfile.write(tt.encode('utf8') + '\n')
    # print cc[0]
    # print cc[1]

profsfile.close()
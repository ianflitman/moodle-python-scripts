__author__ = 'ian'

import ldap
from connectors.MdlSqlConnector import MdlSqlConnector
import mysql.connector
from mysql.connector import connect
from mysql.connector.errors import Error
from mapping.mdlServers import servers
from mapping.constants import logs
import time
import datetime
import logging


class MDLAccountChecker:

    def __init__(self, db):
        self.log = logs['mdl_check'] + time.strftime("%d/%m/%Y").replace('/', '-') + '.txt'
        logging.basicConfig(filename=self.log, level=logging.DEBUG)

        #debug var
        self.deletions = []
        self.accountsDeleted = 0
        self.firstnameChanges = 0
        self.lastnameChanges = 0
        self.emailChanges = 0
        self.usernameChanges = 0
        self.unresolved_emails = []
        self.sciperless = []

        self.ldap = ldap.open("ldap.epfl.ch")
        self.basedn = "c=ch"
        self.mdlConnector = MdlSqlConnector(db)
        self.server = servers[db]
        self.cursor = None
        self.connect_mdl()
        self.checkAllUserDetailsAgainstLDAP()
        self.logSummary()
        self.mdl_cnx.close()


    def connect_mdl(self):
        try:
            self.mdl_cnx = connect(user=self.server['user'], password=self.server['password'], host=self.server['host'], port=self.server['port'], database=self.server['database'])
        except mysql.connector.Error as err:
            logging.error('Failed to connect to server. Error code: {0}. SQLState value: {1}. Error message: {2}'.format(err.errno, err.sqlstate, err.msg))

        self.cursor = self.mdl_cnx.cursor()


    def checkAllUserDetailsAgainstLDAP(self):
        # id 1 and 2 are the default guest and admin accounts respectively - made and necessary for any Moodle installation
        # and should be outside of the LDAP directory
        all_users_query = ("SELECT username, firstname, lastname, email, idnumber FROM mdl_user where id > 2")
        try:
            self.cursor.execute(all_users_query)
        except mysql.connector.Error as err:
            logging.error('Failed to execute all_users_query. Error code: {0}. SQLState value: {1}. Error message: {2}'.format(err.errno, err.sqlstate, err.msg))


        for(username, firstname, lastname, email, idnumber) in self.cursor:
            if idnumber == u'':
                duplicateAccounts = self.mdlConnector.duplicateAccountNumByName(firstname, lastname)
                print 'Moodle account with no sciper: {0} {1}'.format(firstname.encode('utf-8'), lastname.encode('utf-8'))
                self.sciperless.append('username: {0} firstname {1} lastname {2} email {3}'
                                       .format(username, firstname.encode('utf-8'), lastname.encode('utf-8'), email))
                if len(duplicateAccounts) > 1:
                    logging.warn("Duplicate accounts found for '{0}' '{1}'".format(firstname.encode('utf-8'), lastname.encode('utf-8')))
                    for counter, account in enumerate(duplicateAccounts, 1):
                        logging.warn("{0} has username '{1}' with sciper {2} email {3} created on {4}".format(counter, account[0].encode('utf-8'), account[1], account[3], account[2]))
                continue

            # student is external to epfl so ignore LDAP checks for them
            if '@' in idnumber:
                continue
                #print 'external {0}'.format(idnumber)


            student_filter = "(uniqueidentifier={0})".format(idnumber)
            student_ldap_details = self.ldap.search_s(self.basedn, ldap.SCOPE_SUBTREE, student_filter)

            # if student is no longer in LDAP and therefore EPFL
            if not student_ldap_details:
                course_enrolments = self.deleteUser(idnumber)
                self.deletions.append('{0} {1} ({2}) {3} from courses: {4}'.format(firstname.encode('utf-8'), lastname.encode('utf-8'), idnumber, email, course_enrolments))
                self.accountsDeleted += 1

            else:

                # sometimes there is no 'mail' field for an LDAP record, in which case we use the exisiting moodle email
                # if available. When it isn't, we try to infer it from givenName and sn fields, if they are single words
                # for each, create it and return that. Failing that we log the ambiguous ones with hyphenated names, or
                # multiple first or last names,  or those with non ascii characters, and call 'continue' on the loop.
                # This is a distinct minority - maybe 5 in 20 000 records.
                ldap_email = self.get_email(student_ldap_details, email)


                try:
                    ldap_username = self.parse_username(student_ldap_details[0][1]['uid'][0], ldap_email) #student_ldap_details[0][1]['mail'][0])
                except Exception as err:
                    print err.message


                if username != ldap_username:
                    try:
                        self.updateUserDetails(idnumber, 'username', ldap_username, username)
                        logging.info('Username Change: {0} {1} ({2}), {3} changed to {4}'.format(firstname.encode('utf-8'), lastname.encode('utf-8'), idnumber, username, ldap_username.encode('utf-8')))
                        self.usernameChanges += 1
                    except mysql.connector.Error as err:
                        logging.warn('Failed to updateUserDetails with sciper: {0} username {1} with new value: {2}. Error code: {3}. SQLState value: {4}. Error message: {5}'.format(idnumber, username, ldap_username, err.errno, err.sqlstate, err.msg))
                        # duplicate username  i.e. column integrity error
                        if err.errno == 1062:
                            print 'duplicate identity with old sciper exists. selecting it and testing it...'
                            logging.info('duplicate identity with old sciper exists. selecting it and testing it...')
                            old_username_last_login = self.mdlConnector.getLastLoginByUsername(username)
                            new_username_last_login = self.mdlConnector.getLastLoginByUsername(ldap_username)
                            old_username_creation = self.mdlConnector.getAccountCreationTime(username)
                            new_username_creation = self.mdlConnector.getAccountCreationTime(ldap_username)
                            logging.info("account with username {0} created on {1}".format(username, old_username_creation))
                            logging.info('account with username {0} created on {1}'.format(ldap_username, new_username_creation))
                            if old_username_last_login > new_username_last_login:
                                logging.info('old username most recent login by {0} milliseconds'.format(old_username_last_login-new_username_last_login))
                            else:
                                logging.info('new username most recent login by {0} milliseconds'.format(new_username_last_login-old_username_last_login))


                """

                Name changes not implemented as the vast majority we are removing unicode from names which arguably
                removes more of their 'identity' than worth the 2 or 3 names from 20 000 users who change surname
                Users can change this themselves via their settings. Email changes are more fundamental as they
                are required for forum messaging.

                if 'givenName' in student_ldap_details[0][1]:
                    ldap_firstname = student_ldap_details[0][1]['givenName'][0].decode('utf-8')
                    if firstname.lower()!= ldap_firstname.lower():
                    #self.updateUserDetails(idnumber, 'firstname', ldap_firstname)
                        logging.info('Firstname Change: {0} {1} ({2}), {0} changed to {3}'.format(firstname.encode('utf-8'), lastname.encode('utf-8'), idnumber, ldap_firstname.encode('utf-8')))
                        self.firstnameChanges += 1

                if 'sn' in student_ldap_details[0][1]:
                    ldap_lastname = student_ldap_details[0][1]['sn'][0].decode('utf-8')
                    if lastname.lower() != ldap_lastname.lower():
                        #self.updateUserDetails(idnumber, 'lastname', ldap_lastname)
                        logging.info('Lastname Change: {0} {1} ({2}), {1} changed to {3}'.format(firstname.encode('utf-8'), lastname.encode('utf-8'), idnumber, ldap_lastname.encode('utf-8')))
                        self.lastnameChanges += 1

                """

                if email != ldap_email:
                    self.updateUserDetails(idnumber, 'email', ldap_email)
                    try:
                        logging.info('Email Change: {0} {1} ({2}), {3} changed to {4}'.format(firstname.encode('utf-8'), lastname.encode('utf-8'), idnumber, email.encode('utf-8'), ldap_email.encode('utf-8')))
                    except Exception:
                        pass
                    self.emailChanges += 1



    def idnumber_is_external(self, idnumber):
        return '@' in idnumber


    # EPFL members with duplicate identities in ldap can have their uid or username as 'name@acronymn'
    # rather than just 'name' and therefore we use this function to check whether the names have the
    # @ symbol and whether they have an epfl email address. If so, we snip off the @acronym part of
    # the returned username/uid. If they do not have an epfl email address, their username should be
    # identical to their email address so we keep it intact. If the email is empty then we just return the uid.
    def parse_username(self, uid, mail):
        if '@' in uid and '@epfl.ch' in mail:
            return uid[0:uid.index('@')]
        else:
            return uid


    def updateUserDetails(self, idnumber, field, newValue, oldValue=None):
        try:
            self.mdlConnector.updateUserDetails(idnumber, field, newValue)
        except mysql.connector.Error as err:
            raise err
            #raise mysql.connector.Error('Failed to updateUserDetails with sciper: {0} for field: {1}  with value: {2}. Error code: {3}. SQLState value: {4}. Error message: {5}'.format(idnumber, field, newValue, err.errno, err.sqlstate, err.msg))


    def deleteUser(self, idnumber):
        try:
            self.mdlConnector.deleteUser(idnumber)
            #print idnumber
        except mysql.connector.Error as err:
            logging.error('Failed to deleteUser with sciper: {0}. Error code: {1}. SQLState value: {2}. Error message: {3}'.format(idnumber, err.errno, err.sqlstate, err.msg))


    def logSummary(self):
        logging.info('\n\nMDLAccountChecker Summary for {0}'.format(time.strftime("%d/%m/%Y")))
        logging.info('Accounts deleted: {0}\nUsername changes: {1}\nFirstname changes: {2}\nLastname changes: {3}\nEmail changes:{4}'.format(self.accountsDeleted, self.usernameChanges, self.firstnameChanges, self.lastnameChanges, self.emailChanges))
        logging.warn('Unresolved emails: \n{0}'.format(self.unresolved_emails))

        logging.info('\n\n\nACCOUNT DELETIONS\n')
        for deletion in self.deletions:
            logging.info(deletion + '\n')


        logging.info('\n\n\nNO SCIPERS')
        logging.warn('{} accounts with no scipers'.format(len(self.sciperless)))
        for account in self.sciperless:
            logging.warn('\n'+account)

    def get_email(self, ldap_record, moodle_email):

        if 'mail' not in ldap_record[0][1]:
            if(moodle_email == u''):
                try:
                    print moodle_email, ldap_record[0][1]['swissEduPersonUniqueID'], ldap_record[0][1]['givenName'], ldap_record[0][1]['sn']
                    return self.create_email_from_ldap(ldap_record)
                except (ValueError, KeyError) as err:
                    print err.message
                    self.unresolved_emails.append(ldap_record)
                    return u''
            else:
                return moodle_email
        else:
            return ldap_record[0][1]['mail'][0]


    def create_email_from_ldap(self, ldap_record):
        try:
            if len(ldap_record[0][1]['givenName'][0].split()) == 1 and len(ldap_record[0][1]['givenName']) == 1:
                email = ldap_record[0][1]['givenName'][0].lower() + '.'
            else:
                raise ValueError('too many first names')
        except KeyError:
            raise KeyError('no givenName in ldap record')

        try:
            if len(ldap_record[0][1]['sn'][0].split()) == 1 and len(ldap_record[0][1]['sn']) == 1:
                email += ldap_record[0][1]['sn'][0].lower() + '@epfl.ch'
            else:
                raise ValueError('too many surnames')
        except KeyError:
            raise KeyError('no sn value in ldap record')

        print email
        return email


    def check_ldap_entry(self, person):

        if 'givenName' not in person:
            return False

        if 'sn' not in person:
            return False

        if 'mail' not in person:
            #print 'no mail'
            return False

        if 'uniqueIdentifier' not in person:
            return False

        if 'uid' not in person:
            return False

        return True


#executionStart = int(round(time.time() * 1000))
#mdlCheck = MDLAccountChecker('db_dev')
#
#timeTaken = int(round(time.time() * 1000) - executionStart)
#print(timeTaken/1000)
# pass
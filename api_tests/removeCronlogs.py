__author__ = 'ian'

from subprocess import Popen, PIPE, check_output
import datetime
import dateutil.relativedelta
import os

d = datetime.date.today()
past_d = d - dateutil.relativedelta.relativedelta(months=3)
cutoff_month = past_d.strftime('%m')
cutoff_regex = '^cron_..' + cutoff_month
crondir = '/home/ian/workspace/pyMoodleScripts/api_tests/datadir/cron/'
proc = Popen(['ls', crondir], stdout=PIPE)
out = check_output(('grep', cutoff_regex), stdin=proc.stdout)
list_out = out.split('\n')
for item in list_out:
    try:
        os.remove(crondir + item)
    except OSError:
        pass


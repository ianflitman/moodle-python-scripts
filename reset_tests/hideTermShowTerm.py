__author__ = 'ian'

from connectors.MdlSqlConnector import MdlSqlConnector

myConnector = MdlSqlConnector('db_dev')
# show all courses that start Sept 2015 - (first update Sept 2014 to Sept 2015!)
myConnector.toggleVisibilityByDate(1442181600, True)
# hide all ETE 2015 courses
myConnector.toggleVisibilityByDate(1424041200, False)

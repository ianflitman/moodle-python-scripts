from mapping.mdlServers import dev_srv_server, prod_srv_server
from connectors.MdlServiceConnector import MDLServiceConnector
import xml.etree.ElementTree as ET
import base64

mdlSrv = MDLServiceConnector()

with open('/home/ian/workspace/pyMoodleScripts/datadir/courses.txt') as f:
    encoded = base64.b64encode(f.readline())

args = {
    contextid: 69,
    component: 'str',
    filearea: 'str',
    itemid: 70,
    filepath: 'str',
    filename: 'str',
    filecontent: encoded, #base64
    contextlevel: 'course',
    component: '/path/to/file'
}

params = {'file': args}
try:
    result = self.MdlSrv.rest_protocol(prod_srv_server, params, 'core_files_upload')
    res_tree = ET.fromstring(result)
    if res_tree.tag == 'EXCEPTION':
        print(shortname, res_tree[0].text, res_tree[1].text)
    elif res_tree.tag == 'RESPONSE':
        print(shortname, course_title, res_tree.find(".//KEY[@name='id']/VALUE").text)

except Exception as err:
    #print 'failed to create course {0} with exception: {1}'.format(shortname, err.message)
    raise Exception('Failed to create course {0} due to: '.format(shortname, err.message))



__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector
from connectors.IsAcademiaConnector import IsAcademiaConnector

myMdlSqlConnector = MdlSqlConnector('db_dev')
myMdlSqlConnector.connect_mdl()

myIsAcademiaConnector = IsAcademiaConnector('2014-2015', 'HIVER')

courses = myIsAcademiaConnector.getAllCourseShortnames()

for course in courses:
    enrolled_users = myMdlSqlConnector.getUsersEnrolledByCourseShortname(course)

    for user in enrolled_users:
        myMdlSqlConnector.removeStudentFromCourseBySciper(user, course)
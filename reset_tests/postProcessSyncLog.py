__author__ = 'ian'

from subprocess import Popen, PIPE
import os.path
import ast
import ldap
import logging

class PostProcessSyncLog:


    def __init__(self, log_name):

        self.ldap = ldap.open("ldap.epfl.ch")
        self.ldap.simple_bind_s()
        self.basedn = "c=ch"
        self.users_set = set()

        logfile = log_name + '.txt'
        log = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'logs/', logfile))
        self.filtered_log = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'logs/', log_name + '_filtered.txt'))
        self.filtered_report = os.path.abspath(os.path.join(os.path.dirname(__file__), '..', 'logs/', log_name + '_report.txt'))
        logging.basicConfig(filename=self.filtered_report, level=logging.DEBUG)

        f = open(self.filtered_log, 'w')

        print log
        print self.filtered_log
        proc = Popen(['grep', 'error: skipping unknown user idnumber', log], stdout=PIPE)
        stdout_value = proc.communicate()[0]
        print stdout_value
        f.write(stdout_value)
        f.close()
        self.process_filtered_log()


    def process_filtered_log(self):

        with open(self.filtered_log, 'r') as f:
            for line in f.readlines():
                res = line.find('idnumber') + len('idnumber')
                end = line.find('in', res)
                sciper_str = line[res:end].rstrip().lstrip()
                sciper = ast.literal_eval(sciper_str)

                end_res = line.find('course') + len('course')
                course_escaped_str = line[end_res:].rstrip().lstrip()
                course_str = ast.literal_eval(course_escaped_str)

                if sciper not in self.users_set:
                    user_filter = "(uniqueidentifier={0})".format(sciper)
                    user_result = self.ldap.search_s(self.basedn, ldap.SCOPE_SUBTREE, user_filter)
                    self.users_set.add(sciper)
                    isValid = self.check_ldap_entry(user_result[0][1], course_str)


    def check_ldap_entry(self, person, course_str):

        if 'givenName' not in person:
            logging.info('{0} in {1} has no givenName'.format(person['uniqueIdentifier'], course_str))
            if person['employeeType']:
                logging.info('employeeType {}'.format(person['employeeType']))
            return

        if 'sn' not in person:
            return False

        if 'mail' not in person:
            logging.info('{0} in {1} has no email'.format(person['uniqueIdentifier'], course_str))
            if person['employeeType']:
                logging.info('employeeType {}'.format(person['employeeType']))
            return False

        if 'uniqueIdentifier' not in person:
            return False

        if 'uid' not in person:
            return False

        logging.info('{0} in {1} has nothing wrong!'.format(person['uniqueIdentifier'], course_str))
        if person['employeeType']:
            logging.info('employeeType {}'.format(person['employeeType']))
        return True
        pass





post = PostProcessSyncLog('SyncPHPLog_28-08-2015')
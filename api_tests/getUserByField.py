__author__ = 'ian'
from connectors.MdlServiceConnector import MDLServiceConnector
from connectors.MdlSqlConnector import MdlSqlConnector
from mapping.mdlServers import dev_srv_server, prod_srv_server


mdlConnector = MDLServiceConnector()
mdlSQLConnector = MdlSqlConnector('db_dev')

search_vals = {'field': 'idnumber',
               'values': [696968]}

result = mdlConnector.rest_protocol(dev_srv_server, search_vals, 'core_user_get_users_by_field')
present = mdlSQLConnector.userExistsWithSciper(696968)
pass
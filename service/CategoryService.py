#!/usr/bin/env python
# -*- coding: utf-8 -*-

from connectors.MdlServiceConnector import MDLServiceConnector
from mapping.mdlServers import servers
from mapping import courseCategoryMapping

# Categories in courseCategoryMapping is a list of tuples (title, description, isParent(True/False) )
# If the category is a parent then it will have bachelor and masters sub-categories belonging to it

# If the code fails for some reason then make sure to run:
# DELETE FROM `mdl_course_categories` where id > 1;
# ALTER TABLE `mdl_course_categories` AUTO_INCREMENT = 2;

class CategoryCreator:

    def __init__(self, db='dev_srv_server'):
        self.db = servers[db]
        self.mdlConnector = MDLServiceConnector()
        self.categoryList = []
        self.createCategories()

    # We start the count from 2 as Miscellaneous is the default category in a fresh install of moodle
    # with an id already of 1, and it cannot be removed or overwritten or Moodle falls over like a drunk
    def createCategories(self):

        for categoryCounter, cat in enumerate(courseCategoryMapping.Categories, start=2):
            category = {}
            category['name'] = cat[0].encode('utf-8')
            category['description'] = cat[1].encode('utf-8')
            category['parent'] = 0
            self.categoryList.append(category)
            print category['name'].decode('utf-8')
            print categoryCounter


        for category in self.categoryList:
            new_category = {'categories': category}
            result = self.mdlConnector.rest_protocol(self.db, new_category, 'core_course_create_categories')

        self.categoryList = []

        for categoryCounter, cat in enumerate(courseCategoryMapping.Categories, start=2):

            if(cat[2] is True):
                ba_category = {}
                ba_category['name'] = 'Bachelor'
                ba_category['parent'] = categoryCounter
                ba_category['description'] = 'Cours Bachelor pour {0}'.format(cat[0].encode('utf-8'))
                self.categoryList.append(ba_category)

                ma_category = {}
                ma_category['name'] = 'Master'
                ma_category['parent'] = categoryCounter
                ma_category['description'] = 'Cours Master pour {0}'.format(cat[0].encode('utf-8'))
                self.categoryList.append(ma_category)
                print cat[0].encode('utf-8')
                print 'BA & MA'
            else:
                single_category = {}
                single_category['name'] = cat[0].encode('utf-8')
                single_category['description'] = cat[1].encode('utf-8')
                single_category['parent'] = 0
                self.categoryList.append(single_category)
                print single_category['name']
                print 'single category'

            print '============='

        for category in self.categoryList:
            new_category = {'categories': category}
            result = self.mdlConnector.rest_protocol(self.db, new_category, 'core_course_create_categories')

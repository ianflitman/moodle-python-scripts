__author__ = 'ian'

# unix times are 22 00 the previous day GMT for HIVER
# but 23 00 for ETE

# http://www.onlineconversion.com/unix_time.htm
# http://sac.epfl.ch/academic-calendar


Year_2014_2015 = {
    'HIVER': {'start': 1410732000, 'weeks': 14},
    'ETE': {'start': 1424041200, 'weeks': 15}
}

# Sept 14th and 22nd Feb
Year_2015_2016 = {
    'HIVER': {'start': 1442181600, 'weeks': 14},
    'ETE': {'start': 1456095600, 'weeks': 15}  # '1456095600' initially wrong 1424646000 i.e Feb 23rd 2015
}

# 20th Sept and 20th Feb
Year_2016_2017 = {
    'HIVER': {'start': 1474408800, 'weeks': 14},
    'ETE': {'start': 1487631600, 'weeks': 15}
}

Timetable = {
    '2014-2015': Year_2014_2015,
    '2015-2016': Year_2015_2016,
    '2016-2017': Year_2016_2017
}


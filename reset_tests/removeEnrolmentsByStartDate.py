__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector

myMdlSqlConnector = MdlSqlConnector('db_dev')
myMdlSqlConnector.connect_mdl()

courses = myMdlSqlConnector.getCoursesByStartDate(1410732000)

for course in courses:
    enrolled_users = myMdlSqlConnector.getUsersEnrolledByCourseShortname(course[0])

    for user in enrolled_users:
        myMdlSqlConnector.removeStudentFromCourseBySciper(user, course[0])
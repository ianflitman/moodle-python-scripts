__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector
import json
import re

def course_isValid(shortname):
    if re.search('-\d{3}', shortname):
        sectionCode = shortname[0:shortname.index('-')]
        if sectionCode not in ['EPFL', 'ETH']:  # known exceptions
            return True
        else:
            return False
    else:
        return False


datadir = "/home/ian/workspace/pyMoodleScripts/teacher_migrate/datadir/"

current = "ete_2016_Jan18.json"
previous = "hiver2015_jan192016.json"

Mdl = MdlSqlConnector('db_70')
Mdl_Archive = MdlSqlConnector('db_archive')

current_stream = open(datadir + current, "r")
current_data = json.loads(current_stream.readline())

previous_stream = open(datadir + previous, "r")
previous_data = json.loads(previous_stream.readline())
print "%s previous courses" % len(previous_data)

current = {}
for course_data in current_data:
    course = course_data['course']
    if course_isValid(course["courseCode"]):
        current[course['courseCode']] = course

past = {}
for course_data in previous_data:
    course = course_data['course']
    if course_isValid(course['courseCode']):
        past[course['courseCode']] = course


course_two_terms = 0;

for code in current.keys():
    if code in past.keys():
        print '{0} has two terms'.format(code)
        course_two_terms += 1

        pass

print 'two term course total: {0}'.format(course_two_terms)


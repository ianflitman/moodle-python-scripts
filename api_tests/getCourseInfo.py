__author__ = 'ian'
from connectors.MdlServiceConnector import MDLServiceConnector
from connectors.MdlSqlConnector import MdlSqlConnector
from mapping.mdlServers import prod_srv_server, dev_srv_server

myMdlConnector = MDLServiceConnector()
myMdlSqlConnector = MdlSqlConnector('db_prod')

cat = myMdlSqlConnector.getIdCategoryFromShortname('AR-999')

params = {
 'name': 'shortname',
 'value': 'AR-102'
}

course = {'options': params}

result = myMdlConnector.rest_protocol(prod_srv_server, course, 'core_course_get_contents')

pass
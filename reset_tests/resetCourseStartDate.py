__author__ = 'ian'
from connectors.MdlSqlConnector import MdlSqlConnector

myMdlSqlConnector = MdlSqlConnector('db_70')
myMdlSqlConnector.connect_mdl()

# resets Sept 2014 to Sept 2015
# myMdlSqlConnector.resetCourseStartDates(1410732000, 1442181600)
# resets Feb 2015 to Feb 2016
myMdlSqlConnector.resetCourseStartDates(1424041200, 1456095600)
pass


__author__ = 'ian'

# for Mdl APIs - tokens generated for webservice on each server set up previously
dev_srv_server = {
    'protocol': 'rest',
    'uri': 'http://mdl-dev.epfl.ch',
    'token': '9d90dfa264a7930432a1e92322c6dcdb'
}

prod_srv_server = {
    'protocol': 'rest',
    'uri': 'http://moodle.epfl.ch',
    'token': '9d90dfa264a7930432a1e92322c6dcdb'
}

staging_srv_server = {
    'protocol': 'rest',
    'uri': 'http://craftmoodle10.epfl.ch',
    'token': '9d90dfa264a7930432a1e92322c6dcdb'
    # localhost db has 'token': '7eca679de7c099a6675d4827498ed12a'
}


# for direct Mdl db connections
# requires ssh tunnel if used from outside server itself:
# ssh -L 3307:craftmoodle50.epfl.ch:3306 -i /home/ian/Documents/ssh_keys/openssh_ianflitman_privatekey kis@craftmoodle30.epfl.ch

db_prod = {
    'user': 'moodleroot',
    'password': 'sTe1la6!',
    'host': 'localhost',
    'port': '3307',
    'database': 'moodle'
}

# ssh -L 3309:localhost:3306 -i /home/ian/Documents/ssh_keys/openssh_ianflitman_privatekey kis@craftmoodle10.epfl.ch
db_int = {
    'user': 'moodle',
    'password': 'fe1ix9',
    'host': 'localhost',
    'port': '3309',
    'database': 'moodle'
}
# db_int = {
#     'user': 'moodleroot',
#     'password': 'sTe1la6',
#     'host': 'localhost',
#     'port': '3309',
#     'database': 'moodle'
# }

# requires ssh tunnel if used outside the server itself:
# ssh -L 3308:localhost:3306 -i /home/ian/.ssh/id_rsa root@mdl-dev.epfl.ch
# db_dev = {
#     'user': 'root',
#     'password': 'IjmagcM',
#     'host': 'localhost',
#     'port': '3308',
#     'database': 'moodle'
# }

db_dev = {
    'user': 'moodle',
    'password': 'fe1ix9',
    'host': 'mdl-db.epfl.ch',
    'port': '3306',
    'database': 'moodle'
}

db_mdl_dev = {
    'user': 'root',
    'password': 'ian',
    'host': 'localhost',
    'port': '3308',
    'database': 'mdl_dev'
}

db_mdl_rhel = {
    'user': 'ian',
    'password': 'flitman',
    'host': '128.178.13.186',
    'port': '3306',
    'database': 'moodle'
}

# no ssh tunnel and therefore change of ports
db_inside_prod = {
    'user': 'moodleroot',
    'password': 'sTe1la6!',
    'host': 'localhost',
    'port': '3306',
    'database': 'moodle'
}

# ssh -L 3307:localhost:3306  flitman@craftmoodle70.epfl.ch
# then supply passwd
db_70 = {
    'user': 'moodle',
    'password': 'fe1ix9',
    'host': 'localhost',
    'port': '3307',
    'database': 'moodle'
}

db_inside_dev = {
    'user': 'root',
    'password': 'ian',
    'host': 'localhost',
    'port': 3306,
    'database': 'moodle'
}

# ssh -L 3308:localhost:3306  flitman@cedegesrv7.epfl.ch
db_archive = {
    'user': 'moodle',
    'password': 'fe1ix9',
    'host': 'localhost',
    'port': 3308,
    'database': 'mdlarch_1415'
}

servers = {
    'dev_srv_server': dev_srv_server,
    'dev_prod': prod_srv_server,
    'db_int': db_int,
    'db_dev': db_dev,
    'db_mdl_dev': db_mdl_dev,
    'db_mdl_rhel': db_mdl_rhel,
    'db_prod': db_prod,
    'db_inside_prod': db_inside_prod,
    'db_inside_dev': db_inside_dev,
    'db_70': db_70,
    'db_archive': db_archive
}
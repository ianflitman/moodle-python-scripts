__author__ = 'ian'

import requests
from requests.exceptions import HTTPError
from requests.exceptions import ConnectionError
from lxml import etree
from lxml.etree import tostring
import json
import re

from itertools import izip
from mapping.courseCategoryMapping import Categories #CodeToCategory

#FILE_PATH = '/home/ian/moodleDatas/'
FILE_PATH = '/var/www/vhosts/moodle.epfl.ch/moodlescripts/json/'


class IsAcademiaConnector:

    def __init__(self, year, term):
        self.baseUrl = 'https://isa.epfl.ch/services/'
        self.year = year
        self.term = term
        #self.headers = {'Content-Type': 'application/xml', 'Accept': 'text/html,application/xhtml+xml,application/xml'}
        self.headers = {'Content-Type': 'application/json', 'Accept': 'application/json'}
        self.data = {}


    def getCatalog(self):
        self.allCourseRequestUrl = self.baseUrl
        self.allCourseRequestUrl += "catalog/{0}/{1}".format(self.year, self.term)
        response = requests.get(self.allCourseRequestUrl, headers=self.headers)
        try:
            response.raise_for_status()
        except requests.exceptions.HTTPError as e:
            print e.message + ' in getCatalog()'

        return json.loads(response.content)


    def getAllCourses(self):
        self.allCourseRequestUrl = self.baseUrl
        self.allCourseRequestUrl += "courses/{0}/{1}".format(self.year, self.term)

        try:
            response = requests.get(self.allCourseRequestUrl, headers=self.headers)
            response.raise_for_status()
        except (HTTPError, ConnectionError) as err:
            raise Exception("getAllCourses failed in IsAcamdeiaConnector. Msg: {1}".format(err.message))

        return json.loads(response.content)


    def getWinterTerm(self):
        self.allCourseRequestUrl = self.baseUrl
        self.allCourseRequestUrl += "courses/{0}/{1}".format(self.year, 'HIVER')
        try:
            response = requests.get(self.allCourseRequestUrl, headers=self.headers)
            response.raise_for_status()
        except (HTTPError, ConnectionError) as err:
            raise Exception("getAllCourses failed in IsAcamdeiaConnector. Msg: {1}".format(err.message))

        return json.loads(response.content)


    def getAllCoursesFromFile(self, file):
        filename  = file + '.json'
        with open(FILE_PATH + filename) as courses_file:
            self.data = json.load(courses_file)
        return self.data


    def getAllCourseShortnames(self):
        courses = self.getCatalog()

        shortnames = []
        for course in courses:
            if re.search('-\d{3}', course['code']):
                #print course['code']
                shortnames.append(course['code'])
            #else:
            #    print '\t\t\t\t\t\t\tNO MATCH: ' + course['code']

        return shortnames


    def getAllCourseInfo(self):
        xml = self.getAllCourses()
        courseReturn = []
        courses = xml.findall('./course')
        for course in courses:
            #print etree.tostring(course)
            course_params = {}
            course_params['shortname'] = course.find('./code').text.encode('utf8')
            course_params['fullname'] = course.find('./title').text.encode('utf8')
            #coursebookInfo = self.getCourseBook(course_params['shortname'])
            #course_params['plans'] = coursebookInfo[0]
            #course_params['content'] = coursebookInfo[1]
            courseReturn.append(course_params)

        return courseReturn

    def study_plan(self, plans, shortcode):
        self.file.write(shortcode +'\n')

        for plan in plans:
            self.file.write(plan.text +'\n')

        if len(plans) > 1:
            self.file.write('\n\n\n')

        pass

    def getCourseBook(self, shortCode):
        self.courseBookRequestUrl = self.baseUrl
        self.courseBookRequestUrl += 'books/{0}/course/{1}'.format(self.year, shortCode)
        try:
            response = requests.get(self.courseBookRequestUrl, headers=self.headers)
            try:
                courseBookXML = etree.fromstring(response.content)
                print tostring(courseBookXML)
            except:
                pass
            #print etree.tostring(courseBookXML)
            study_plans = courseBookXML.xpath("//study-plan")
            #self.study_plan(study_plan, shortCode)
            contentXML = courseBookXML.xpath("//type[@code='RUBRIQUE_CONTENU']/../content")

            courseContent = []
            for content in contentXML:
                print '=============content========='
                courseContent.append(content.text)
                print tostring(content)

            plans = []
            for study_plan in study_plans:
                print '=============study plan========='
                plans.append(study_plan.text)
                print (study_plan.text)


            return (plans, courseContent)
        except HTTPError:
            print HTTPError



    def getSectionCourseBooks(self, section):
        courses = []
        self.sectionCourseBooksRequestUrl = self.baseUrl
        self.sectionCourseBooksRequestUrl += 'books/{0}/section/{1}'.format(self.year, section)
        try:
            response = requests.get(self.sectionCourseBooksRequestUrl, headers= self.headers)
            self.xmlfile.write(response.content)

            sectionCourseBooksXML = etree.fromstring(response.content).xpath('//course-book')
            for coursebook in sectionCourseBooksXML:
                course = {}
                course['fullname'] = coursebook.find('./course/title').text.encode('utf8')
                course['shortname'] = coursebook.find('./course/code').text.encode('utf8')
                courses.append(course)

                pass
        except:
            print HTTPError

        return courses


    def getCourseInscriptions(self, shortCode):
        self.courseInscriptionsRequestUrl = self.baseUrl
        self.courseInscriptionsRequestUrl += 'inscriptions/{0}/course/{1}'.format(self.year, shortCode)
        inactive_scipers = []
        active_scipers = []
        try:
            response = requests.get(self.courseInscriptionsRequestUrl, headers=self.headers)
            xml = etree.fromstring(response.content)
            inscriptions = xml.findall('./course/curricula/cursus/inscriptions/inscription')
            for inscription in inscriptions:
                if(inscription.find('./active').text == 'false'):
                    inactive_scipers.append(inscription.find('./sciper').text)
                else:
                    active_scipers.append(inscription.find('./sciper').text)

        except HTTPError:
            print HTTPError

        return {
            'active': active_scipers,
            'inactive': inactive_scipers
        }


    def getInactiveInscriptions(self, inscriptions):
        inactive_students = []

        for inscription in inscriptions:
            if(inscription.find('./active').text == 'false'):
                inactive_students.append(inscription.find('./sciper').text)

        return inactive_students



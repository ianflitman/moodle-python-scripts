#!/usr/bin/env python
# -*- coding: utf-8 -*-

#categories
#last two FC for Formation Continue and MISC for Miscellaneous are fictional codes
#when parent is 0 then it has no child categories

Categories = [
    (u'Architecture (AR)', u'Faculté ENAC \nSection d\'architecture', True),
    (u'Génie civil (GC)', u'Faculté ENAC \nSection de Génie Civil', True),
    (u'Sciences et ingénierie de l\'environnement (SIE)', u'Faculté ENAC\nSection Sciences et ingénierie de l\'environnement', True),
    (u'Chimie, Génie Chimique (CGC)', u'Chimie, Génie Chimique (CGC)\nSection Chimie, Génie chimique', True),
    (u'Mathématiques (MA)',u'Faculté de Sciences de base\nSection Mathématiques', True),
    (u'Physique (PH)', u'Faculté Sciences de base\nSection Physique', True),
    (u'Génie électrique et électronique (EL)', u'Faculté Sciences et techniques de l\'ingénieur\nSection Génie électrique et électronique', True),
    (u'Génie Mécanique (GM)', u'Faculté de Sciences et Technologies de l\'ingénieur\nSection Génie Mécanique', True),
    (u'Matériaux (MX)', u'Faculté Sciences de l\'ingénieur\nSection Matériaux', True),
    (u'Microtechnique (MT)', u'Faculté Sciences et Techniques de l\'ingénieur\nSection Microtechnique', True),
    (u'Informatique (IN)', u'Faculte I & C\nSection d\'Informatique', True),
    (u'Systèmes de Communication (SC)',u'Faculté I & C\nSection de Systèmes de communications', True),
    (u'Sciences et Technologies du Vivant (SV)', u'Faculté Sciences de la vie\nSection Sciences et technologies du vivant', True),
    (u'Collège du Management de la Technologie (CDM)', u'College du Management de la Technologie', True),
    (u'Sciences Humaines et Sociales (SHS)', u'Collège des Humanités\nSciences humaines et sociales', True),

    (u'Cours Préparatoire (CMS)', u'Cours Préparatoire', False),
    (u'Collège de Sciences (UNIL)', u'Collège de Sciences', False),
    (u'Ecole Doctorale (ED)', u'Ecole Doctorale', False),
    (u'Formation Continue', u'Formation Continue', False),
    # Miscellaneous is the default category in a fresh moodle installation
    #'MISC': (u'Miscellaneous', u'Miscellaneous', False)
]

CodeToCategoryDict = {
    'AR': (u'Architecture (AR)', u'Faculté ENAC \nSection d\'architecture', True),
    'GC': (u'Génie civil (GC)', u'Faculté ENAC \nSection de Génie Civil', True),
    'SIE': (u'Sciences et ingénierie de l\'environnement (SIE)', u'Faculté ENAC\nSection Sciences et ingénierie de l\'environnement', True),
    'CGC': (u'Chimie, Génie Chimique (CGC)', u'Chimie, Génie Chimique (CGC)\nSection Chimie, Génie chimique', True),
    'MA': (u'Mathématiques (MA)',u'Faculté de Sciences de base\nSection Mathématiques', True),
    'PH': (u'Physique (PH)', u'Faculté Sciences de base\nSection Physique', True),
    'EL': (u'Génie électrique et électronique (EL)', u'Faculté Sciences et techniques de l\'ingénieur\nSection Génie électrique et électronique', True),
    'GM': (u'Génie Mécanique (GM)', u'Faculté de Sciences et Technologies de l\'ingénieur\nSection Génie Mécanique', True),
    'MX': (u'Matériaux (MX)', u'Faculté Sciences de l\'ingénieur\nSection Matériaux', True),
    'MT': (u'Microtechnique (MT)', u'Faculté Sciences et Techniques de l\'ingénieur\nSection Microtechnique', True),
    'IN': (u'Informatique (IN)', u'Faculte I & C\nSection d\'Informatique', True),
    'SC': (u'Systèmes de Communication (SC)',u'Faculté I & C\nSection de Systèmes de communications', True),
    'SV': (u'Sciences et Technologies du Vivant (SV)', u'Faculté Sciences de la vie\nSection Sciences et technologies du vivant', True),
    'CDM': (u'Collège du Management de la Technologie (CDM)', u'College du Management de la Technologie', True),
    'SHS': (u'Sciences Humaines et Sociales (SHS)', u'Collège des Humanités\nSciences humaines et sociales', True),

    'CMS': (u'Cours Préparatoire (CMS)', u'Cours Préparatoire', False),
    'UNIL': (u'Collège de Sciences (UNIL)', u'Collège de Sciences', False),
    'ED': (u'Ecole Doctorale (ED)', u'Ecole Doctorale', False),
    'FC': (u'Formation Continue', u'Formation Continue', False),
    'PREPA': (u'Cours Préparatoire (CMS)', u'Cours Préparatoire', False)
    # Miscellaneous is the default category in a fresh moodle installation
    #'MISC': (u'Miscellaneous', u'Miscellaneous', False), #{'Parent': 50}, {'Parent': 1})
}

extraCategories = [('Formation Continue'), 'Miscellaneous']


CourseCodeToCategory = {
    'AR': {'Master': 79, 'Bachelor': 54},  #41 AR,
    u'BIO': {'Master': 68, 'Bachelor': 61},  #52 SV,
    'BIOENG': {'Master': 68, 'Bachelor': 61}, #52 SV,
    'CH': {'Master': 78, 'Bachelor': 55},  #42 CGC,
    'CIVIL': {'Master': 76, 'Bachelor': 57},  #44 GC,
    'COM': {'Master': 74, 'Bachelor': 66},  #46 IN,
    'CS': {'Master': 74, 'Bachelor': 66},  #46 IN,
    'ChE': {'Master': 78, 'Bachelor': 55},  #42 CGC,
    'EE': {'Master': 77, 'Bachelor': 56},  #43 EL,
    'ENG': {'Master': 69, 'Bachelor': 59},  #51 SIE,
    'ENV': {'Master': 69, 'Bachelor': 59},  #51 SIE,
    'FIN': {'Master': 85, 'Bachelor': 151},  #12 CDM,
    'HUM': {'Master': 15, 'Bachelor': 16},  #14 SHS,
    'MATH': {'Master': 72, 'Bachelor': 64},  #48 MA,
    'ME': {'Master': 75, 'Bachelor': 58},  #45 GM,
    'MGT': {'Master': 85, 'Bachelor': 151},  #12 CDM,
    'MICRO': {'Master': 71, 'Bachelor': 63},  #49 MT,
    'MSE': {'Master': 73, 'Bachelor': 65},  # 47 MX,
    'PHYS': {'Master': 70, 'Bachelor': 62},  # 50 PH,
    'UNIL': 87, #{'Master': 87, 'Bachelor': 87},  # no distinction currently between Master and Bachelor for UNIL
    'PREPA': 10  # Cours Preparatoire
}

CourseCodesToIgnore = ['ETH', 'Cours UNIL']

oldNoChildrenCatIds = [82, 10, 87, 6, 1, 0]
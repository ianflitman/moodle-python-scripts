new course: AR-474
taught by: Cornelia Dominique Tapparelli Portilla Kawamura, sciper: 154975, email: cornelia.tapparelli@epfl.ch
new course: MATH-186
taught by: Giordano Favi, sciper: 121777, email: giordano.favi@epfl.ch
new course: MSE-486
taught by: Holger Frauenrath, sciper: 190315, email: holger.frauenrath@epfl.ch
new course: EE-525
taught by: Catherine Dehollain, sciper: 107331, email: catherine.dehollain@epfl.ch
new course: AR-424(a)
	 Could not find email for course AR-424(a) prof with sciper: 258242
new course: MATH-106(en)
taught by: Jan Sickmann Hesthaven, sciper: 232231, email: jan.hesthaven@epfl.ch
new course: MICRO-314
taught by: Yves Perriard, sciper: 106071, email: yves.perriard@epfl.ch
taught by: André Hodder, sciper: 101913, email: andre.hodder@epfl.ch
new course: MATH-467
taught by: János Pach, sciper: 183120, email: janos.pach@epfl.ch
new course: MATH-302
taught by: Boris Buffoni, sciper: 120861, email: boris.buffoni@epfl.ch
new course: PENS-211
taught by: Richard Nguyen, sciper: 251921, email: richard.nguyen@epfl.ch
taught by: Dominique Perrault, sciper: 235585, email: dominique.perrault@epfl.ch
new course: ChE-459
taught by: Philippe Zaza, sciper: 169257, email: philippe.zaza@epfl.ch
new course: PHYS-438
taught by: Rolf Gruetter, sciper: 161735, email: rolf.gruetter@epfl.ch
new course: HUM-433(b)
taught by: Frédéric Kaplan, sciper: 174121, email: frederic.kaplan@epfl.ch
new course: CS-431
taught by: Martin Rajman, sciper: 112354, email: martin.rajman@epfl.ch
taught by: Jean-Cédric Chappelier, sciper: 112547, email: jean-cedric.chappelier@epfl.ch
new course: MSE-202
taught by: Helena Van Swygenhoven, sciper: 144178, email: helena.vanswygenhoven@epfl.ch
taught by: Igor Stolichnov, sciper: 112564, email: igor.stolitchnov@epfl.ch
new course: ME-201
	 Could not find email for course ME-201 prof with sciper: 134499
new course: HEP-409
	 Could not find email for course HEP-409 prof with sciper: 251150
new course: MATH-215
taught by: Eva Bayer Fluckiger, sciper: 138858, email: eva.bayer@epfl.ch
new course: ENV-523
	 Could not find email for course ENV-523 prof with sciper: 173898
new course: AR-402(e)
taught by: Yves Weinand, sciper: 156151, email: yves.weinand@epfl.ch
new course: EE-570
taught by: Sidi-Rachid Cherkaoui, sciper: 104759, email: rachid.cherkaoui@epfl.ch
new course: ENV-241
	 Could not find email for course ENV-241 prof with sciper: 172717
	 Could not find email for course ENV-241 prof with sciper: 109329
new course: MSE-311
taught by: Stefano Mischler, sciper: 105869, email: stefano.mischler@epfl.ch
new course: PREPA-037
taught by: Walter Gaxer, sciper: 105209, email: walter.gaxer@epfl.ch
new course: PREPA-038
taught by: Camil-Demetru Petrescu, sciper: 107044, email: camil.petrescu@epfl.ch
new course: HUM-324(a)
	 Could not find email for course HUM-324(a) prof with sciper: 196397
new course: CIVIL-210
taught by: Christophe Ancey, sciper: 148669, email: christophe.ancey@epfl.ch
new course: CIVIL-515
taught by: Christophe Ancey, sciper: 148669, email: christophe.ancey@epfl.ch
new course: MICRO-510
taught by: Yves Perriard, sciper: 106071, email: yves.perriard@epfl.ch
taught by: André Hodder, sciper: 101913, email: andre.hodder@epfl.ch
taught by: Christian Köchli, sciper: 105563, email: christian.koechli@epfl.ch
new course: MICRO-513
taught by: Dimitri Nestor Alice Van De Ville, sciper: 152027, email: dimitri.vandeville@epfl.ch
new course: UNIL-005
taught by: Gervais Chapuis, sciper: 122829, email: gervais.chapuis@epfl.ch
new course: UNIL-006
taught by: Jacques Thévenaz, sciper: 123676, email: jacques.thevenaz@epfl.ch
new course: MATH-106(c)
taught by: Klaus-Dieter Semmler, sciper: 106409, email: klaus-dieter.semmler@epfl.ch
new course: BIO-598(b)
	 Could not find email for course BIO-598(b) prof with sciper: 126096
new course: MATH-323
taught by: Kathryn Hess Bellwald, sciper: 105396, email: kathryn.hess@epfl.ch
new course: HUM-331
	 Could not find email for course HUM-331 prof with sciper: 126096
new course: HUM-339
	 Could not find email for course HUM-339 prof with sciper: 245404
new course: MICRO-311(b)
taught by: Pierre Vandergheynst, sciper: 120906, email: pierre.vandergheynst@epfl.ch
new course: MATH-105(a)
taught by: Joachim Krieger, sciper: 199126, email: joachim.krieger@epfl.ch
new course: HUM-240
	 Could not find email for course HUM-240 prof with sciper: 197901
new course: BIO-443
taught by: Aleksandra Radenovic, sciper: 161458, email: aleksandra.radenovic@epfl.ch
new course: MATH-131
taught by: Victor Panaretos, sciper: 180565, email: victor.panaretos@epfl.ch
new course: MICRO-213
taught by: Yves Perriard, sciper: 106071, email: yves.perriard@epfl.ch
new course: EE-545
taught by: Michael Mattes, sciper: 114730, email: michael.mattes@epfl.ch
new course: MSE-451
taught by: John Christopher Plummer, sciper: 106123, email: christopher.plummer@epfl.ch
new course: MICRO-311(a)
taught by: Pierre Vandergheynst, sciper: 120906, email: pierre.vandergheynst@epfl.ch
new course: PHYS-325
taught by: Stefano Alberti, sciper: 104368, email: stefano.alberti@epfl.ch
new course: EE-470
taught by: Sidi-Rachid Cherkaoui, sciper: 104759, email: rachid.cherkaoui@epfl.ch
new course: AR-212
taught by: Nicola Braghieri, sciper: 180534, email: nicola.braghieri@epfl.ch
new course: CH-448
taught by: Georges Wagnières, sciper: 106697, email: georges.wagnieres@epfl.ch
new course: HUM-411(d)
	 Could not find email for course HUM-411(d) prof with sciper: 209405
new course: ChE-407
taught by: Ardemis Boghossian, sciper: 240723, email: ardemis.boghossian@epfl.ch
new course: ChE-404
taught by: César Pulgarin, sciper: 106159, email: cesar.pulgarin@epfl.ch
new course: MSE-351
taught by: Igor Stolichnov, sciper: 112564, email: igor.stolitchnov@epfl.ch
taught by: Paul Muralt, sciper: 105945, email: paul.muralt@epfl.ch
new course: EE-599(a)
	 Could not find email for course EE-599(a) prof with sciper: 126096
new course: MSE-421
taught by: Michele Ceriotti, sciper: 235586, email: michele.ceriotti@epfl.ch
taught by: Francesco Stellacci, sciper: 196998, email: francesco.stellacci@epfl.ch
new course: CH-446
taught by: Oleg Boyarkine, sciper: 104570, email: oleg.boiarkin@epfl.ch
new course: CH-447
taught by: Mohammad Khaja Nazeeruddin, sciper: 105958, email: mdkhaja.nazeeruddin@epfl.ch
new course: ENV-361
	 Could not find email for course ENV-361 prof with sciper: 192838
new course: CH-442
taught by: Jacques-Edouard Moser, sciper: 105914, email: jacques-edouard.moser@epfl.ch
new course: PREPA-033
taught by: Roger Sauser, sciper: 157882, email: roger.sauser@epfl.ch
new course: HUM-405(b)
taught by: Boris Beaude, sciper: 168003, email: boris.beaude@epfl.ch
new course: PHYS-402
taught by: Jean-Paul Richard Kneib, sciper: 222189, email: jean-paul.kneib@epfl.ch
new course: HUM-404(b)
taught by: Michaël Meyer, sciper: 218374, email: michael.meyer@unil.ch
new course: PHYS-404
new course: HUM-343
	 Could not find email for course HUM-343 prof with sciper: 166176
new course: HUM-345
	 Could not find email for course HUM-345 prof with sciper: 114106
new course: AR-302(d)
taught by: Anja Fröhlich, sciper: 225189, email: anja.frohlich@epfl.ch
taught by: Martin Fröhlich, sciper: 225190, email: martin.frohlich@epfl.ch
new course: EE-365
taught by: Roberto Zoia, sciper: 106775, email: roberto.zoia@epfl.ch
new course: EE-361
taught by: André Hodder, sciper: 101913, email: andre.hodder@epfl.ch
new course: PREPA-008
taught by: Simon Bossoney, sciper: 100527, email: simon.bossoney@epfl.ch
new course: ME-341
taught by: Navid Borhani, sciper: 115416, email: navid.borhani@epfl.ch
taught by: John Richard Thome, sciper: 108776, email: john.thome@epfl.ch
new course: CIVIL-434
	 Could not find email for course CIVIL-434 prof with sciper: 106699
taught by: Roberto Gargiani, sciper: 126097, email: roberto.gargiani@epfl.ch
taught by: Aurelio Muttoni, sciper: 133346, email: aurelio.muttoni@epfl.ch
new course: PREPA-004
taught by: Olivier Woringer, sciper: 106748, email: olivier.woringer@epfl.ch
new course: PREPA-006
taught by: Roger Röthlisberger, sciper: 107148, email: roger.roethlisberger@epfl.ch
taught by: Simon Bossoney, sciper: 100527, email: simon.bossoney@epfl.ch
new course: PHYS-108
taught by: Minh Quang Tran, sciper: 106568, email: minhquang.tran@epfl.ch
new course: MATH-401
taught by: Hans-Jörg Ruppen, sciper: 145266, email: hans-joerg.ruppen@epfl.ch
new course: CIVIL-260
taught by: Ian Smith, sciper: 106443, email: ian.smith@epfl.ch
new course: UNIL-131
taught by: Darlène Goldstein, sciper: 151741, email: darlene.goldstein@epfl.ch
new course: PHYS-501(b)
	 Could not find email for course PHYS-501(b) prof with sciper: 126096
new course: MATH-106(e)
taught by: Anna Lachowska, sciper: 167946, email: anna.lachowska@epfl.ch
new course: ENV-420
taught by: Philippe Adam, sciper: 240591, email: philippe.adam@epfl.ch
new course: AR-449
taught by: Maria Cristina Munari Probst, sciper: 159393, email: mariacristina.munariprobst@epfl.ch
new course: HUM-237
	 Could not find email for course HUM-237 prof with sciper: 245404
	 Could not find email for course HUM-237 prof with sciper: 198529
new course: HUM-239
	 Could not find email for course HUM-239 prof with sciper: 267780
	 Could not find email for course HUM-239 prof with sciper: 218732
new course: AR-445
taught by: Pierre Frey, sciper: 101461, email: pierre.frey@epfl.ch
new course: BIOENG-420
taught by: Bart Deplancke, sciper: 182376, email: bart.deplancke@epfl.ch
taught by: David Michael Suter, sciper: 235263, email: david.suter@epfl.ch
new course: EE-533
taught by: Seyed Armin Tajalli, sciper: 167171, email: armin.tajalli@epfl.ch
new course: ME-443
taught by: Christophe Nicolet, sciper: 114191, email: christophe.nicolet@epfl.ch
new course: ME-524
taught by: Alireza Karimi, sciper: 139973, email: alireza.karimi@epfl.ch
new course: AR-402(t)
taught by: Raphael Alain Zuber, sciper: 257462, email: raphael.zuber@epfl.ch
new course: HUM-407(b)
	 Could not find email for course HUM-407(b) prof with sciper: 229955
new course: CS-207(a)
taught by: Jean-Cédric Chappelier, sciper: 112547, email: jean-cedric.chappelier@epfl.ch
taught by: Edouard Bugnion, sciper: 229105, email: edouard.bugnion@epfl.ch
new course: MATH-450
taught by: Assyr Abdulle, sciper: 189915, email: assyr.abdulle@epfl.ch
new course: PENS-205
taught by: Luca Giovanni Pattaroni, sciper: 168009, email: luca.pattaroni@epfl.ch
new course: PENS-207
taught by: Emmanuel Pierre Jean Ravalet, sciper: 220794, email: emmanuel.ravalet@epfl.ch
taught by: Derek Pierre Christie, sciper: 220581, email: derek.christie@epfl.ch
taught by: Dominique Von Der Mühll, sciper: 106674, email: dominique.vondermuehll@epfl.ch
taught by: Bettina Schaefli, sciper: 110841, email: bettina.schaefli@epfl.ch
new course: PENS-201
taught by: Alfred Hatt, sciper: 101855, email: fred.hatt@epfl.ch
new course: PENS-200
taught by: Jacques Dubey, sciper: 159096, email: jacques.dubey@epfl.ch
new course: PENS-202
taught by: Georges Abou Jaoudé, sciper: 104355, email: georges.abou-jaoude@epfl.ch
new course: MSE-461
taught by: Paul Muralt, sciper: 105945, email: paul.muralt@epfl.ch
new course: HUM-428(b)
taught by: Dominique Vinck, sciper: 141723, email: dominique.vinck@unil.ch
new course: MSE-466
taught by: Ernst Rudolf Zürcher, sciper: 178624, email: ernstrudolf.zurcher@epfl.ch
new course: MSE-464
taught by: Ludger Weber, sciper: 115064, email: ludger.weber@epfl.ch
taught by: John Christopher Plummer, sciper: 106123, email: christopher.plummer@epfl.ch
new course: CS-110(f)
taught by: Nicolas Macris, sciper: 107423, email: nicolas.macris@epfl.ch
taught by: Jean-Pierre Hubaux, sciper: 105427, email: jean-pierre.hubaux@epfl.ch
taught by: Anastasia Ailamaki, sciper: 177957, email: anastasia.ailamaki@epfl.ch
new course: HUM-434(b)
	 Could not find email for course HUM-434(b) prof with sciper: 182176
new course: MATH-206
taught by: Joachim Stubbe, sciper: 125665, email: joachim.stubbe@epfl.ch
new course: MATH-205
taught by: Bernard Dacorogna, sciper: 104848, email: bernard.dacorogna@epfl.ch
new course: PREPA-031
taught by: Olivier Woringer, sciper: 106748, email: olivier.woringer@epfl.ch
new course: PREPA-030
taught by: Myriam Prongué Costa, sciper: 210795, email: myriam.pronguecosta@epfl.ch
new course: PREPA-032
taught by: Camil-Demetru Petrescu, sciper: 107044, email: camil.petrescu@epfl.ch
	 Could not find email for course PREPA-032 prof with sciper: 126039
new course: PREPA-035
taught by: Guido Burmeister, sciper: 104677, email: guido.burmeister@epfl.ch
taught by: Roger Röthlisberger, sciper: 107148, email: roger.roethlisberger@epfl.ch
new course: HUM-412(d)
taught by: Hugues Poltier, sciper: 141572, email: hugues.poltier@unil.ch
taught by: Gaia Barazzetti, sciper: 181991, email: gaia.barazzetti@unil.ch
new course: ENV-493
taught by: Monique Ruzicka-Rossier, sciper: 117356, email: monique.ruzicka@epfl.ch
	 Could not find email for course ENV-493 prof with sciper: 126096
new course: EE-599(b)
	 Could not find email for course EE-599(b) prof with sciper: 126096
new course: CH-412
taught by: Kai Johnsson, sciper: 123155, email: kai.johnsson@epfl.ch
new course: ENG-435
taught by: Imre Blank, sciper: 218493, email: imre.blank@epfl.ch
new course: ME-301
taught by: Kossi Agbeviade, sciper: 104364, email: kossi.agbeviade@epfl.ch
taught by: Navid Borhani, sciper: 115416, email: navid.borhani@epfl.ch
new course: PHYS-211
taught by: Iva Tkalcec Vâju, sciper: 136611, email: iva.tkalcec@epfl.ch
new course: HUM-410(b)
	 Could not find email for course HUM-410(b) prof with sciper: 171337
	 Could not find email for course HUM-410(b) prof with sciper: 196400
	 Could not find email for course HUM-410(b) prof with sciper: 218733
taught by: Charles Joye, sciper: 218335, email: charles.joye@epfl.ch
new course: HUM-416(b)
	 Could not find email for course HUM-416(b) prof with sciper: 181989
	 Could not find email for course HUM-416(b) prof with sciper: 196399
new course: MICRO-443
taught by: Pavel Kejik, sciper: 114934, email: pavel.kejik@epfl.ch
new course: MICRO-441
taught by: Frédéric Chautems, sciper: 138009, email: frederic.chautems@epfl.ch
new course: MATH-330
taught by: Robert Dalang, sciper: 104859, email: robert.dalang@epfl.ch
new course: HUM-307
taught by: Charles Joye, sciper: 218335, email: charles.joye@epfl.ch
new course: ChE-320
taught by: Vassily Hatzimanikatis, sciper: 174688, email: vassily.hatzimanikatis@epfl.ch
new course: AR-522
taught by: Kersten Geers, sciper: 236660, email: kersten.geers@epfl.ch
new course: CIVIL-533
taught by: Brice Tanguy Alphonse Lecampion, sciper: 258069, email: brice.lecampion@epfl.ch
new course: UNIL-108
taught by: Giordano Favi, sciper: 121777, email: giordano.favi@epfl.ch
new course: BIO-430
taught by: Didier Coquoz, sciper: 181595, email: didier.coquoz@epfl.ch
new course: UNIL-105
taught by: Anne-Sophie Chauvin, sciper: 149479, email: anne-sophie.chauvin@epfl.ch
new course: HUM-415(b)
taught by: Jacques Lévy, sciper: 166241, email: jacques.levy@epfl.ch
new course: HUM-301
	 Could not find email for course HUM-301 prof with sciper: 126039
new course: MATH-416
taught by: Nicolas Monod, sciper: 181579, email: nicolas.monod@epfl.ch
new course: MATH-417
taught by: Pierre Francis André Le Boudec, sciper: 240161, email: pierre.leboudec@epfl.ch
new course: MGT-413(b)
new course: PHYS-440
taught by: Guido Haefeli, sciper: 161435, email: guido.haefeli@epfl.ch
new course: AR-416
taught by: Philipp Christian Schaerer, sciper: 103576, email: philipp.schaerer@epfl.ch
new course: AR-414
taught by: Georges Abou Jaoudé, sciper: 104355, email: georges.abou-jaoude@epfl.ch
taught by: Ronald Cicurel, sciper: 176013, email: ronald.cicurel@epfl.ch
taught by: Sébastien Lasserre, sciper: 154037, email: sebastien.lasserre@epfl.ch
new course: AR-412
taught by: Georges Abou Jaoudé, sciper: 104355, email: georges.abou-jaoude@epfl.ch
new course: CS-173
taught by: Eduardo Sanchez, sciper: 106339, email: eduardo.sanchez@epfl.ch
new course: ME-571
taught by: Mirco Magnini, sciper: 212324, email: mirco.magnini@epfl.ch
new course: AR-220
taught by: Nicola Braghieri, sciper: 180534, email: nicola.braghieri@epfl.ch
new course: HUM-277
	 Could not find email for course HUM-277 prof with sciper: 262089
new course: HUM-276
	 Could not find email for course HUM-276 prof with sciper: 263196
new course: HUM-271
taught by: Hugues Poltier, sciper: 141572, email: hugues.poltier@unil.ch
new course: HUM-408(b)
	 Could not find email for course HUM-408(b) prof with sciper: 260621
new course: MATH-600
taught by: Michel Bierlaire, sciper: 118332, email: michel.bierlaire@epfl.ch
new course: HUM-409(b)
taught by: Véronique Mauron Layaz, sciper: 172779, email: veronique.mauron@epfl.ch
taught by: André Ourednik, sciper: 169009, email: andre.ourednik@epfl.ch
new course: MATH-470
taught by: Michael Schmutz, sciper: 209364, email: m.schmutz@epfl.ch
new course: CS-108
taught by: Michel Schinz, sciper: 103610, email: michel.schinz@epfl.ch
new course: CIVIL-437
taught by: Eugen Brühwiler, sciper: 104646, email: eugen.bruehwiler@epfl.ch
new course: CH-453
taught by: Jiri Vanicek, sciper: 181577, email: jiri.vanicek@epfl.ch
new course: PREPA-002
taught by: Anne-Marie Dovi-Bleynat, sciper: 100435, email: anne-marie.dovi@epfl.ch
taught by: Véronique Tissot-Daguette, sciper: 106559, email: veronique.tissot@epfl.ch
new course: CIVIL-435
taught by: Dimitrios Lignos, sciper: 265223, email: dimitrios.lignos@epfl.ch
new course: PHYS-439
taught by: Andrii Neronov, sciper: 162298, email: andrii.neronov@epfl.ch
taught by: Aurelio Bay, sciper: 122784, email: aurelio.bay@epfl.ch
new course: HUM-234
	 Could not find email for course HUM-234 prof with sciper: 196400
new course: HUM-418(b)
	 Could not find email for course HUM-418(b) prof with sciper: 166175
new course: HUM-337(a)
taught by: Gianni Haver, sciper: 157690, email: Gianni.Haver@unil.ch
new course: PHYS-432
taught by: Riccardo Rattazzi, sciper: 174685, email: riccardo.rattazzi@epfl.ch
new course: ENV-509
taught by: Michael Jon Mattle, sciper: 152295, email: michael.mattle@epfl.ch
taught by: Marc Deront, sciper: 104934, email: marc.deront@epfl.ch
new course: HUM-403(b)
	 Could not find email for course HUM-403(b) prof with sciper: 218732
new course: HEP-403
	 Could not find email for course HEP-403 prof with sciper: 109752
	 Could not find email for course HEP-403 prof with sciper: 251149
new course: HEP-401
	 Could not find email for course HEP-401 prof with sciper: 251152
new course: HEP-406
	 Could not find email for course HEP-406 prof with sciper: 251207
new course: HEP-404
	 Could not find email for course HEP-404 prof with sciper: 251147
	 Could not find email for course HEP-404 prof with sciper: 251151
new course: AR-402(b)
taught by: Harry Gugger, sciper: 147009, email: harry.gugger@epfl.ch
new course: CH-359
taught by: Jiri Vanicek, sciper: 181577, email: jiri.vanicek@epfl.ch
taught by: Anne-Clémence Corminboeuf, sciper: 178464, email: clemence.corminboeuf@epfl.ch
taught by: Ursula Röthlisberger, sciper: 150117, email: ursula.roethlisberger@epfl.ch
new course: CH-427
taught by: Marinella Mazzanti, sciper: 166619, email: marinella.mazzanti@epfl.ch
new course: CH-424
taught by: Kay Severin, sciper: 145138, email: kay.severin@epfl.ch
new course: MSE-231
taught by: Dragan Damjanovic, sciper: 104860, email: dragan.damjanovic@epfl.ch
taught by: Igor Stolichnov, sciper: 112564, email: igor.stolitchnov@epfl.ch
new course: PHYS-323
taught by: Pierre North, sciper: 157449, email: pierre.north@epfl.ch
new course: PHYS-320
taught by: Iva Tkalcec Vâju, sciper: 136611, email: iva.tkalcec@epfl.ch
taught by: Arnaud Magrez, sciper: 161833, email: arnaud.magrez@epfl.ch
taught by: Daniel Oberli, sciper: 106001, email: daniel.oberli@epfl.ch
new course: PENS-311
taught by: Isabella Maria Pasqualini, sciper: 176340, email: isabella.pasqualini@epfl.ch
new course: HUM-431
	 Could not find email for course HUM-431 prof with sciper: 126039
new course: AR-202(k)
taught by: Dominique Perrault, sciper: 235585, email: dominique.perrault@epfl.ch
new course: CH-229
taught by: Paul Joseph Dyson, sciper: 149418, email: paul.dyson@epfl.ch
new course: HUM-350
	 Could not find email for course HUM-350 prof with sciper: 207135
new course: CH-390
	 Could not find email for course CH-390 prof with sciper: 126096
new course: HUM-228
	 Could not find email for course HUM-228 prof with sciper: 126096
new course: HUM-224
	 Could not find email for course HUM-224 prof with sciper: 229955
new course: HUM-225
	 Could not find email for course HUM-225 prof with sciper: 184338
new course: ME-432
taught by: John Botsis, sciper: 111182, email: john.botsis@epfl.ch
taught by: Joël Cugnoni, sciper: 100957, email: joel.cugnoni@epfl.ch
new course: MATH-207(c)
taught by: Philippe Metzener, sciper: 105837, email: philippe.metzener@epfl.ch
new course: AR-402(u)
taught by: Raphaël Pascal Ménard, sciper: 262335, email: raphael.menard@epfl.ch
new course: MATH-207(d)
taught by: Philippe Metzener, sciper: 105837, email: philippe.metzener@epfl.ch
new course: CS-413
taught by: Sabine Süsstrunk, sciper: 125681, email: sabine.susstrunk@epfl.ch
new course: MSE-443(b)
taught by: William Craig Carter, sciper: 240979, email: william.carter@epfl.ch
taught by: Michele Ceriotti, sciper: 235586, email: michele.ceriotti@epfl.ch
new course: MSE-478
taught by: Frank Nüesch, sciper: 105997, email: frank.nuesch@epfl.ch
new course: PHYS-500(b)
	 Could not find email for course PHYS-500(b) prof with sciper: 126096
new course: MSE-473
new course: MSE-474
taught by: Sébastien Vaucher, sciper: 172821, email: sebastien.vaucher@epfl.ch
taught by: Johann Michler, sciper: 108553, email: johann.michler@epfl.ch
taught by: Stephan Siegmann, sciper: 174195, email: stephan.siegmann@epfl.ch
new course: AR-420
taught by: Luca Ortelli, sciper: 113297, email: luca.ortelli@epfl.ch
taught by: Maria Chiara Barone, sciper: 171020, email: mariachiara.barone@epfl.ch
new course: HUM-439(b)
	 Could not find email for course HUM-439(b) prof with sciper: 263730
	 Could not find email for course HUM-439(b) prof with sciper: 263196
new course: ENG-472(b)
	 Could not find email for course ENG-472(b) prof with sciper: 126096
new course: UNIL-002
taught by: Georges Meylan, sciper: 158695, email: georges.meylan@epfl.ch
new course: CIVIL-224
taught by: Pierino Lestuzzi, sciper: 136291, email: pierino.lestuzzi@epfl.ch
	 Could not find email for course CIVIL-224 prof with sciper: 106693
new course: MICRO-515
taught by: Dario Floreano, sciper: 111729, email: dario.floreano@epfl.ch
new course: PREPA-022
taught by: Roger Sauser, sciper: 157882, email: roger.sauser@epfl.ch
taught by: Guido Burmeister, sciper: 104677, email: guido.burmeister@epfl.ch
new course: PREPA-020
taught by: Hans-Jörg Ruppen, sciper: 145266, email: hans-joerg.ruppen@epfl.ch
new course: HUM-414(b)
	 Could not find email for course HUM-414(b) prof with sciper: 126096
new course: EE-333
taught by: Mihai Adrian Ionescu, sciper: 122431, email: adrian.ionescu@epfl.ch
new course: MSE-271
taught by: Lionel Sofia, sciper: 154702, email: lionel.sofia@epfl.ch
taught by: Cyril Dénéréaz, sciper: 111726, email: cyril.denereaz@epfl.ch
new course: PENS-308
taught by: Patricia Guaita, sciper: 164835, email: patricia.guaita@epfl.ch
taught by: Dieter Dietz, sciper: 173997, email: dieter.dietz@epfl.ch
new course: PENS-301
taught by: Christian Roecker, sciper: 106255, email: christian.roecker@epfl.ch
taught by: Maria Cristina Munari Probst, sciper: 159393, email: mariacristina.munariprobst@epfl.ch
new course: PENS-302
taught by: Laurent Tacher, sciper: 106517, email: laurent.tacher@epfl.ch
taught by: François Golay, sciper: 105275, email: francois.golay@epfl.ch
taught by: Yvan Delemontey, sciper: 188569, email: yvan.delemontey@epfl.ch
taught by: Lyesse Laloui, sciper: 105611, email: lyesse.laloui@epfl.ch
taught by: Valentin Simeonov, sciper: 107964, email: valentin.simeonov@epfl.ch
taught by: Franz Graf, sciper: 161950, email: franz.graf@epfl.ch
new course: PENS-303
taught by: Jérôme Zufferey, sciper: 116344, email: jerome.zufferey@epfl.ch
taught by: Pierre-Yves Gilliéron, sciper: 115027, email: pierre-yves.gillieron@epfl.ch
taught by: Olivier Burdet, sciper: 104671, email: olivier.burdet@epfl.ch
new course: PHYS-502(b)
	 Could not find email for course PHYS-502(b) prof with sciper: 126096
new course: PHYS-209
taught by: Frédéric Courbin, sciper: 166196, email: frederic.courbin@epfl.ch
new course: COM-302
taught by: Rüdiger Urbanke, sciper: 124369, email: rudiger.urbanke@epfl.ch
new course: AR-402(s)
taught by: João Pedro Barros Falcão de Campos, sciper: 257459, email: joao.falcaodecampos@epfl.ch
new course: HUM-369
taught by: Frédéric Kaplan, sciper: 174121, email: frederic.kaplan@epfl.ch
new course: HUM-364
taught by: Brenno Boccadoro, sciper: 167302, email: Brenno.Boccadoro@unige.ch
new course: MSE-207
taught by: Roland Logé, sciper: 243441, email: roland.loge@epfl.ch
new course: MSE-205
taught by: Pierre-Etienne Bourban, sciper: 104610, email: pierre-etienne.bourban@epfl.ch
new course: EE-380
taught by: André Décurnex, sciper: 104903, email: andre.decurnex@epfl.ch
new course: MSE-209
taught by: Paul Bowen, sciper: 104626, email: paul.bowen@epfl.ch
taught by: Esther Amstad, sciper: 240724, email: esther.amstad@epfl.ch
new course: EE-582
	 Could not find email for course EE-582 prof with sciper: 173661
new course: EE-583
taught by: Bruno Storni, sciper: 173663, email: bruno.storni@epfl.ch
new course: MATH-106(f)
taught by: Giordano Favi, sciper: 121777, email: giordano.favi@epfl.ch
new course: UNIL-111
taught by: François Patthey, sciper: 123314, email: francois.patthey@epfl.ch
new course: UNIL-110
taught by: Aurelio Bay, sciper: 122784, email: aurelio.bay@epfl.ch
new course: MATH-105(b)
taught by: Joachim Stubbe, sciper: 125665, email: joachim.stubbe@epfl.ch
new course: HUM-323(a)
	 Could not find email for course HUM-323(a) prof with sciper: 182176
new course: MGT-431
taught by: Thomas Alois Weber, sciper: 216550, email: thomas.weber@epfl.ch
new course: MATH-405
taught by: Joachim Krieger, sciper: 199126, email: joachim.krieger@epfl.ch
new course: MATH-404
taught by: Boris Buffoni, sciper: 120861, email: boris.buffoni@epfl.ch
new course: HUM-437(b)
taught by: Delphine Preissmann, sciper: 185819, email: delphine.preissmann@unil.ch
new course: HUM-436(b)
	 Could not find email for course HUM-436(b) prof with sciper: 229643
taught by: Yann Dahhaoui, sciper: 260640, email: yann.dahhaoui@unil.ch
new course: HUM-238
	 Could not find email for course HUM-238 prof with sciper: 192425
new course: AR-460
taught by: Jacques Lévy, sciper: 166241, email: jacques.levy@epfl.ch
taught by: Eduardo Camacho-Hübner, sciper: 100701, email: eduardo.camacho-huebner@epfl.ch
new course: AR-461
taught by: Boris Beaude, sciper: 168003, email: boris.beaude@epfl.ch
new course: HUM-429(b)
	 Could not find email for course HUM-429(b) prof with sciper: 192425
new course: BIOENG-440
new course: BIOENG-447
taught by: Bruno Emanuel Ferreira De Sousa Correia, sciper: 253103, email: bruno.correia@epfl.ch
taught by: Oliver Hantschel, sciper: 209845, email: oliver.hantschel@epfl.ch
new course: PHYS-454
taught by: Marc-André Dupertuis, sciper: 105021, email: marc-andre.dupertuis@epfl.ch
new course: MATH-225
taught by: Kathryn Hess Bellwald, sciper: 105396, email: kathryn.hess@epfl.ch
new course: MSE-103
taught by: Jean-Marie Drezet, sciper: 101178, email: jean-marie.drezet@epfl.ch
taught by: Pierre-Etienne Bourban, sciper: 104610, email: pierre-etienne.bourban@epfl.ch
taught by: Léa Deillon, sciper: 161342, email: lea.deillon@epfl.ch
new course: COM-516
taught by: Nicolas Macris, sciper: 107423, email: nicolas.macris@epfl.ch
taught by: Olivier Lévêque, sciper: 107931, email: olivier.leveque@epfl.ch
new course: COM-512
taught by: Laura Elisa Celis, sciper: 245193, email: elisa.celis@epfl.ch
taught by: Patrick Thiran, sciper: 103925, email: patrick.thiran@epfl.ch
new course: ENV-321
taught by: Tom Ian Battin, sciper: 253102, email: tom.battin@epfl.ch
new course: HUM-269
	 Could not find email for course HUM-269 prof with sciper: 166175
new course: ENG-432
taught by: Jean-Luc Marendaz, sciper: 148581, email: jean-luc.marendaz@epfl.ch
new course: HUM-262(a)
	 Could not find email for course HUM-262(a) prof with sciper: 182176
new course: HUM-334(a)
taught by: Marta Roca Escoda, sciper: 229445, email: marta.rocaescoda@unil.ch
new course: HUM-417(b)
taught by: Michael-Andreas Esfeld, sciper: 158392, email: michael-andreas.esfeld@epfl.ch
new course: CIVIL-123
taught by: Olivier Burdet, sciper: 104671, email: olivier.burdet@epfl.ch
new course: UNIL-301
taught by: Pascal Turberg, sciper: 134475, email: pascal.turberg@epfl.ch
new course: MICRO-434
taught by: Philippe Renaud, sciper: 107144, email: philippe.renaud@epfl.ch
new course: ENG-436
taught by: Carl Erik Hansen, sciper: 218473, email: carl-erik.hansen@epfl.ch
new course: CH-426
taught by: Shaik Mohammed Zakeeruddin, sciper: 106763, email: shaik.zakeer@epfl.ch
new course: ChE-304
taught by: Berend Smit, sciper: 242254, email: berend.smit@epfl.ch
taught by: Jeremy Luterbacher, sciper: 154408, email: jeremy.luterbacher@epfl.ch
new course: AR-402(c)
taught by: Kersten Geers, sciper: 236660, email: kersten.geers@epfl.ch
new course: MSE-322
taught by: Karen Scrivener, sciper: 138857, email: karen.scrivener@epfl.ch
taught by: Lionel Sofia, sciper: 154702, email: lionel.sofia@epfl.ch
taught by: Emmanuelle Boehm Courjault, sciper: 156608, email: emmanuelle.boehm@epfl.ch
new course: CH-431
taught by: Anne-Clémence Corminboeuf, sciper: 178464, email: clemence.corminboeuf@epfl.ch
new course: BIO-379
taught by: André Pexieder, sciper: 103075, email: andre.pexieder@epfl.ch
new course: ME-324
taught by: Philippe Müllhaupt, sciper: 105941, email: philippe.muellhaupt@epfl.ch
new course: HUM-423
new course: HUM-424
	 Could not find email for course HUM-424 prof with sciper: 225319
new course: HUM-401(b)
	 Could not find email for course HUM-401(b) prof with sciper: 157990
	 Could not find email for course HUM-401(b) prof with sciper: 221192
new course: MATH-351
taught by: Marco Picasso, sciper: 106096, email: marco.picasso@epfl.ch
new course: HUM-321
taught by: Claus Gunti, sciper: 181990, email: claus.gunti@unil.ch
new course: HUM-327
	 Could not find email for course HUM-327 prof with sciper: 184338
new course: HUM-326
	 Could not find email for course HUM-326 prof with sciper: 229955
new course: HUM-438(b)
taught by: Félicie Marie Guénola de Maupeou d'Ableiges, sciper: 260749, email: feliciedemaupeou@hotmail.fr
new course: PHYS-424
taught by: Ambrogio Fasoli, sciper: 105078, email: ambrogio.fasoli@epfl.ch
taught by: Duccio Testa, sciper: 153200, email: duccio.testa@epfl.ch
taught by: Alan Howling, sciper: 105426, email: alan.howling@epfl.ch
taught by: Ivo Furno, sciper: 111055, email: ivo.furno@epfl.ch
new course: CS-308
taught by: Nicolas Macris, sciper: 107423, email: nicolas.macris@epfl.ch
new course: HUM-251
	 Could not find email for course HUM-251 prof with sciper: 114106
new course: ME-420
new course: ME-424
taught by: Kossi Agbeviade, sciper: 104364, email: kossi.agbeviade@epfl.ch
new course: PHYS-607
taught by: Luc Thévenaz, sciper: 106544, email: luc.thevenaz@epfl.ch
new course: PHYS-106(h)
taught by: Ambrogio Fasoli, sciper: 105078, email: ambrogio.fasoli@epfl.ch
new course: MICRO-343
taught by: Peter Ryser, sciper: 115532, email: peter.ryser@epfl.ch
new course: MICRO-342
taught by: Peter Ryser, sciper: 115532, email: peter.ryser@epfl.ch
new course: BIO-451
taught by: Jeffrey David Jensen, sciper: 217758, email: jeffrey.jensen@epfl.ch
new course: AR-423(a)
taught by: Roberto Gargiani, sciper: 126097, email: roberto.gargiani@epfl.ch
new course: CS-455
taught by: Ola Nils Anders Svensson, sciper: 213225, email: ola.svensson@epfl.ch
new course: AR-433
	 Could not find email for course AR-433 prof with sciper: 183544
taught by: Eglantine Bigot-Doll, sciper: 173266, email: eglantine.bigot-doll@epfl.ch
taught by: Paule Soubeyrand, sciper: 106453, email: paule.soubeyrand@epfl.ch
new course: EE-565
taught by: Drazen Dujic, sciper: 243438, email: drazen.dujic@epfl.ch
new course: UNIL-401
taught by: Jean-Luc Marendaz, sciper: 148581, email: jean-luc.marendaz@epfl.ch
new course: PREPA-016
taught by: Roger Sauser, sciper: 157882, email: roger.sauser@epfl.ch
new course: PREPA-014
taught by: Véronique Tissot-Daguette, sciper: 106559, email: veronique.tissot@epfl.ch
taught by: Anne-Marie Dovi-Bleynat, sciper: 100435, email: anne-marie.dovi@epfl.ch
new course: PREPA-012
taught by: Jördis Tietje-Girault, sciper: 115196, email: jordis.tietje-girault@epfl.ch
new course: PREPA-010
taught by: Saida Guennoun-Lehmann, sciper: 207302, email: saida.guennoun-lehmann@epfl.ch
new course: PREPA-018
taught by: Camil-Demetru Petrescu, sciper: 107044, email: camil.petrescu@epfl.ch
taught by: Roger Röthlisberger, sciper: 107148, email: roger.roethlisberger@epfl.ch
new course: HUM-435(b)
taught by: Samuel Bendahan, sciper: 190153, email: samuel.bendahan@epfl.ch
new course: CS-470
taught by: Paolo Ienne, sciper: 101954, email: paolo.ienne@epfl.ch
new course: ChE-410
taught by: Oliver Kröcher, sciper: 239988, email: oliver.krocher@epfl.ch
new course: HUM-406(b)
taught by: Marie Chabbey, sciper: 264199, email: marie.chabbey@epfl.ch
taught by: Adriano Giardina, sciper: 167293, email: adriano.giardina@unil.ch
new course: COM-303
taught by: Paolo Prandoni, sciper: 110610, email: paolo.prandoni@epfl.ch
new course: PHYS-416
taught by: Aurelio Bay, sciper: 122784, email: aurelio.bay@epfl.ch
new course: MICRO-540
taught by: Peter Ryser, sciper: 115532, email: peter.ryser@epfl.ch
new course: HUM-371
	 Could not find email for course HUM-371 prof with sciper: 263196
new course: MATH-481
taught by: John Maddocks, sciper: 113868, email: john.maddocks@epfl.ch
new course: HUM-419(b)
	 Could not find email for course HUM-419(b) prof with sciper: 188747
new course: MATH-314
taught by: Donna Testerman, sciper: 133751, email: donna.testerman@epfl.ch
new course: HUM-420(b)
	 Could not find email for course HUM-420(b) prof with sciper: 166176
new course: MSE-211
taught by: Holger Frauenrath, sciper: 190315, email: holger.frauenrath@epfl.ch
new course: MATH-312
taught by: Eva Bayer Fluckiger, sciper: 138858, email: eva.bayer@epfl.ch
new course: PENS-209
taught by: Jade Rudler, sciper: 187909, email: jade.rudler@epfl.ch
taught by: Riccardo Scarinci, sciper: 241005, email: riccardo.scarinci@epfl.ch
new course: PENS-208
taught by: Patrick Giromini, sciper: 240123, email: patrick.giromini@epfl.ch
taught by: Nicola Braghieri, sciper: 180534, email: nicola.braghieri@epfl.ch
new course: CIVIL-428
	 Could not find email for course CIVIL-428 prof with sciper: 259287
new course: CIVIL-557
taught by: Michel Bierlaire, sciper: 118332, email: michel.bierlaire@epfl.ch
new course: UNIL-127
taught by: Jean-Luc Marendaz, sciper: 148581, email: jean-luc.marendaz@epfl.ch
new course: UNIL-125
taught by: Kamiar Aminian, sciper: 104385, email: kamiar.aminian@epfl.ch
taught by: Dominique Pioletti, sciper: 103117, email: dominique.pioletti@epfl.ch
new course: UNIL-123
taught by: Wolfgang Harbich, sciper: 105362, email: wolfgang.harbich@epfl.ch
new course: ENV-596
	 Could not find email for course ENV-596 prof with sciper: 126096
new course: MGT-426
taught by: Philippe Wieser, sciper: 106738, email: philippe.wieser@epfl.ch
new course: MATH-360
taught by: Frank de Zeeuw, sciper: 225804, email: frank.dezeeuw@epfl.ch
new course: ME-107
taught by: Jean-François Ferrot, sciper: 218963, email: jean-francois.ferrot@epfl.ch
taught by: Bertrand Lacour, sciper: 240428, email: bertrand.lacour@epfl.ch
new course: AR-402(r)
taught by: Christian Andreas Müller, sciper: 257460, email: christian.muellerinderbitzin@epfl.ch
